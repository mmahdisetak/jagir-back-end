﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.TestHost;
using UnitTest.ControllerTest.Event;
using UnitTest.ControllerTest.Item;
using UnitTest.ControllerTest.Ticket;
using UnitTest.ControllerTest.User;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace IntegrationTests.Scenario
{
    public class ReserveScenarioTest : AppFactory
    {
        private readonly ITestOutputHelper _outputHelper;

        public ReserveScenarioTest(ITestOutputHelper outputHelper) : base(outputHelper)
        {
            _outputHelper = outputHelper;
        }

        [Fact]
        public async Task ShouldWorkCorrectly()
        {
            var admin = Host.GetTestClient();
            await admin.AuthToAdmin();
            
            await new IncreaseCreditTest(_outputHelper, admin).IncreaseCredit(100000);
            var eventId = await new GetBestEventsTest(_outputHelper, admin).GetBestEvents(3);
            var itemId = await new GetEventItemsTest(_outputHelper, admin).GetEventItems(eventId);
            await new ReserveTest(_outputHelper, admin).Reserve("", new[] {itemId});
            await new GetEventItemsTest(_outputHelper, admin).GetEventItems(eventId);
        }
    }
}