﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.TestHost;
using UnitTest.ControllerTest.Event;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace IntegrationTests.Scenario
{
    public class EventCRUDTest : AppFactory
    {
        private readonly ITestOutputHelper _outputHelper;

        public EventCRUDTest(ITestOutputHelper outputHelper) : base(outputHelper)
        {
            _outputHelper = outputHelper;
        }

        [Fact]
        public async Task ShouldWorkCorrectly()
        {
            var admin = Host.GetTestClient();
            await admin.AuthToAdmin();

            var id = await new CreateEventTest(_outputHelper, admin)
                .CreateEvent("channel1", "TestEvent", "IntegratedEvent", true, true, null, null);

            await new EditEventTest(_outputHelper, admin)
                .EditEvent(id, "EditedEvent", null,"ThisEventHasBeenEdited", null, null, null, null);

            var @event = await new GetEventTest(_outputHelper, admin).GetEvent(id);

            Assert.Equal("EditedEvent", @event.Name);
            Assert.Equal("ThisEventHasBeenEdited", @event.Description);
        }
    }
}