﻿using System.Threading.Tasks;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.ControllerTest.Account;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace IntegrationTests.Scenario
{
    public class SingUpSingInTest : AppFactory
    {
        private readonly ITestOutputHelper _outputHelper;

        public SingUpSingInTest(ITestOutputHelper outputHelper) : base(outputHelper)
        {
            _outputHelper = outputHelper;
        }

        [Fact]
        public async Task ShouldWorkCorrectly()
        {
            var user = Host.GetTestClient();
            await new SignUpTest(_outputHelper, user).SignUp("NewUser1", "newuser1@khu.ir", "newUserPass123");
            await new SignInTest(_outputHelper, user).SignIn(SignInType.Email, "newuser1@khu.ir", "newUserPass123",
                ErrorCode.UnConfirmedEmail);
        }
    }
}