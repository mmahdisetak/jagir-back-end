﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.TestHost;
using UnitTest.ControllerTest.Channel;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace IntegrationTests.Scenario
{
    public class ChannelCRUDTest : AppFactory
    {
        private readonly ITestOutputHelper _outputHelper;

        public ChannelCRUDTest(ITestOutputHelper outputHelper) : base(outputHelper)
        {
            _outputHelper = outputHelper;
        }

        [Fact]
        public async Task ShouldWorkCorrectly()
        {
            var admin = Host.GetTestClient();
            await admin.AuthToAdmin();

            var id = await new CreateChannelTest(_outputHelper, admin)
                .CreateChannel("IntegratedChannel", "ChannelForTest", "user1", null);

            await new EditChannelProfileTest(_outputHelper, admin)
                .EditChannelProfile(id, "EditedChannel", "ThisChannelHasBeenEdited", "", "");

            var channel = await new GetChannelProfileTest(_outputHelper, admin).GetChannelProfile(id);

            Assert.Equal("EditedChannel", channel.Name);
            Assert.Equal("ThisChannelHasBeenEdited", channel.Description);
        }
    }
}