﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.TestHost;
using UnitTest.ControllerTest.User;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace IntegrationTests.Scenario
{
    public class FollowScenarioTest : AppFactory
    {
        private readonly ITestOutputHelper _outputHelper;

        public FollowScenarioTest(ITestOutputHelper outputHelper) : base(outputHelper)
        {
            _outputHelper = outputHelper;
        }

        [Fact]
        public async Task ShouldWorkCorrectly()
        {
            var user = Host.GetTestClient();
            await user.AuthToUser();
            
            await new FollowChannelTest(_outputHelper, user).FollowChannel("channel1", true);
            await new FollowChannelTest(_outputHelper, user).FollowChannel("channel1", false);
        }
    }
}