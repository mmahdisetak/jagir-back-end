﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.TestHost;
using UnitTest.ControllerTest.Comment;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace IntegrationTests.Scenario
{
    public class CommentScenarioTest : AppFactory
    {
        private readonly ITestOutputHelper _outputHelper;

        public CommentScenarioTest(ITestOutputHelper outputHelper) : base(outputHelper)
        {
            _outputHelper = outputHelper;
        }

        [Fact]
        public async Task ShouldWorkCorrectly()
        {
            var user = Host.GetTestClient();
            await user.AuthToUser();

            await new SubmitCommentTest(_outputHelper, user).SubmitComment("event1", "I like this event!");
            await new GetEventCommentsTest(_outputHelper, user).GetEventComments("event1");
        }
    }
}