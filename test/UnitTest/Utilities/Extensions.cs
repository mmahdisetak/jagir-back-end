﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Application.ViewModels.Account;
using Domain.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.Utilities
{
    public static class Extensions
    {
        public static async Task<string> GetErrorCode(this HttpResponseMessage response)
        {
            var errorCode = (await response.GetJson())["error"]?["code"];
            return errorCode?.ToString();
        }

        public static async Task<JObject> GetJson(this HttpResponseMessage response)
        {
            return JObject.Parse(await response.Content.ReadAsStringAsync());
        }

        public static async Task<string> GetContent(this HttpResponseMessage message)
        {
            var content = Regex.Replace(
                await message.Content.ReadAsStringAsync(),
                @"\\u(?<Value>[a-fA-F0-9]{4})",
                m => ((char) int.Parse(m.Groups["Value"].Value, NumberStyles.HexNumber)).ToString());

            try
            {
                if (!content.StartsWith("{"))
                    throw new Exception();

                var indentation = 0;
                var quoteCount = 0;
                const string indentString = "    ";
                var result =
                    from ch in content
                    let quotes = ch == '"' ? quoteCount++ : quoteCount
                    let lineBreak = ch == ',' && quotes % 2 == 0
                        ? ch + Environment.NewLine + string.Concat(Enumerable.Repeat(indentString, indentation))
                        : null
                    let openChar = ch == '{' || ch == '['
                        ? ch + Environment.NewLine + string.Concat(Enumerable.Repeat(indentString, ++indentation))
                        : ch.ToString()
                    let closeChar = ch == '}' || ch == ']'
                        ? Environment.NewLine + string.Concat(Enumerable.Repeat(indentString, --indentation)) + ch
                        : ch.ToString()
                    select lineBreak ?? (openChar.Length > 1
                        ? openChar
                        : closeChar);

                return string.Concat(result);
            }
            catch
            {
                return content;
            }
        }

        public static async Task<HttpResponseMessage> GetResponse(HttpClient client, string channelJson, string url)
        {
            var response = await client.PostAsync(url,
                new StringContent(channelJson, Encoding.UTF8, "application/json"));
            return response;
        }

        public static async Task AuthToUser(this HttpClient client)
        {
            var userObj = new SignInViewModel.Request
            {
                Login = "TestUser@Jagir.ir",
                Type = SignInType.Email,
                Password = "TestPassword"
            };
            var user = JsonConvert.SerializeObject(userObj);

            var response = await client.PostAsync(Urls.Account.SignIn,
                new StringContent(user, Encoding.UTF8, "application/json"));

            client.DefaultRequestHeaders.Add("Cookie", response.Headers.GetValues("Set-Cookie").ToArray()[0]);
        }
        
        public static async Task AuthToUser2(this HttpClient client)
        {
            var userObj = new SignInViewModel.Request
            {
                Login = "TestUser3@Jagir.ir",
                Type = SignInType.Email,
                Password = "TestPassword"
            };
            var user = JsonConvert.SerializeObject(userObj);

            var response = await client.PostAsync(Urls.Account.SignIn,
                new StringContent(user, Encoding.UTF8, "application/json"));

            client.DefaultRequestHeaders.Add("Cookie", response.Headers.GetValues("Set-Cookie").ToArray()[0]);
        }

        public static async Task AuthToSupervisor(this HttpClient client)
        {
            var userObj = new SignInViewModel.Request
            {
                Login = "channelSupervisor@Jagir.ir",
                Type = SignInType.Email,
                Password = "TestPassword"
            };
            var user = JsonConvert.SerializeObject(userObj);

            var response = await client.PostAsync(Urls.Account.SignIn,
                new StringContent(user, Encoding.UTF8, "application/json"));

            client.DefaultRequestHeaders.Add("Cookie", response.Headers.GetValues("Set-Cookie").ToArray()[0]);
        }

        public static async Task AuthToAdmin(this HttpClient client)
        {
            var userObj = new SignInViewModel.Request
            {
                Login = "TestAdmin@Jagir.ir",
                Type = SignInType.Email,
                Password = "AdminPassword"
            };
            var user = JsonConvert.SerializeObject(userObj);

            var response = await client.PostAsync(Urls.Account.SignIn,
                new StringContent(user, Encoding.UTF8, "application/json"));

            client.DefaultRequestHeaders.Add("Cookie", response.Headers.GetValues("Set-Cookie").ToArray()[0]);
        }

        public static async Task Assertion(ITestOutputHelper outputHelper, HttpResponseMessage response,
            HttpStatusCode expectedStatusCode, ErrorCode expectedErrorCode)
        {
            //show additional output
            outputHelper.WriteLine(await response.GetContent());

            //Assert
            Assert.Equal(expectedStatusCode, response.StatusCode);
            Assert.Equal($"{(short) expectedErrorCode}", await response.GetErrorCode());
        }
    }
}