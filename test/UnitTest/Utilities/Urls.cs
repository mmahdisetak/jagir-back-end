﻿namespace UnitTest.Utilities
{
    public class Urls
    {
        public static class Account
        {
            private static string Base => "Account/";
            public static string SignIn => Base + "SignIn";
            public static string SignUp => Base + "SignUp";
            public static string SignOut => Base + "SignOut";
            public static string ConfirmEmail => Base + "ConfirmEmail";
            public static string ForgetPassword => Base + "ForgetPassword";
            public static string ResetPassword => Base + "ResetPassword";
        }

        public static class Channel
        {
            private static string Base => "Channel/";
            public static string CreateChannel => Base + "CreateChannel";
            public static string EditChannelProfile => Base + "EditChannelProfile";
            public static string GetChannelProfile => Base + "GetChannelProfile";
            public static string SearchChannelFollowers => Base + "SearchChannelFollowers";
            public static string AddSupervisor => Base + "AddSupervisor";
            public static string GetBestChannels => Base + "GetBestChannels";
        }

        public static class User
        {
            private static string Base => "User/";
            public static string GetProfile => Base + "GetProfile";
            public static string FollowChannel => Base + "FollowChannel";
            public static string EditProfile => Base + "EditProfile";
        }

        public static class Event
        {
            private static string Base => "Event/";
            public static string CreateEvent => Base + "CreateEvent";
            public static string EditEvent => Base + "EditEvent";
            public static string RemoveEvent => Base + "RemoveEvent";
            public static string GetEvent => Base + "GetEvent";
            public static string SearchEvents => Base + "SearchEvents";
            public static string GetBestEvents => Base + "GetBestEvents";
            public static string GetCurrentEvents => Base + "GetCurrentEvents";
            public static string GetPassedCasualEvents => Base + "GetPassedCasualEvents";
            public static string GetPassedWeeklyEvents => Base + "GetPassedWeeklyEvents";
            public static string GetUpcomingEvents => Base + "GetUpcomingEvents";
        }

        public static class Comment
        {
            private static string Base => "Comment/";
            public static string GetEventComments => Base + "GetEventComments";
            public static string SubmitComment => Base + "SubmitComment";
        }
        
        public static class Item
        {
            private static string Base => "Item/";
            public static string CreateItem => Base + "CreateItem";
            public static string EditItem => Base + "EditItem";
            public static string RemoveItem => Base + "RemoveItem";
            public static string GetEventItems => Base + "GetEventItems";
        }
        
        public static class Ticket
        {
            private static string Base => "Ticket/";
            public static string Cancel => Base + "Cancel";
            public static string GetUserTickets => Base + "GetUserTickets";
            public static string Reserve => Base + "Reserve";
        }
    }
}