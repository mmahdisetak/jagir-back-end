﻿using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Domain.Enums;
using Hangfire;
using Infrastructure.Persistence;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebAPI;
using Xunit;
using Xunit.Abstractions;
using DatabaseInitializer = UnitTest.Persistence.DatabaseInitializer;

namespace UnitTest.Utilities
{
    public class AppFactory
    {
        private readonly ITestOutputHelper _outputHelper;
        private readonly string _uri;
        protected IHost Host;

        protected AppFactory()
        {
            ConfigureWebHost();
            new DatabaseInitializer(Host.Services).Initialize().GetAwaiter().GetResult();
        }

        protected AppFactory(ITestOutputHelper outputHelper)
        {
            _outputHelper = outputHelper;
            var initial = new StackTrace().GetFrame(2)?.GetMethod()?.Name == "InvokeMethod";

            if (initial)
            {
                ConfigureWebHost();
                new DatabaseInitializer(Host.Services).Initialize().GetAwaiter().GetResult();
            }

            var actionName = GetType().Name;
            if (actionName.EndsWith("Test"))
                actionName = actionName[..^4];

            var controller = GetType().Namespace?.Split(".").Last();
            if (controller?.EndsWith("Test") ?? false)
                controller = controller[..^4];

            _uri = controller + "/" + actionName;
        }

        private void ConfigureWebHost()
        {
            var hostBuilder = new HostBuilder()
                .ConfigureWebHost(webHost =>
                {
                    // Add TestServer
                    webHost.UseTestServer();
                    webHost.UseStartup<Startup>();
                    webHost.UseConfiguration(new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json", true)
                        .Build());

                    // configure the services after the startup has been called.
                    webHost.ConfigureTestServices(services =>
                    {
                        // Remove the app's ApplicationDbContext registration.
                        var descriptor = services.SingleOrDefault(d
                            => d.ServiceType == typeof(DbContextOptions<DatabaseContext>));

                        if (descriptor != null)
                            services.Remove(descriptor);

                        // Add In memory DataBase for testing
                        services.AddDbContext<DatabaseContext>(options =>
                        {
                            options.UseInMemoryDatabase("InMemoryDbForTesting");
                        });

                        services.AddHangfire(config =>
                            config.UseInMemoryStorage());
                    });
                });

            // Build and start the IHost
            Host = hostBuilder.Start();
        }

        protected async Task<HttpResponseMessage> Fetch(dynamic data, ErrorCode errorCode = ErrorCode.NoError,
            HttpClient client = null)
        {
            client ??= Host.GetTestClient();

            HttpResponseMessage response = await client.PostAsync(_uri, JsonContent.Create(data));

            _outputHelper.WriteLine(await response.GetContent());

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal($"{(short) errorCode}", await response.GetErrorCode());

            return response;
        }
    }
}