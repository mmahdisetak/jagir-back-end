using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Application.ViewModels.Item;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Item
{
    [Collection("Jagir")]
    public class GetEventItemsTest : AppFactory
    {
        private readonly HttpClient _client;

        public GetEventItemsTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToUser().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData("event1")]
        [InlineData("event2")]
        [InlineData("event3")]
        [InlineData("invalidEventId", ErrorCode.EventNotFound)]
        public async Task<string> GetEventItems(String eventId, ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new GetEventItemsViewModel.Request
            {
                EventId = eventId
            };
            var response = await Fetch(data, errorCode, _client);
            return (await response.Content.ReadFromJsonAsync<GetEventItemsViewModel.Response>())?.Items?[0].Sessions[0]
                .Id;
        }
    }
}