using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Application.Dtos.Item;
using Application.ViewModels.Channel;
using Application.ViewModels.Item;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Item
{
    [Collection("Jagir")]
    public class CreateItemTest : AppFactory
    {
        private readonly HttpClient _client;
        private static readonly DateTime _now = DateTime.UtcNow;
        private static readonly TimeSpan _2Hour = TimeSpan.FromHours(2);
        private static readonly DateTime[] _schedules = {_now, _now + _2Hour, _now + 2 * _2Hour};

        public CreateItemTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToAdmin().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData("event2", "name", "desc", 30, 2, 200, null)]
        public async Task CreateItem(string eventId, string name, string description, int capacity, int limitPerUser,
            int price, DateTime[] schedules, ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new CreateItemViewModel.Request
            {
                EventId = eventId,
                Item = new CreateItemDto
                {
                    Name = name,
                    Description = description,
                    Capacity = capacity,
                    LimitPerUser = limitPerUser,
                    Price = price,
                    Schedules = _schedules
                }
            };
            await Fetch(data, errorCode, _client);
        }
    }
}