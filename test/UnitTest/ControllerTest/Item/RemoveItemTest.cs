using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.Item;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Item
{
    [Collection("Jagir")]
    public class RemoveItemTest : AppFactory
    {
        private readonly ITestOutputHelper _outputHelper;
        private readonly HttpClient _client;

        public RemoveItemTest(ITestOutputHelper outputHelper)
        {
            _outputHelper = outputHelper;
            _client = Host.GetTestClient();
        }
        
        [Fact]
        public async Task ShouldWorkCorrectly()
        {
            await _client.AuthToUser();
            var data = JsonConvert.SerializeObject(new RemoveItemViewModel.Request
            {
                Id = "item1"
            });
            var response = await Extensions.GetResponse(_client, data, Urls.Item.RemoveItem);
            await Extensions.Assertion(_outputHelper, response, HttpStatusCode.OK, ErrorCode.NoError);
        }
        
      /*  [Fact]
        public async Task InvalidItemId()
        {
            await _client.AuthToUser();
            var data = JsonConvert.SerializeObject(new RemoveItemViewModel.Request
            {
                Id = "invalidItemId"
            });
            var response = await Extensions.GetResponse(_client, data, Urls.Item.RemoveItem);
            await Extensions.Assertion(_outputHelper, response, HttpStatusCode.OK, ErrorCode.NoError);
        }*/
    }
}