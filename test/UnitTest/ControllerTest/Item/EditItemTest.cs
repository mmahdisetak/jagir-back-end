using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Application.Dtos.Item;
using Application.ViewModels.Item;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Item
{
    [Collection("Jagir")]
    public class EditItemTest : AppFactory
    {
        private readonly ITestOutputHelper _outputHelper;
        private readonly HttpClient _client;

        public EditItemTest(ITestOutputHelper outputHelper)
        {
            _outputHelper = outputHelper;
            _client = Host.GetTestClient();
        }
        
        [Fact]
        public async Task ShouldWorkCorrectly()
        {
            SessionDto session1 = new SessionDto() { Id = "item1",Booked = 0,Schedule = new DateTime()};
            SessionDto session2 = new SessionDto() { Id = "item1",Booked = 1,Schedule = new DateTime()};
            await _client.AuthToUser();
            var data = JsonConvert.SerializeObject(new EditItemViewModel.Request
            {
                Item = new ItemDto()
                {
                    Name = "newName",
                    Description = "newDesc",
                    Capacity = 3,
                    LimitPerUser = 2,
                    Price = 2,
                    Sessions = new[] {session1,session2}
                },
            });
            var response = await Extensions.GetResponse(_client, data, Urls.Item.EditItem);
            await Extensions.Assertion(_outputHelper, response, HttpStatusCode.OK, ErrorCode.NoError);
        }
        
        [Fact]
        public async Task InvalidITemId()
        {
            SessionDto sessionDto = new SessionDto() { Id = "invalidItemId",Booked = 0,Schedule = new DateTime()};
            await _client.AuthToUser();
            var data = JsonConvert.SerializeObject(new EditItemViewModel.Request
            {
                Item = new ItemDto()
                {
                    Name = "newName",
                    Description = "newDesc",
                    Capacity = 3,
                    LimitPerUser = 2,
                    Price = 2,
                    Sessions = new[] {sessionDto}
                },
            });
            var response = await Extensions.GetResponse(_client, data, Urls.Item.EditItem);
            await Extensions.Assertion(_outputHelper, response, HttpStatusCode.OK, ErrorCode.ItemNotFound);
        }
    }
}