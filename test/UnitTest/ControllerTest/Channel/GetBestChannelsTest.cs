using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.Channel;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Channel
{
    [Collection("Jagir")]
    public class GetBestChannelsTest : AppFactory
    {
        private readonly HttpClient _client;

        public GetBestChannelsTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToAdmin().Wait();
            }
            else
                _client = client;
        }
        
        [Theory]
        [InlineData(2)] // Should work correctly
        public async Task GetBestChannels(int size, ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new GetBestChannelsViewModel.Request
            {
                Size = size
            };
            await Fetch(data, errorCode, _client);
        }
        
    }
}