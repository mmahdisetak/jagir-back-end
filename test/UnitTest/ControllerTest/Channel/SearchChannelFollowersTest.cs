﻿using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.Channel;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Channel
{
    [Collection("Jagir")]
    public class SearchChannelFollowersTest : AppFactory
    {
        private readonly HttpClient _client;

        public SearchChannelFollowersTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToAdmin().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData("channel1", "", 10, 1)] // Should return all channel1 followers
        [InlineData("channel1", "User", 3, 1)] // Should return channel1 followers 1st page with keyword 'User' with 3 search results in each page
        [InlineData("channel1", "User", 3, 2)] // Should return channel1 followers 2nd page with keyword 'User' with 3 search results in each page
        public async Task SearchChannelFollowers(string channelId, string searchKey, int pageSize, int pageNumber,
            ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new SearchChannelFollowersViewModel.Request
            {
                ChannelId = channelId,
                SearchKey = searchKey,
                PageSize = pageSize,
                PageNumber = pageNumber
            };
            await Fetch(data, errorCode, _client);
        }
    }
}