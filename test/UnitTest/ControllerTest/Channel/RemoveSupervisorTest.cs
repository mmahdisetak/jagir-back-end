using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.Channel;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Channel
{
    [Collection("Jagir")]
    public class RemoveSupervisorTest : AppFactory
    {
        private readonly HttpClient _client;

        public RemoveSupervisorTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToAdmin().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        // [InlineData("channel1", "channelSupervisor")] //Should work correctly
        [InlineData("channel1", "InvalidUserID", ErrorCode.UserNotFound)] //User not found
        public async Task RemoveSupervisor(string channelId, string userId, ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new ChangeSupervisorViewModel.Request
            {
                ChannelId = channelId,
                UserId = userId
            };
            await Fetch(data, errorCode, _client);
        }
    }
}