﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Application.ViewModels.Channel;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Channel
{
    [Collection("Jagir")]
    public class CreateChannelTest : AppFactory
    {
        private readonly HttpClient _client;

        public CreateChannelTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToAdmin().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData("ChannelCreationWorksCorrectly", "GodChannelTest", "user1", null)] //ShouldWorkCorrectly
        [InlineData("ChannelCreationWorksCorrectly", "GodChannelTest", "user1",
            new string[] {"JagirTestUser0Id", "JagirTestUser1Id"})] // Adding multiple supervisors
        [InlineData("ChannelCreationWorksCorrectly", "GodChannelTest", "user1",
            new string[] {"JagirTestUser0Id", "JagirTestUser1"},
            ErrorCode.UserNotFound)] // Invalid user Id among supervisors Ids
        [InlineData("ChannelCreationWorksCorrectly", "GodChannelTest", "user1", new[] {"user4"})] //ShouldWorkCorrectly
        [InlineData("ChannelCreationUserNotFound", "BadChannelTest", "user1", new[] {"InvalidUserID"},
            ErrorCode.UserNotFound)] //UserNotFoundTest
        [InlineData("InvalidOwnerId", "BadChannelTest", "InvalidUserID", new string[] { },
            ErrorCode.UserNotFound)] //InvalidOwnerId
        [InlineData("BadChannelTest", "BadChannelTest", "", new string[] { },
            ErrorCode.InvalidInput)] //Empty owner Id
        [InlineData("BadChannelTest", "BadChannelTest", null, new string[] { },
            ErrorCode.InvalidInput)] //Null owner Id
        public async Task<string> CreateChannel(string name, string description, string ownerId, string[] supervisorsId,
            ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new CreateChannelViewModel.Request
            {
                Name = name,
                Description = description,
                OwnerId = ownerId,
                SupervisorsId = supervisorsId
            };

            var response = await Fetch(data, errorCode, _client);
            return (await response.Content.ReadFromJsonAsync<CreateChannelViewModel.Response>())?.Id;
        }

        // [Theory]
        // [InlineData("ChannelCreationWorksCorrectly", "GodChannelTest", "user1", null,ErrorCode.AccessDenied)]
        // public async Task AccessDenied(string name, string description, string ownerId, string[] supervisorsId,
        //     ErrorCode errorCode = ErrorCode.NoError)
        // {
        //     await _client.AuthToUser();
        //     await CreateChannel(name, description, ownerId, supervisorsId, errorCode);
        // }
    }
}