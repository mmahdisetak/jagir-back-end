﻿using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Application.Dtos.Channel;
using Application.ViewModels.Channel;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Channel
{
    [Collection("Jagir")]
    public class GetChannelProfileTest : AppFactory
    {
        private readonly HttpClient _client;

        public GetChannelProfileTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            _client = client ?? Host.GetTestClient();
        }

        [Theory]
        [InlineData("channel1")] //ShouldWorkCorrectly
        [InlineData("ch1", ErrorCode.ChannelNotFound)] //ChannelNotFoundTest
        public async Task<ChannelProfileDto> GetChannelProfile(string id, ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new GetChannelProfileViewModel.Request
            {
                Id = id
            };

            var response = await Fetch(data, errorCode, _client);
            return (await response.Content.ReadFromJsonAsync<GetChannelProfileViewModel.Response>())?.Channel;
        }
    }
}