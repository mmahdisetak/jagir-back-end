using System.Net.Http;
using System.Threading.Tasks;
using Application.Dtos.Channel;
using Application.ViewModels.Channel;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Channel
{
    public class GetOwnedChannelsTest : AppFactory
    {
        private readonly HttpClient _client;

        public GetOwnedChannelsTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToAdmin().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData()] // ShouldWorkCorrectly
        public async Task GetOwnedChannels(ErrorCode errorCode = ErrorCode.NoError)
        {
            await Fetch(new { }, errorCode, _client);
        }
    }
}