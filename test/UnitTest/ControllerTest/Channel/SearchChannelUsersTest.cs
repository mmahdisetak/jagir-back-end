using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.Channel;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Channel
{
    public class SearchChannelUsersTest : AppFactory
    {
        private readonly HttpClient _client;

        public SearchChannelUsersTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToAdmin().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData("InvalidChannelId", "",ErrorCode.ChannelNotFound)] // Invalid channel Id
        [InlineData("channel1", "")] // should return all channel members
        [InlineData("channel1", "User")] // search in channel users
        public async Task SearchChannelUsers(string channelId, string searchKey,
            ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new SearchChannelUsersViewModel.Request
            {
               ChannelId = channelId,
               SearchKey = searchKey
            };
            await Fetch(data, errorCode, _client);
        }
    }
}