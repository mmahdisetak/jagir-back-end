using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.Channel;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;
namespace UnitTest.ControllerTest.Channel
{
    [Collection("Jagir")]
    public class RemoveChannelTest : AppFactory
    {
        
            private readonly HttpClient _client;

            public RemoveChannelTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
            {
                if (client == null)
                {
                    _client = Host.GetTestClient();
                    _client.AuthToAdmin().Wait();
                }
                else
                    _client = client;
            }
            [Theory]
            [InlineData("channel1")] //Should work correctly
            [InlineData("InvalidChannelId", ErrorCode.ChannelNotFound)] // Channel not found
            public async Task RemoveChannel(string channelId, ErrorCode errorCode = ErrorCode.NoError)
            {
                var data = new RemoveChannelViewModel.Request
                {
                    Id = channelId
                };
                await Fetch(data, errorCode, _client);
            }

    }
}