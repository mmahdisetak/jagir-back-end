﻿using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.Channel;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Channel
{
    [Collection("Jagir")]
    public class EditChannelProfileTest : AppFactory
    {
        private readonly HttpClient _client;

        public EditChannelProfileTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToAdmin().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData("channel1", "Good channel", "", "", "")] //Changing name only
        [InlineData("channel1", "", "New Description", "", "")] //Changing description only
        [InlineData("channel1", "Good channel", "New Description", "", "")] //Changing name and description at same time
        [InlineData(null, "Bad channel", "New Description", "", "", ErrorCode.ChannelNotFound)] //Null data
        [InlineData("ch1", "Bad channel", "New Description", "", "", ErrorCode.ChannelNotFound)] //wrong data
        [InlineData("channel1", "Not Good channel", "New Description", "2", "",ErrorCode.FileNotFound)] //Invalid avatar Id
        [InlineData("channel1", "Not Good channel", "New Description", "", "2", ErrorCode.FileNotFound)] //Invalid banner Id
        public async Task EditChannelProfile(string channelId, string name, string description, string avatarId,
            string bannerId, ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new EditChannelProfileViewModel.Request
            {
                ChannelId = channelId,
                Name = name,
                Description = description,
                AvatarId = avatarId,
                BannerId = bannerId
            };

            await Fetch(data, errorCode, _client);
        }
    }
}