using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.Event;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Event
{
    [Collection("Jagir")]
    public class RemoveEventTest : AppFactory
    {
        private readonly HttpClient _client;

        public RemoveEventTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToAdmin().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData("event1")]// Should work correctly
        [InlineData("InvalidEventId",ErrorCode.EventNotFound)]// Invalid event Id
        public async Task RemoveEvent(string eventId, ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new GetChannelEventsViewModel.Request
            {
                Id = eventId
            };
            await Fetch(data, errorCode, _client);
        }

    }
}