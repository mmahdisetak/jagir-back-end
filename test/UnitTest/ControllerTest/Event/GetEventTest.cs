using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Application.Dtos.Event;
using Application.ViewModels.Event;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Event
{
    [Collection("Jagir")]
    public class GetEventTest : AppFactory
    {
        private readonly HttpClient _client;

        public GetEventTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            _client = client ?? Host.GetTestClient();
        }

        [Theory]
        [InlineData("event1")] //ShouldWorkCorrectly
        [InlineData("event2")] //ShouldWorkCorrectly
        [InlineData("event3")] //ShouldWorkCorrectly
        [InlineData("invalidEventId", ErrorCode.EventNotFound)] //EventNotFoundTest
        public async Task<EventDto> GetEvent(string id, ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new GetEventViewModel.Request
            {
                Id = id
            };

            var response = await Fetch(data, errorCode, _client);
            return (await response.Content.ReadFromJsonAsync<GetEventViewModel.Response>())?.Event;
        }
    }
}