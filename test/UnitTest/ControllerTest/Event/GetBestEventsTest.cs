using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Application.ViewModels.Event;
using Domain.Enums;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Event
{
    [Collection("Jagir")]
    public class GetBestEventsTest : AppFactory
    {
        private readonly HttpClient _client;

        public GetBestEventsTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            _client = client;
        }

        [Theory]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(300)]
        public async Task<string> GetBestEvents(int size, ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new GetBestEventsViewModel.Request
            {
                Size = size
            };

            var response = await Fetch(data, errorCode, _client);
            return (await response.Content.ReadFromJsonAsync<GetBestEventsViewModel.Response>())?.Events[0].Id;
        }
    }
}