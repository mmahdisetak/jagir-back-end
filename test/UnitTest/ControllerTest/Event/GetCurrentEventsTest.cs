using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Application.ViewModels.Comment;
using Application.ViewModels.Event;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Event
{
    [Collection("Jagir")]
    public class GetCurrentEventsTest : AppFactory
    {
        private readonly HttpClient _client;

        public GetCurrentEventsTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToUser().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData("channel1")] // Should work correctly
        [InlineData("invalidChannel", ErrorCode.ChannelNotFound)] // Should work correctly
        public async Task GetCurrentEvents(string channelId, ErrorCode errorCode = ErrorCode.NoError)
        {
            await _client.AuthToUser();
            var data = new GetChannelEventsViewModel.Request
            {
                Id = channelId
            };
            await Fetch(data, errorCode, _client);
        }
    }
}