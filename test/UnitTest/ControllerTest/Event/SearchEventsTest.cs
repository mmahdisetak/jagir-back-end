using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.Event;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Event
{
    [Collection("Jagir")]
    public class SearchEventsTest : AppFactory
    {
        private readonly HttpClient _client;

        public SearchEventsTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToUser().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData("channel1", "", EventType.Casual, EventTime.Passed, 1, 10)] // Should work correctly
        [InlineData("channel1", "description", EventType.Casual, EventTime.Passed, 1, 10)] // Should work correctly
        [InlineData("channel1", "AbsentKeyword", EventType.Casual, EventTime.Passed, 1, 10)] // Should work correctly
        [InlineData("channel1", "AbsentKeyword", EventType.Casual, EventTime.Passed, 2, 10)] // Should work correctly
        [InlineData("", "", EventType.Casual, EventTime.Future, 1, 10)] //Upcoming full search
        [InlineData("", "", EventType.Casual, EventTime.Passed, 1, 10)] //Passed full search
        [InlineData("", "", EventType.Weekly, EventTime.Running, 1, 10)] //Weekly running full search
        public async Task SearchEvents(string channelId, string key,
            EventType type, EventTime filter, int pageNumber, int pageSize, ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new SearchEventsViewModel.Request
            {
                ChannelId = channelId,
                Key = key,
                Type = type,
                Filter =filter,
                PageNumber = pageNumber,
                PageSize = pageSize
            };
            await Fetch(data, errorCode, _client);
        }
    }
}