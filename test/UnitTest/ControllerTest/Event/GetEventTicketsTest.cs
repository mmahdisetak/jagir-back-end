using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Application.ViewModels.Event;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;
using Application.Dtos.Event;
namespace UnitTest.ControllerTest.Event
{
    [Collection("Jagir")]
    public class GetEventTicketsTest : AppFactory
    {
        private readonly HttpClient _client;

        public GetEventTicketsTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToAdmin().Wait();
            }
            else
                _client = client;
        }
        
        [Theory]
        [InlineData("event1")]// Should work correctly
        [InlineData("event2")]// Should work correctly
        [InlineData("event3")]// Should work correctly
        [InlineData("InvalidEventId",ErrorCode.EventNotFound)]// Invalid event Id
        public async Task GetEventTickets(string eventId, ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new GetEventTicketsViewModel.Request
            {
                EventId = eventId
            };
            await Fetch(data, errorCode, _client);
        }

    }
}