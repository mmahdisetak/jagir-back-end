﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Application.ViewModels.Event;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;
using Application.Dtos.Event;

namespace UnitTest.ControllerTest.Event
{
    [Collection("Jagir")]
    public class CreateEventTest : AppFactory
    {
        private readonly HttpClient _client;

        public CreateEventTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToAdmin().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData("channel1", "GoodEvent", "CoolEvent", true, true, null, null)] //ShouldWorkCorrectly
        [InlineData("NotFoundChannel", "GoodEvent", "CoolEvent", true, true, null, null,
            ErrorCode.ChannelNotFound)] //ChannelNotFound
        public async Task<string> CreateEvent(string channelId, string name, string description, bool weeklyReset,
            bool isActive, DateTime? startingDate, DateTime? endingDate, ErrorCode errorCode = ErrorCode.NoError)
        {
            startingDate ??= DateTime.UtcNow;
            endingDate ??= DateTime.UtcNow;
            var data = new CreateEventViewModel.Request
            {
                ChannelId = channelId,
                Event = new EventDto
                {
                    Name = name,
                    Description = description,
                    WeeklyReset = weeklyReset,
                    IsActive = isActive,
                    StartingDate = startingDate,
                    EndingDate = endingDate
                }
            };

            var response = await Fetch(data, errorCode, _client);
            return (await response.Content.ReadFromJsonAsync<CreateEventViewModel.Response>())?.Id;
        }
    }
}