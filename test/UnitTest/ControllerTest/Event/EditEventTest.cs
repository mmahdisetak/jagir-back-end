using System;
using System.Net.Http;
using System.Threading.Tasks;
using Application.Dtos.Event;
using Application.ViewModels.Event;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Event
{
    [Collection("Jagir")]
    public class EditEventTest : AppFactory
    {
        private readonly HttpClient _client;

        public EditEventTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToAdmin().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData("event1", "newEvent",null, "EventDesc", true, true, null, null)] //ShouldWorkCorrectly
        [InlineData("event1", null, null,null, null, null, null, null)] //ShouldWorkCorrectly
        [InlineData("NotFoundEvent", null,null, null, null, null, null, null, ErrorCode.EventNotFound)] //EventNotFound
        [InlineData("event1", null,"InvalidAvatarId", null, null, null, null, null, ErrorCode.FileNotFound)] //FileNotFound
        public async Task EditEvent(string eventId, string name,string avatarId, string description, bool? weeklyReset,
            bool? isActive, DateTime? startingDate, DateTime? endingDate, ErrorCode errorCode = ErrorCode.NoError)
        {
            startingDate ??= DateTime.UtcNow;
            endingDate ??= DateTime.UtcNow;
            var data = new EditEventViewModel.Request
            {
                Id = eventId,
                Event = new EventDto
                {
                    Name = name,
                    AvatarId = avatarId,
                    Description = description,
                    WeeklyReset = weeklyReset,
                    IsActive = isActive,
                    StartingDate = startingDate,
                    EndingDate = endingDate
                }
            };

            await Fetch(data, errorCode, _client);
        }
    }
}