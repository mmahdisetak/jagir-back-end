using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.Event;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Event
{
    [Collection("Jagir")]
    public class GetPassedWeeklyEventsTest : AppFactory
    {
        private readonly HttpClient _client;

        public GetPassedWeeklyEventsTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToUser().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData("channel1")]
        [InlineData("invalidChannel", ErrorCode.ChannelNotFound)]
        public async Task GetPassedWeeklyEvents(string channelId, ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new GetChannelEventsViewModel.Request
            {
                Id = channelId
            };
            await Fetch(data, errorCode, _client);
        }
    }
}