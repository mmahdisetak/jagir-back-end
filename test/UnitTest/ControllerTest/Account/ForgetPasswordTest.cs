using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.Account;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Account
{
    [Collection("Jagir")]
    public class ForgetPasswordTest : AppFactory
    {
        private readonly HttpClient _client;

        public ForgetPasswordTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            _client = client ?? Host.GetTestClient();
        }

        [Theory]
        [InlineData(SignInType.Email,"TestUser@Jagir.ir")] // successful with email as login
        [InlineData(SignInType.UserName,"User1")] // successful with username as login 
        [InlineData(SignInType.Email,"",ErrorCode.InvalidInput)] // Empty email 
        [InlineData(SignInType.UserName,"",ErrorCode.InvalidInput)] // Empty username
        [InlineData(SignInType.Email,"testEmailInvalid",ErrorCode.UserNotFound)] // Invalid email
        [InlineData(SignInType.UserName,"testUsernameInvalid",ErrorCode.UserNotFound)] // Invalid username
        [InlineData(SignInType.Email,"UnconfirmedEmail@Jagir.ir",ErrorCode.UnConfirmedEmail)] // Unconfirmed email by email
        [InlineData(SignInType.UserName,"User2",ErrorCode.UnConfirmedEmail)] // Unconfirmed email by username
        public async Task ForgetPassword(SignInType signInType, string login,
            ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new ForgetPasswordViewModel.Request
            {
                Type = signInType,
                Login = login
            };
            await Fetch(data, errorCode, _client);
        }
    }
}