﻿using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.Account;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Account
{
    [Collection("Jagir")]
    public class SignInTest : AppFactory
    {
        private readonly HttpClient _client;

        public SignInTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            _client = client ?? Host.GetTestClient();
        }
        
        [Theory]
        [InlineData(SignInType.Email, "TestUser@Jagir.ir", "TestPassword")] //ShouldWorkCorrectly
        [InlineData(SignInType.Email, "JagirTestUser5@Jagir.ir", "TestPassword")] //ShouldWorkCorrectly
        [InlineData(SignInType.Email, "", "TestPassword", ErrorCode.InvalidInput)] //EmptyLoginInput
        [InlineData(SignInType.Email, "TestUser3@Jagir.ir", "", ErrorCode.InvalidInput)] //EmptyPasswordInput
        [InlineData(SignInType.Email, "TestUser3@Jagir.ir", "WrongPassword", ErrorCode.SignInFailed)] //WrongPassword1
        [InlineData(SignInType.Email, "TestUser@Jagir.ir", "WrongPassword", ErrorCode.SignInFailed)] //WrongPassword2
        [InlineData(SignInType.UserName, "NotExistingsUser", "TestPassword", ErrorCode.SignInFailed)] //UserDoesNotExistTest
        [InlineData(SignInType.Email, "UnconfirmedEmail@Jagir.ir", "TestPassword", ErrorCode.UnConfirmedEmail)] //UnconfirmedEmailTest
        
        public async Task SignIn(SignInType type, string login, string password,
            ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new SignInViewModel.Request
            {
                Type = type,
                Login = login,
                Password = password
            };

            await Fetch(data, errorCode, _client);
        }
    }
}