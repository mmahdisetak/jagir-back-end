using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.Account;
using Domain.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Account
{
    [Collection("Jagir")]
    public class ResetPasswordTest : AppFactory
    {
        private readonly ITestOutputHelper _outputHelper;
        private readonly HttpClient _client;

        public ResetPasswordTest(ITestOutputHelper outputHelper)
        {
            _outputHelper = outputHelper;
            _client = Host.GetTestClient();
        }
        
        [Fact]
        public async Task InvalidToken()
        {
            var resetPasswordData = JsonConvert.SerializeObject(new ResetPasswordViewModel.Request
            {
                Email = "UnconfirmedEmail@Jagir.ir",
                Token = "not valid token",
                NewPassword = "new"
            });

            var response = await Extensions.GetResponse(_client, resetPasswordData, Urls.Account.ResetPassword);
            await Extensions.Assertion(_outputHelper, response, HttpStatusCode.OK, ErrorCode.InvalidToken);
        } 
        
        [Fact]
        public async Task EmptyTokenInput()
        {
            var resetPasswordData = JsonConvert.SerializeObject(new ResetPasswordViewModel.Request
            {
                Email = "UnconfirmedEmail@Jagir.ir",
                Token = "",
                NewPassword = "new"
            });

            var response = await Extensions.GetResponse(_client, resetPasswordData, Urls.Account.ResetPassword);
            await Extensions.Assertion(_outputHelper, response, HttpStatusCode.OK, ErrorCode.InvalidInput);
        }
    }
}