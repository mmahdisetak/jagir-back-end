using System;
using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.Account;
using Domain.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Account
{
    [Collection("Jagir")]
    public class ConfirmEmailTest : AppFactory
    {
        private readonly HttpClient _client;

        public ConfirmEmailTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData("", "token",ErrorCode.InvalidInput)] // Empty email
        [InlineData("InvalidEmailRegex@", "token",ErrorCode.InvalidInput)] // Invalid email regex
        [InlineData("UnconfirmedEmail@Jagir.ir", null,ErrorCode.InvalidInput)] // Null token
        [InlineData("UnconfirmedEmail@Jagir.ir", "",ErrorCode.InvalidInput)] // Empty token
        [InlineData("UnconfirmedEmail@Jagir.ir", "InvalidToken",ErrorCode.InvalidToken)] // Invalid token
        [InlineData("TestUser3@Jagir.ir", "token",ErrorCode.EmailAlreadyConfirmed)] // Email address already confirmed
        [InlineData("InvalidUserEmail@Jagir.ir", "token",ErrorCode.UserNotFound)] // Invalid email address
        public async Task ConfirmEmail(string email, string token, ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new ConfirmEmailViewModel.Request
            {
                Email = email,
                Token = token
            };
            await Fetch(data, errorCode, _client);
        }
    }
}