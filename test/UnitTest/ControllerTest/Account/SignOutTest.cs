using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.Account;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Account
{
    [Collection("Jagir")]
    public class SignOutTest : AppFactory
    {
        private readonly ITestOutputHelper _outputHelper;
        private readonly HttpClient _client;

        public SignOutTest(ITestOutputHelper outputHelper)
        {
            _outputHelper = outputHelper;
            _client = Host.GetTestClient();
        }
        
        [Fact]
        public async Task ShouldWorkCorrectly()
        {
            await _client.AuthToUser();
            var signOutData = JsonConvert.SerializeObject(new SignOutViewModel());

            var response = await Extensions.GetResponse(_client, signOutData, Urls.Account.SignOut);
            await Extensions.Assertion(_outputHelper, response, HttpStatusCode.OK, ErrorCode.NoError);
        } 

    }
}