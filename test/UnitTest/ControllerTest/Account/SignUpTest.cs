﻿using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.Account;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Account
{
    [Collection("Jagir")]
    public class SignUpTest : AppFactory
    {
        private readonly HttpClient _client;

        public SignUpTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            _client = client ?? Host.GetTestClient();
        }

        [Theory]
        [InlineData("TestUserShouldSignUpCorrectly", "shouldWorkCorrectly@Jagir.ir", "TestPassword")] // Should work correctly
        [InlineData("EmptyEmailTestUser", "", "TestPassword",ErrorCode.InvalidInput)] // White space email
        [InlineData("NullEmailTestUser", null, "TestPassword",ErrorCode.InvalidInput)] // Null email
        [InlineData("", "shouldWorkCorrectly@Jagir.ir", "TestPassword", ErrorCode.InvalidInput)]// White space username
        [InlineData(null, "shouldWorkCorrectly@Jagir.ir", "TestPassword",ErrorCode.InvalidInput)]// Null username
        [InlineData("TestUser", "TestUser@Jagir.ir", "TestPassword", ErrorCode.DuplicatedInput)]
        [InlineData("InvalidEmailFormatTest", "InvalidEmailTestUserJagirir", "TestPassword", ErrorCode.InvalidInput)] // Invalid email regex
        [InlineData("EmptyPasswordUser", "EmptyPasswordTestUserJagir@Jagir.ir", "", ErrorCode.InvalidInput)] // Empty Password
        [InlineData("NullPasswordUser", "NullPasswordTestUserJagir@Jagir.ir", null, ErrorCode.InvalidInput)] // Null Password
        [InlineData("WeekPasswordUser", "WeekPasswordTestUserJagir@Jagir.ir", "week", ErrorCode.InvalidInput)] // Week Password
        public async Task SignUp(string userName, string email, string password,
            ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new SignUpViewModel.Request
            {
                UserName = userName,
                Email = email,
                Password = password
            };

            await Fetch(data, errorCode, _client);
        }
    }
}