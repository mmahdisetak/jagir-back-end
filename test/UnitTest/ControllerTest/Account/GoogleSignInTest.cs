using System.Net.Http;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Account
{
    [Collection("Jagir")]
    public class GoogleSignInTest : AppFactory
    {
        private readonly ITestOutputHelper _outputHelper;
        private readonly HttpClient _client;

        public GoogleSignInTest(ITestOutputHelper outputHelper)
        {
            _outputHelper = outputHelper;
            _client = Host.GetTestClient();
        }
    }
}