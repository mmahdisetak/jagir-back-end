using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.TestHost;
using Domain.Enums;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;
using Application.ViewModels.User;

namespace UnitTest.ControllerTest.User
{
    [Collection("Jagir")]
    public class SearchChannelsEventsTest : AppFactory
    {
        private readonly HttpClient _client;

        public SearchChannelsEventsTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToUser().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData("channel1","",5)] // Searching without keyword
        [InlineData("channel1","event2",5)] // Testing match finding
        [InlineData("channel1","eVent",5)] // Testing case sensitivity
        [InlineData("channel1","event",2)] // Testing limit count
        [InlineData(null,"event",2)] // Testing full search for coverage
        public async Task SearchChannelsEvents(string channelId, string key, int size,
            ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new SearchChannelsEventsViewModel.Request
            {
                ChannelId = channelId,
                Key = key,
                Size = size
            };
            await Fetch(data, errorCode, _client);
        }
    }
}