﻿using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.User;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.User
{
    [Collection("Jagir")]
    public class EditProfileTest : AppFactory
    {
        private readonly HttpClient _client;

        public EditProfileTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToAdmin().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData("newFirstName", "newLastName", "new", "", null)]
        [InlineData("newFirstName", "newLastName", "new", "channelSupervisor@Jagir.ir", null, ErrorCode.DuplicatedInput)]// Duplicated email
        [InlineData("newFirstName", "newLastName", "user1", "", null, ErrorCode.DuplicatedInput)]// Duplicated username
        public async Task EditProfile(string firstName, string lastName, string userName, string email, string avatarId,
            ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new EditProfileViewModel.Request
            {
                FirstName = firstName,
                LastName = lastName,
                UserName = userName,
                Email = email,
                AvatarId = avatarId
            };
            await Fetch(data, errorCode, _client);
        }
    }
}