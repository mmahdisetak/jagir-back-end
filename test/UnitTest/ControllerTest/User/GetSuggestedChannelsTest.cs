using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.TestHost;
using Domain.Enums;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.User
{
    [Collection("Jagir")]
    public class GetSuggestedChannelsTest : AppFactory
    {
        private readonly HttpClient _client;

        public GetSuggestedChannelsTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToUser().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData()] // Should work correctly
        public async Task GetUser1SuggestedChannels(ErrorCode errorCode = ErrorCode.NoError)
        {
            await Fetch(new { }, errorCode, _client);
        }
    }
}