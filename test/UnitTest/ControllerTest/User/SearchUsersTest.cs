using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.User;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.User
{
    [Collection("Jagir")]
    public class SearchUsersTest : AppFactory
    {
        private readonly HttpClient _client;

        public SearchUsersTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToAdmin().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData("", 2)] // Search count limit test
        [InlineData("", 5)] // Search count limit test
        [InlineData("User", 5)] // keyword test
        public async Task SearchUsers(string searchKey, int size, ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new SearchUsersViewModel.Request
            {
                SearchKey = searchKey,
                Size = size
            };

            await Fetch(data, errorCode, _client);
        }
    }
}