using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.User;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.User
{
    [Collection("Jagir")]
    public class IncreaseCreditTest : AppFactory
    {
        private readonly HttpClient _client;

        public IncreaseCreditTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToUser2().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData(10000)] // Should work correctly
        [InlineData(500001, ErrorCode.InvalidCredit)] // Credit more than 500000
        [InlineData(-1, ErrorCode.InvalidCredit)] // Negative credit
        public async Task IncreaseCredit(long credit, ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new IncreaseCreditViewModel.Request
            {
                Credit = credit
            };

            await Fetch(data, errorCode, _client);
        }
    }
}