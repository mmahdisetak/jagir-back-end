﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.User;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.User
{
    [Collection("Jagir")]
    public class FollowChannelTest : AppFactory
    {
        private readonly HttpClient _client;

        public FollowChannelTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToAdmin().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData("channel1",true)]
        [InlineData("InvalidChannelId",true,ErrorCode.ChannelNotFound)]
        public async Task FollowChannel(string channelId, bool status, ErrorCode errorCode = ErrorCode.NoError)
        {

            var data = new FollowChannelViewModel.Request
            {
                ChannelId = channelId,
                Status = status,
            };

            await Fetch(data, errorCode, _client);
        }
    }
}