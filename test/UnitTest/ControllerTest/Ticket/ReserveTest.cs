using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.Ticket;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Ticket
{
    [Collection("Jagir")]
    public class ReserveTest : AppFactory
    {
        private readonly HttpClient _client;

        public ReserveTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToUser().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData("", new string[] {"item1", "item2"})]
        [InlineData("", new string[] {"item1", "InvalidItemId"}, ErrorCode.ItemNotFound)]
        public async Task Reserve(string s, string[] itemsIds, ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new ReserveViewModel.Request
            {
                ItemIds = itemsIds
            };
            await Fetch(data, errorCode, _client);
        }

        [Theory]
        [InlineData(new string[] {"item1", "item2"}, ErrorCode.NotEnoughCredit)]
        public async Task NotEnoughCredit(string[] itemsIds, ErrorCode errorCode = ErrorCode.NoError)
        {
            await _client.AuthToUser2();
            var data = new ReserveViewModel.Request
            {
                ItemIds = itemsIds
            };
            await Fetch(data, errorCode, _client);
        }
    }
}