using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.Ticket;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace UnitTest.ControllerTest.Ticket
{
    [Collection("Jagir")]
    public class GetUserTicketsTest : AppFactory
    {
        private readonly HttpClient _client;

        public GetUserTicketsTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToUser().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData()]
        public async Task GetUser1Tickets(ErrorCode errorCode = ErrorCode.NoError)
        {
            await Fetch(new { }, errorCode, _client);
        }

        [Theory]
        [InlineData()]
        public async Task GetUser2Tickets(ErrorCode errorCode = ErrorCode.NoError)
        {
            await _client.AuthToUser2();
            await Fetch(new { }, errorCode, _client);
        }
    }
}