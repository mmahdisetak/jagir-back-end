using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.Item;
using Application.ViewModels.Ticket;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Ticket
{
    [Collection("Jagir")]
    public class CancelTest : AppFactory
    {
        private readonly HttpClient _client;

        public CancelTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToUser().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData("ticket1")] // Should work correctly
        [InlineData("InvalidTicketId",ErrorCode.TicketNotFound)] // Invalid ticket Id
        public async Task Cancel(string ticketId, ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new CancelViewModel.Request
            {
                Id = ticketId
            };
            await Fetch(data, errorCode, _client);
        }
    }
}