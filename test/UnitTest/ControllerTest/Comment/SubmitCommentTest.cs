using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Application.ViewModels.Comment;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Comment
{
    [Collection("Jagir")]
    public class SubmitCommentTest : AppFactory
    {
        private readonly HttpClient _client;

        public SubmitCommentTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToUser2().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData("event1", "Awli!")]
        [InlineData("invalidEvent", "Ay baba!",ErrorCode.EventNotFound)]
        public async Task SubmitComment(string eventId, string content, ErrorCode errorCode = ErrorCode.NoError)
        {
            await _client.AuthToUser();
            var data = new SubmitCommentViewModel.Request
            {
                EventId = eventId,
                Content = content
            };
            await Fetch(data, errorCode, _client);
        }
    }
}