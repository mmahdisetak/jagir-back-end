﻿using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Application.ViewModels.Comment;
using Domain.Enums;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using UnitTest.Utilities;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest.ControllerTest.Comment
{
    [Collection("Jagir")]
    public class GetEventCommentsTest : AppFactory
    {
        private readonly HttpClient _client;

        public GetEventCommentsTest(ITestOutputHelper outputHelper, HttpClient client = null) : base(outputHelper)
        {
            if (client == null)
            {
                _client = Host.GetTestClient();
                _client.AuthToUser2().Wait();
            }
            else
                _client = client;
        }

        [Theory]
        [InlineData("event1")]
        [InlineData("event2")]
        [InlineData("event3")]
        [InlineData(null,ErrorCode.EventNotFound)] // Null data
        [InlineData("InvalidEventId", ErrorCode.EventNotFound)]
        public async Task GetEventComments(string eventId, ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new GetEventCommentsViewModel.Request
            {
                EventId = eventId
            };
            await Fetch(data, errorCode, _client);
        }
    }
}