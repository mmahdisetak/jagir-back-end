﻿using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Application.ViewModels.Account;
using Domain.Enums;
using MarkopTest.Handler;
using Newtonsoft.Json;

namespace LoadTest.Handlers
{
    public class AdminHandler : TestHandler
    {
        public override async Task Before(HttpClient client)
        {
            var userObj = new SignInViewModel.Request
            {
                Login = "TestAdmin@Jagir.ir",
                Type = SignInType.Email,
                Password = "AdminPassword"
            };
            var user = JsonConvert.SerializeObject(userObj);

            var response = await client.PostAsync("Account/SignIn",
                new StringContent(user, Encoding.UTF8, "application/json"));

            client.DefaultRequestHeaders.Add("Cookie", response.Headers.GetValues("Set-Cookie").ToArray()[0]);
        }
    }
}