using System.Threading.Tasks;
using LoadTest.Handlers;
using Xunit;
using Xunit.Abstractions;

namespace LoadTest.Controller.Channel
{
    public class GetOwnedChannelsTest : AppFactory
    {
        public GetOwnedChannelsTest(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }

        [Theory]
        [AdminHandler]
        [InlineData] // ShouldWorkCorrectly
        public async Task GetOwnedChannels()
        {
            await PostJsonAsync(new { });
        }
    }
}