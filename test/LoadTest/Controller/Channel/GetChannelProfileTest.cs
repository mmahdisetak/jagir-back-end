﻿using System.Threading.Tasks;
using Application.ViewModels.Channel;
using Domain.Enums;
using Xunit;
using Xunit.Abstractions;

namespace LoadTest.Controller.Channel
{
    [Collection("Jagir")]
    public class GetChannelProfileTest : AppFactory
    {
        public GetChannelProfileTest(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }

        [Theory]
        [InlineData("channel1")] //ShouldWorkCorrectly
        [InlineData("ch1")] //ChannelNotFoundTest
        public async Task GetChannelProfile(string id)
        {
            var data = new GetChannelProfileViewModel.Request
            {
                Id = id
            };

            await PostJsonAsync(data);
        }
    }
}