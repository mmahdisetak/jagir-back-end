﻿using System.Threading.Tasks;
using Application.ViewModels.Channel;
using LoadTest.Handlers;
using Xunit;
using Xunit.Abstractions;

namespace LoadTest.Controller.Channel
{
    [Collection("Jagir")]
    public class SearchChannelFollowersTest : AppFactory
    {
        public SearchChannelFollowersTest(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }

        [Theory]
        [AdminHandler]
        [InlineData("channel1", "", 10, 1)] // Should return all channel1 followers
        [InlineData("channel1", "User", 3,
            1)] // Should return channel1 followers 1st page with keyword 'User' with 3 search results in each page
        [InlineData("channel1", "User", 3,
            2)] // Should return channel1 followers 2nd page with keyword 'User' with 3 search results in each page
        public async Task SearchChannelFollowers(string channelId, string searchKey, int pageSize, int pageNumber)
        {
            var data = new SearchChannelFollowersViewModel.Request
            {
                ChannelId = channelId,
                SearchKey = searchKey,
                PageSize = pageSize,
                PageNumber = pageNumber
            };
            await PostJsonAsync(data);
        }
    }
}