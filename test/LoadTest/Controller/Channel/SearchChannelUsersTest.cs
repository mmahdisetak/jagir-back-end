using System.Threading.Tasks;
using Application.ViewModels.Channel;
using LoadTest.Handlers;
using Xunit;
using Xunit.Abstractions;

namespace LoadTest.Controller.Channel
{
    public class SearchChannelUsersTest : AppFactory
    {
        public SearchChannelUsersTest(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }

        [Theory]
        [AdminHandler]
        [InlineData("InvalidChannelId", "")] // Invalid channel Id
        [InlineData("channel1", "")] // should return all channel members
        [InlineData("channel1", "User")] // search in channel users
        public async Task SearchChannelUsers(string channelId, string searchKey)
        {
            var data = new SearchChannelUsersViewModel.Request
            {
                ChannelId = channelId,
                SearchKey = searchKey
            };
            await PostJsonAsync(data);
        }
    }
}