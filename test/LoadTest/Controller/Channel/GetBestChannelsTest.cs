﻿using System.Threading.Tasks;
using Application.ViewModels.Channel;
using LoadTest.Handlers;
using Xunit;
using Xunit.Abstractions;

namespace LoadTest.Controller.Channel
{
    [Collection("Jagir")]
    public class GetBestChannelsTest : AppFactory
    {
        public GetBestChannelsTest(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }

        [Theory]
        [UserHandler]
        [InlineData(2)] // Should work correctly
        public async Task GetBestChannels(int size)
        {
            var data = new GetBestChannelsViewModel.Request
            {
                Size = size
            };
            await PostJsonAsync(data);
        }
    }
}