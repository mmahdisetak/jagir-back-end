using System.Threading.Tasks;
using Application.ViewModels.Event;
using Xunit;
using Xunit.Abstractions;

namespace LoadTest.Controller.Event
{
    [Collection("Jagir")]
    public class GetBestEventsTest : AppFactory
    {
        public GetBestEventsTest(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }

        [Theory]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(300)]
        public async Task GetBestEvents(int size)
        {
            var data = new GetBestEventsViewModel.Request
            {
                Size = size
            };
            await PostJsonAsync(data);
        }
    }
}