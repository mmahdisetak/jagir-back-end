using System.Threading.Tasks;
using Application.ViewModels.Event;
using Xunit;
using Xunit.Abstractions;

namespace LoadTest.Controller.Event
{
    [Collection("Jagir")]
    public class GetEventTest : AppFactory
    {
        public GetEventTest(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }

        [Theory]
        [InlineData("event1")] //ShouldWorkCorrectly
        [InlineData("event2")] //ShouldWorkCorrectly
        [InlineData("event3")] //ShouldWorkCorrectly
        [InlineData("invalidEventId")] //EventNotFoundTest
        public async Task GetEvent(string id)
        {
            var data = new GetEventViewModel.Request
            {
                Id = id
            };

            await PostJsonAsync(data);
        }
    }
}