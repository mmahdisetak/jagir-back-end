using System.Threading.Tasks;
using Application.ViewModels.Event;
using Domain.Enums;
using LoadTest.Handlers;
using Xunit;
using Xunit.Abstractions;

namespace LoadTest.Controller.Event
{
    [Collection("Jagir")]
    public class GetEventTicketsTest : AppFactory
    {
        public GetEventTicketsTest(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }

        [Theory]
        [AdminHandler]
        [InlineData("event1")] // Should work correctly
        [InlineData("event2")] // Should work correctly
        [InlineData("event3")] // Should work correctly
        [InlineData("InvalidEventId", ErrorCode.EventNotFound)] // Invalid event Id
        public async Task GetEventTickets(string eventId, ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new GetEventTicketsViewModel.Request
            {
                EventId = eventId
            };
            await PostJsonAsync(data);
        }
    }
}