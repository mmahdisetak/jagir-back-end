using System.Threading.Tasks;
using Application.ViewModels.Event;
using Domain.Enums;
using LoadTest.Handlers;
using Xunit;
using Xunit.Abstractions;

namespace LoadTest.Controller.Event
{
    [Collection("Jagir")]
    public class RemoveEventTest : AppFactory
    {
        public RemoveEventTest(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }

        [Theory]
        [AdminHandler]
        [InlineData("event1")] // Should work correctly
        [InlineData("InvalidEventId", ErrorCode.EventNotFound)] // Invalid event Id
        public async Task RemoveEvent(string eventId, ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new GetChannelEventsViewModel.Request
            {
                Id = eventId
            };
            await PostJsonAsync(data);
        }
    }
}