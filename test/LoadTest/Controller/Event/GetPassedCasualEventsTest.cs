using System.Threading.Tasks;
using Application.ViewModels.Event;
using Domain.Enums;
using LoadTest.Handlers;
using Xunit;
using Xunit.Abstractions;

namespace LoadTest.Controller.Event
{
    [Collection("Jagir")]
    public class GetPassedCasualEventsTest : AppFactory
    {
        public GetPassedCasualEventsTest(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }

        [Theory]
        [UserHandler]
        [InlineData("channel1")]
        public async Task GetPassedCasualEvents(string channelId, ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new GetChannelEventsViewModel.Request
            {
                Id = channelId
            };
            await PostJsonAsync(data);
        }
    }
}