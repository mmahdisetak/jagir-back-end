using System.Threading.Tasks;
using Application.ViewModels.Event;
using LoadTest.Handlers;
using Xunit;
using Xunit.Abstractions;

namespace LoadTest.Controller.Event
{
    [Collection("Jagir")]
    public class GetCurrentEventsTest : AppFactory
    {
        public GetCurrentEventsTest(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }

        [Theory]
        [UserHandler]
        [InlineData("channel1")] // Should work correctly
        [InlineData("invalidChannel")]
        public async Task GetCurrentEvents(string channelId)
        {
            var data = new GetChannelEventsViewModel.Request
            {
                Id = channelId
            };
            await PostJsonAsync(data);
        }
    }
}