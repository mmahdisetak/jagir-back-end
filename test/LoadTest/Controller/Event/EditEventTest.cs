using System;
using System.Threading.Tasks;
using Application.Dtos.Event;
using Application.ViewModels.Event;
using LoadTest.Handlers;
using Xunit;
using Xunit.Abstractions;

namespace LoadTest.Controller.Event
{
    [Collection("Jagir")]
    public class EditEventTest : AppFactory
    {
        public EditEventTest(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }

        [Theory]
        [AdminHandler]
        [InlineData("event1", "newEvent", null, "EventDesc", true, true, null, null)] //ShouldWorkCorrectly
        [InlineData("event1", null, null, null, null, null, null, null)] //ShouldWorkCorrectly
        [InlineData("NotFoundEvent", null, null, null, null, null, null, null)] //EventNotFound
        [InlineData("event1", null, "InvalidAvatarId", null, null, null, null, null)] //FileNotFound
        public async Task EditEvent(string eventId, string name, string avatarId, string description, bool? weeklyReset,
            bool? isActive, DateTime? startingDate, DateTime? endingDate)
        {
            startingDate ??= DateTime.UtcNow;
            endingDate ??= DateTime.UtcNow;
            var data = new EditEventViewModel.Request
            {
                Id = eventId,
                Event = new EventDto
                {
                    Name = name,
                    AvatarId = avatarId,
                    Description = description,
                    WeeklyReset = weeklyReset,
                    IsActive = isActive,
                    StartingDate = startingDate,
                    EndingDate = endingDate
                }
            };

            await PostJsonAsync(data);
        }
    }
}