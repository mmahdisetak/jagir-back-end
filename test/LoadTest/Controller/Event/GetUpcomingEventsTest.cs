using System.Threading.Tasks;
using Application.ViewModels.Event;
using Domain.Enums;
using LoadTest.Handlers;
using Xunit;
using Xunit.Abstractions;

namespace LoadTest.Controller.Event
{
    [Collection("Jagir")]
    public class GetUpcomingEventsTest : AppFactory
    {
        public GetUpcomingEventsTest(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }

        [Theory]
        [UserHandler]
        [InlineData("channel1")] // Should work correctly
        [InlineData("invalidChannel", ErrorCode.ChannelNotFound)]
        public async Task GetUpcomingEvents(string channelId, ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new GetChannelEventsViewModel.Request
            {
                Id = channelId
            };
            await PostJsonAsync(data);
        }
    }
}