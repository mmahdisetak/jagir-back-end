﻿using System;
using System.Threading.Tasks;
using Application.Dtos.Event;
using Application.ViewModels.Event;
using LoadTest.Handlers;
using Xunit;
using Xunit.Abstractions;

namespace LoadTest.Controller.Event
{
    [Collection("Jagir")]
    public class CreateEventTest : AppFactory
    {
        public CreateEventTest(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }

        [Theory]
        [AdminHandler]
        [InlineData("channel1", "GoodEvent", "CoolEvent", true, true, null, null)] //ShouldWorkCorrectly
        [InlineData("NotFoundChannel", "GoodEvent", "CoolEvent", true, true, null, null)] //ChannelNotFound
        public async Task CreateEvent(string channelId, string name, string description, bool weeklyReset,
            bool isActive, DateTime? startingDate, DateTime? endingDate)
        {
            startingDate ??= DateTime.UtcNow;
            endingDate ??= DateTime.UtcNow;
            var data = new CreateEventViewModel.Request
            {
                ChannelId = channelId,
                Event = new EventDto
                {
                    Name = name,
                    Description = description,
                    WeeklyReset = weeklyReset,
                    IsActive = isActive,
                    StartingDate = startingDate,
                    EndingDate = endingDate
                }
            };

            await PostJsonAsync(data);
        }
    }
}