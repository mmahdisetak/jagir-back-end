﻿using System.Threading.Tasks;
using Application.ViewModels.Account;
using Domain.Enums;
using Xunit;
using Xunit.Abstractions;

namespace LoadTest.Controller.Account
{
    [Collection("Jagir")]
    public class SignInTest : AppFactory
    {

        public SignInTest(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }
        
        [Theory]
        [InlineData(SignInType.Email, "TestUser@Jagir.ir", "TestPassword")] //ShouldWorkCorrectly
        [InlineData(SignInType.Email, "", "TestPassword", ErrorCode.InvalidInput)] //EmptyLoginInput
        [InlineData(SignInType.Email, "TestUser3@Jagir.ir", "", ErrorCode.InvalidInput)] //EmptyPasswordInput
        [InlineData(SignInType.Email, "TestUser3@Jagir.ir", "WrongPassword", ErrorCode.SignInFailed)] //WrongPassword1
        [InlineData(SignInType.Email, "TestUser@Jagir.ir", "WrongPassword", ErrorCode.SignInFailed)] //WrongPassword2
        [InlineData(SignInType.UserName, "NotExistingsUser", "TestPassword", ErrorCode.SignInFailed)] //UserDoesNotExistTest
        [InlineData(SignInType.Email, "UnconfirmedEmail@Jagir.ir", "TestPassword", ErrorCode.UnConfirmedEmail)] //UnconfirmedEmailTest
       
        public async Task SignIn(SignInType type, string login, string password,
            ErrorCode errorCode = ErrorCode.NoError)
        {
            var data = new SignInViewModel.Request
            {
                Type = type,
                Login = login,
                Password = password
            };

            await PostJsonAsync(data);
        }
    }
}