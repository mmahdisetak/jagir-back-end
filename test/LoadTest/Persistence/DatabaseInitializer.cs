﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Domain.Entities;
using Infrastructure.Persistence;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace LoadTest.Persistence
{
    public class DatabaseInitializer
    {
        private readonly DateTime _now = DateTime.UtcNow;
        private readonly TimeSpan _fullDay = TimeSpan.FromHours(24);
        private readonly TimeSpan _twoMonth = TimeSpan.FromDays(60);
        private readonly TimeSpan _halfMonth = TimeSpan.FromDays(15);

        private readonly TimeSpan _halfDay = TimeSpan.FromHours(12);

        // private readonly TimeSpan _quarterDay = TimeSpan.FromHours(6);
        // private readonly TimeSpan _hour = TimeSpan.FromHours(1);
        private DatabaseContext Context { get; }
        private IConfiguration Configuration { get; }
        private UserManager<User> UserManager { get; }
        private RoleManager<IdentityRole> RoleManager { get; }

        public DatabaseInitializer(IServiceProvider scopeServiceProvider)
        {
            Context = scopeServiceProvider.GetService<DatabaseContext>();
            Configuration = scopeServiceProvider.GetService<IConfiguration>();
            UserManager = scopeServiceProvider.GetService<UserManager<User>>();
            RoleManager = scopeServiceProvider.GetService<RoleManager<IdentityRole>>();
        }

        public async Task Initialize()
        {
            try
            {
                if (await Context.Database.EnsureCreatedAsync())
                    await Initializer();
                else
                    while (!Context.InitializeHistories.Any(history => history.Version == "V1"))
                        Thread.Sleep(500);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task Initializer()
        {
            const string version = "V1";

            if (Context.InitializeHistories.Any(history => history.Version == version))
                return;
            // General Initializer
            await RoleInitializer();
            await AdminInitializer();

            // For Test, Don't remove or edit these
            await TestUserInitializer();
            await ChannelInitializer();
            await ChannelSupervisorInitializer();
            await EventInitializer();
            await TicketInitializer();

            await Context.InitializeHistories.AddAsync(new InitializeHistory
            {
                Version = version
            });

            await Context.SaveChangesAsync();
        }

        private async Task RoleInitializer()
        {
            await RoleManager.CreateAsync(new IdentityRole {Name = "Admin"});
            await RoleManager.CreateAsync(new IdentityRole {Name = "User"});
        }

        private async Task AdminInitializer()
        {
            var owner = new User
            {
                Id = "AdminUser",
                UserName = "Admin1",
                FirstName = "Admin",
                LastName = "Admin",
                Email = "TestAdmin@Jagir.ir",
                EmailConfirmed = true,
            };
            await UserManager.CreateAsync(owner, "AdminPassword");
            await UserManager.AddToRoleAsync(owner, "Admin");
            var adminToken1 = await UserManager.GenerateEmailConfirmationTokenAsync(owner);
            await UserManager.ConfirmEmailAsync(owner, adminToken1);
        }

        private async Task TestUserInitializer()
        {
            // !! Don't change Id and Password and Credit.
            var userTest1 = new User
            {
                Id = "user1",
                UserName = "User1",
                FirstName = "User",
                LastName = "Test",
                Email = "TestUser@Jagir.ir",
                Credit = 50000
            };
            await UserManager.CreateAsync(userTest1, "TestPassword");
            await UserManager.AddToRoleAsync(userTest1, "User");
            var userToken = await UserManager.GenerateEmailConfirmationTokenAsync(userTest1);
            await UserManager.ConfirmEmailAsync(userTest1, userToken);

            // !! Don't change EmailConfirmed.
            var userTest2 = new User
            {
                UserName = "User2",
                FirstName = "User",
                LastName = "Test",
                Email = "UnconfirmedEmail@Jagir.ir",
            };
            await UserManager.CreateAsync(userTest2, "TestPassword");
            await UserManager.AddToRoleAsync(userTest2, "User");

            // !! Don't change EmailConfirmed and Credit.
            var userTest3 = new User
            {
                UserName = "User3",
                FirstName = "User",
                LastName = "Test",
                Email = "TestUser3@Jagir.ir",
                Credit = 0
            };
            await UserManager.CreateAsync(userTest3, "TestPassword");
            await UserManager.AddToRoleAsync(userTest3, "User");
            userToken = await UserManager.GenerateEmailConfirmationTokenAsync(userTest3);
            await UserManager.ConfirmEmailAsync(userTest3, userToken);

            // !! Don't change Id.
            var userTest4 = new User
            {
                Id = "user4",
                UserName = "User4",
                FirstName = "User",
                LastName = "Test",
                Email = "TestUser4@Jagir.ir",
            };
            await UserManager.CreateAsync(userTest4, "TestPassword");
            await UserManager.AddToRoleAsync(userTest4, "User");

            var channelSupervisor = new User
            {
                Id = "channelSupervisor",
                UserName = "ChannelSupervisor",
                FirstName = "supervisor",
                LastName = "khan",
                Email = "channelSupervisor@Jagir.ir",
            };
            await UserManager.CreateAsync(channelSupervisor, "TestPassword");
            await UserManager.AddToRoleAsync(channelSupervisor, "User");
            var userToken5 = await UserManager.GenerateEmailConfirmationTokenAsync(userTest1);
            await UserManager.ConfirmEmailAsync(channelSupervisor, userToken5);
        }

        private async Task ChannelInitializer()
        {
            var channelSupervisor = new ChannelSupervisor
            {
                ChannelId = "channel1",
                UserId = "user1"
            };

            var channelTest1 = new Channel
            {
                Id = "channel1",
                Name = "ch1",
                Description = "cool channel",
                Supervisors = new List<ChannelSupervisor> {channelSupervisor},
                CreatorId = "user1",
            };
            await Context.Channels.AddAsync(channelTest1);
            await Context.ChannelSupervisors.AddAsync(channelSupervisor);
            await Context.SaveChangesAsync();
        }

        private async Task ChannelSupervisorInitializer()
        {
            var channelSupervisor = new ChannelSupervisor
            {
                ChannelId = "channel1",
                UserId = "channelSupervisor"
            };

            await Context.ChannelSupervisors.AddAsync(channelSupervisor);
            await Context.SaveChangesAsync();
        }

        private async Task EventInitializer()
        {
            var compChannel = Context.Channels.FirstOrDefault(ch => ch.Name == "ch1");
            var event1 = new Event
            {
                Name = "eventName", Description = "description", IsActive = true,
                Channel = compChannel, StartingDate = _now - _twoMonth, EndingDate = _now - _halfMonth,
                WeeklyReset = true,
                Avatar = null, Id = "event1"
            };
            var event2 = new Event
            {
                Name = "event2", Description = "description", IsActive = true,
                Channel = compChannel, StartingDate = _now - _fullDay, EndingDate = _now, WeeklyReset = false,
                Avatar = null, Id = "event2"
            };
            var event3 = new Event
            {
                Name = "event3", Description = "description", IsActive = true,
                Channel = compChannel, StartingDate = _now + _halfDay, EndingDate = _now + _fullDay,
                WeeklyReset = false,
                Avatar = null, Id = "event3"
            };
            var item1 = new Item
            {
                Name = "item1", Event = event1, Description = "description", Capacity = 30, Price = 10000,
                IsActive = true, LimitPerUser = 1, Id = "item1"
            };
            var item2 = new Item
            {
                Name = "item2", Event = event3, Description = "description", Capacity = 20, Price = 200,
                IsActive = true, LimitPerUser = 2, Id = "item2"
            };
            var item3 = new Item
            {
                Name = "item3", Event = event3, Description = "description3", Capacity = 40, Price = 300,
                IsActive = false, LimitPerUser = 1, Id = "item3"
            };
            var item4 = new Item
            {
                Name = "item4", Event = event3, Description = "description4", Capacity = 500, Price = 2000,
                IsActive = true, LimitPerUser = 1, Id = "item4"
            };
            await Context.Events.AddAsync(event1);
            await Context.Events.AddAsync(event2);
            await Context.Events.AddAsync(event3);
            await Context.Items.AddAsync(item1);
            await Context.Items.AddAsync(item2);
            await Context.Items.AddAsync(item3);
            await Context.Items.AddAsync(item4);
            await Context.SaveChangesAsync();
        }

        private async Task TicketInitializer()
        {
            var item = Context.Items.FirstOrDefault(item1 => item1.Name == "item1");

            var ticket = new Ticket
            {
                Id = "ticket1", CreateDate = _now, Item = item, IsConfirmed = true, IsUsed = false, Name = "ticket1",
                Price = 1000, QueueNumber = 10, Schedule = _now + _fullDay, UserId = "user1"
            };
            await Context.Tickets.AddAsync(ticket);
            await Context.SaveChangesAsync();
        }
    }
}