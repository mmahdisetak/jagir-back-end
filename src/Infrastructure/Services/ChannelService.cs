﻿using System.Collections.Generic;
using System.Linq;
using Application.Common;
using Application.Dtos.Channel;
using Application.Dtos.User;
using Domain.Enums;
using Infrastructure.Persistence;
using Domain.Entities;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using Application.Resources;
using AutoMapper;
using AutoMapper.Internal;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;

namespace Infrastructure.Services
{
    public interface IChannelService
    {
        public Task EditChannelProfile(string channelId, string name, string description, string bannerId,
            string avatarId);

        public Task<(bool, ChannelProfileDto)> GetChannelProfile(string id);

        public Task<string> CreateChannel(string name, string description, string ownerId,
            string[] supervisorsId);

        Task AddSupervisor(string channelId, string userId);
        Task RemoveSupervisor(string channelId, string userId);

        (int, UserShortProfileDto[]) SearchChannelFollowers(string channelId, string searchKey,
            int pageSize, int pageNumber);

        public ChannelProfileDto[] GetBestChannels(int size);
        Task<ChannelUser[]> SearchChannelUsers(string channelId, string searchKey);
        void RemoveChannel(string id);
        Task<ChannelShortDto[]> GetOwnedChannels();
    }

    public class ChannelService : IChannelService
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly DatabaseContext _context;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly IStringLocalizer<SharedResource> _localizer;

        public ChannelService(DatabaseContext context, IHttpContextAccessor contextAccessor, IMapper mapper,
            UserManager<User> userManager, IStringLocalizer<SharedResource> localizer)
        {
            _context = context;
            _contextAccessor = contextAccessor;
            _mapper = mapper;
            _userManager = userManager;
            _localizer = localizer;
        }

        public async Task EditChannelProfile(string channelId, string name, string description, string bannerId,
            string avatarId)
        {
            await CheckAccess(channelId);
            var channel = await _context.Channels.FindAsync(channelId);
            if (channel == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.ChannelNotFound, Message = _localizer["ChannelNotFound"]});
            if (!string.IsNullOrWhiteSpace(bannerId))
            {
                var banner = await _context.Files.FindAsync(bannerId);
                if (banner == null)
                    throw new ExceptionService(new Error
                        {Code = ErrorCode.FileNotFound, Message = _localizer["BannerNotFound"]});
                channel.BannerId = bannerId;
            }

            if (!string.IsNullOrWhiteSpace(avatarId))
            {
                var avatar = await _context.Files.FindAsync(avatarId);
                if (avatar == null)
                    throw new ExceptionService(new Error
                        {Code = ErrorCode.FileNotFound, Message = _localizer["AvatarNotFound"]});
                channel.AvatarId = avatarId;
            }

            if (!string.IsNullOrWhiteSpace(name))
                channel.Name = name;
            if (!string.IsNullOrWhiteSpace(description))
                channel.Description = description;
            _context.Channels.Update(channel);
            await _context.SaveChangesAsync();
        }

        public async Task<(bool, ChannelProfileDto)> GetChannelProfile(string id)
        {
            var channel = await _context.Channels.FindAsync(id);
            var followersCount = _context.ChannelFollowers.Count(chf => chf.ChannelId == id);

            if (channel == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.ChannelNotFound, Message = _localizer["ChannelNotFound"]});

            var channelProfile = _mapper.Map<ChannelProfileDto>(channel);
            channelProfile.FollowersCount = followersCount;
            var userId = _contextAccessor.HttpContext?.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null) return (false, channelProfile);
            var follower =
                _context.ChannelFollowers.FirstOrDefault(chf => chf.UserId == userId && chf.ChannelId == id);
            channelProfile.FollowStatus = follower != null;

            var roles = await _userManager.GetRolesAsync(user);
            var channelSupervisor =
                _context.ChannelSupervisors.FirstOrDefault(chs => chs.UserId == userId && chs.ChannelId == id);

            return (channelSupervisor != null || roles.Contains("Admin"), channelProfile);
        }

        public async Task<string> CreateChannel(string name, string description, string ownerId,
            string[] supervisorsId)
        {
            if (string.IsNullOrEmpty(ownerId))
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidInput, Message = _localizer["SupervisorNeeded"]});

            var owner = await _userManager.FindByIdAsync(ownerId);
            if (owner == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.UserNotFound, Message = _localizer["UserNotFound"]});

            var creatorId = _contextAccessor.HttpContext?.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var supervisors = new List<User>();
            if (supervisorsId is {Length: > 0})
                foreach (var supervisorId in supervisorsId)
                {
                    var user = await _userManager.FindByIdAsync(supervisorId);
                    if (user == null)
                        throw new ExceptionService(new Error
                            {Code = ErrorCode.UserNotFound, Message = _localizer["UserNotFound"]});
                    supervisors.Add(user);
                }

            var newChannel = new Channel
            {
                Name = name,
                Description = description,
                CreatorId = creatorId,
                Owner = owner
            };

            await _context.Channels.AddAsync(newChannel);
            foreach (var supervisor in supervisors)
            {
                await _context.ChannelSupervisors.AddAsync(new ChannelSupervisor
                    {Channel = newChannel, User = supervisor});
            }

            await _context.SaveChangesAsync();
            return newChannel.Id;
        }

        public async Task AddSupervisor(string channelId, string userId)
        {
            await CheckAccess(channelId);
            if (await _context.Users.FindAsync(userId) == null)
                throw new ExceptionService(new Error
                {
                    Code = ErrorCode.UserNotFound, Message = _localizer["UserNotFound"]
                });
            if (await _context.Channels.FindAsync(channelId) == null)
                throw new ExceptionService(new Error
                {
                    Code = ErrorCode.ChannelNotFound, Message = _localizer["ChannelNotFound"]
                });

            var newChannelSupervisor = new ChannelSupervisor
            {
                ChannelId = channelId,
                UserId = userId
            };

            _context.Add(newChannelSupervisor);
            await _context.SaveChangesAsync();
        }

        public async Task RemoveSupervisor(string channelId, string userId)
        {
            await CheckAccess(channelId);

            var supervisor =
                _context.ChannelSupervisors.FirstOrDefault(chs => chs.ChannelId == channelId && chs.UserId == userId);
            if (supervisor == null)
                throw new ExceptionService(new Error
                {
                    Code = ErrorCode.UserNotFound, Message = _localizer["UserNotFound"]
                });
            _context.ChannelSupervisors.Remove(supervisor);
            await _context.SaveChangesAsync();
        }

        public (int, UserShortProfileDto[]) SearchChannelFollowers(string channelId, string searchKey, int pageSize,
            int pageNumber)
        {
            searchKey ??= "";
            searchKey = searchKey.Trim();

            var queryableFollowers =
                _context.ChannelFollowers.Where(chf =>
                        chf.ChannelId == channelId &&
                        (chf.User.FirstName + chf.User.LastName + chf.User.UserName).Contains(searchKey))
                    .Include(chf => chf.User).Select(chf => chf.User);

            var total = queryableFollowers.Count();

            var followers = queryableFollowers.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToArray();
            return (total, _mapper.Map<UserShortProfileDto[]>(followers));
        }

        public ChannelProfileDto[] GetBestChannels(int size)
        {
            var entityChannels = _context.Channels.OrderByDescending(ch => ch.Followers.Count).Take(size)
                .Include(ch => ch.Followers).ToArray();
            return _mapper.Map<ChannelProfileDto[]>(entityChannels);
        }

        public async Task<ChannelUser[]> SearchChannelUsers(string channelId, string searchKey)
        {
            await CheckAccess(channelId);
            searchKey ??= "";
            searchKey = searchKey.Trim();

            var channel = await _context.Channels.FindAsync(channelId);
            if (channel == null)
                throw new ExceptionService(new Error
                {
                    Code = ErrorCode.ChannelNotFound, Message = _localizer["ChannelNotFound"]
                });
            var owner = channel.Owner;

            var followers = _context.ChannelFollowers.Where(chf =>
                    chf.ChannelId == channelId &&
                    (chf.User.FirstName + chf.User.LastName + chf.User.UserName).Contains(searchKey))
                .Include(chf => chf.User).Select(chf => chf.User).ToArray();

            var supervisors = _context.ChannelSupervisors.Where(chs =>
                    chs.ChannelId == channelId &&
                    (chs.User.FirstName + chs.User.LastName + chs.User.UserName).Contains(searchKey))
                .Include(chf => chf.User).Select(chf => chf.User).ToArray();

            var users = _mapper.Map<ChannelUser[]>(followers,
                opt => opt.AfterMap((_, dest) => dest.ForAll(chs => chs.Role = UserRole.Follower)));

            var ownerDto = owner == null
                ? null
                : _mapper.Map<ChannelUser>(owner,
                    opt => opt.AfterMap((_, dest) => dest.Role = UserRole.Owner));

            var supervisorsDto = _mapper.Map<ChannelUser[]>(supervisors,
                opt => opt.AfterMap((_, dest) => dest.ForAll(chs => chs.Role = UserRole.Supervisor)));
            return users.Concat(supervisorsDto).Append(ownerDto).ToArray();
        }

        public void RemoveChannel(string id)
        {
            var channel = _context.Channels.Include(s => s.Followers)
                .Include(s => s.Supervisors)
                .Include(s => s.Events).ThenInclude(evt => evt.Items)
                .FirstOrDefault(s => s.Id == id);
            if (channel == null)
                throw new ExceptionService(new Error
                {
                    Code = ErrorCode.ChannelNotFound,
                    Message = _localizer["ChannelNotFound"]
                });
            _context.Channels.Remove(channel);
            _context.SaveChanges();
        }

        public async Task<ChannelShortDto[]> GetOwnedChannels()
        {
            var userId = _contextAccessor.HttpContext?.User.FindFirstValue(ClaimTypes.NameIdentifier);

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                throw new ExceptionService(new Error
                {
                    Code = ErrorCode.UserNotFound, Message = _localizer["UserNotFound"]
                });
            var roles = await _userManager.GetRolesAsync(user);

            var channelIds = _context.ChannelSupervisors.Where(chs => chs.UserId == userId).ToList()
                .Select(chs => chs.ChannelId).ToArray();

            var channels = roles.Contains("Admin")
                ? _context.Channels.Include(ch => ch.Followers).ToArray()
                : _context.Channels.Where(ch => ch.OwnerId == userId || channelIds.Contains(ch.Id))
                    .Include(ch => ch.Followers).ToArray();
            return _mapper.Map<ChannelShortDto[]>(channels);
        }

        private async Task CheckAccess(string channelId)
        {
            var userId = _contextAccessor.HttpContext?.User.FindFirstValue(ClaimTypes.NameIdentifier);

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                throw new ExceptionService(new Error
                {
                    Code = ErrorCode.UserNotFound, Message = _localizer["UserNotFound"]
                });
            var roles = await _userManager.GetRolesAsync(user);

            var channelSupervisor =
                _context.ChannelSupervisors.FirstOrDefault(chs => chs.UserId == userId && chs.ChannelId == channelId);
            if (channelSupervisor == null && !roles.Contains("Admin"))
                throw new ExceptionService(new Error
                {
                    Code = ErrorCode.AccessDenied, Message = _localizer["AccessDenied"]
                });
        }
    }
}