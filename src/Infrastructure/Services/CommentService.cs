﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Application.Common;
using Application.Dtos.Comment;
using Application.Resources;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using Infrastructure.Persistence;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;

namespace Infrastructure.Services
{
    public interface ICommentService
    {
        public CommentDto[] GetEventComments(string eventId);
        public Task SubmitComment(string eventId, string content);
    }

    public class CommentService : ICommentService
    {
        private readonly DatabaseContext _context;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly UserManager<User> _userManager;
        private readonly IStringLocalizer<SharedResource> _localizer;

        public CommentService(DatabaseContext context, IMapper mapper, IHttpContextAccessor contextAccessor,
            UserManager<User> userManager, IStringLocalizer<SharedResource> localizer)
        {
            _context = context;
            _mapper = mapper;
            _contextAccessor = contextAccessor;
            _userManager = userManager;
            _localizer = localizer;
        }

        public CommentDto[] GetEventComments(string eventId)
        {
            var @event = _context.Events.Where(evt => evt.Id == eventId).Include(evt => evt.Comments)
                .ThenInclude(cmt => cmt.User).FirstOrDefault();
            if (@event == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.EventNotFound, Message = _localizer["EventNotFound"]});

            return _mapper.Map<CommentDto[]>(@event.Comments);
        }

        public async Task SubmitComment(string eventId, string content)
        {
            var userId = _contextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (await _userManager.FindByIdAsync(userId) == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.UserNotFound, Message = _localizer["UserNotFound"]});

            var @event = await _context.Events.FindAsync(eventId);
            if (@event == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.EventNotFound, Message = _localizer["EventNotFound"]});

            var comment = new Comment {Content = content, Event = @event, UserId = userId, IsConfirmed = true};
            await _context.Comments.AddAsync(comment);
            await _context.SaveChangesAsync();
        }
    }
}