﻿using System;
using System.Linq;
using Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Services
{
    public interface IJobService
    {
        void UpdateEvent(string eventId);
    }

    public class JobService : IJobService
    {
        private readonly DatabaseContext _context;

        public JobService(DatabaseContext context)
        {
            _context = context;
        }

        public void UpdateEvent(string eventId)
        {
            var dbEvent = _context.Events.Include(evt => evt.Items).FirstOrDefault(evt => evt.Id == eventId);
            if (dbEvent == null)
            {
                Console.WriteLine("Job scheduling failed!");
                return;
            }

            dbEvent.StartingDate += TimeSpan.FromDays(7);
            dbEvent.EndingDate += TimeSpan.FromDays(7);
            foreach (var item in dbEvent.Items)
            {
                item.Booked = 0;
                item.Schedule += TimeSpan.FromDays(7);
            }

            _context.SaveChanges();
        }
    }
}