﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Application.Common;
using Application.Dtos.Channel;
using Application.Dtos.Event;
using Application.Dtos.User;
using Application.Resources;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using Infrastructure.Persistence;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;

namespace Infrastructure.Services
{
    public interface IUserService
    {
        public Task<UserProfileDto> GetProfile();
        public Task FollowChannel(string channelId, bool status);
        public Task EditProfile(string email, string userName, string firstName, string lastName, string avatarId);
        public IChannelEventDto[] SearchChannelsEvents(string channelId, string key, int size);
        public Task<long> IncreaseCredit(long credit);
        public ChannelShortDto[] GetFollowedChannels();
        public ChannelProfileDto[] GetSuggestedChannels(int count);
        public EventDto[] GetSuggestedEvents(int count);
        UserShortProfileDto[] SearchUsers(string searchKey, int size);
    }

    public class UserService : IUserService
    {
        private readonly DatabaseContext _context;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IStringLocalizer<SharedResource> _localizer;

        public UserService(DatabaseContext context, UserManager<User> userManager, IMapper mapper,
            IHttpContextAccessor contextAccessor, IStringLocalizer<SharedResource> localizer)
        {
            _context = context;
            _userManager = userManager;
            _mapper = mapper;
            _contextAccessor = contextAccessor;
            _localizer = localizer;
        }

        public async Task<UserProfileDto> GetProfile()
        {
            var userId = _contextAccessor.HttpContext?.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.UserNotFound, Message = _localizer["UserNotFound"]});

            var userRoles = await _userManager.GetRolesAsync(user);
            var roles = "";
            for (var i = 0; i < userRoles.Count; i++)
                roles += i == userRoles.Count - 1 ? userRoles[i] : userRoles[i] + ",";

            _contextAccessor.HttpContext?.Response.Headers.Remove("Roles");
            _contextAccessor.HttpContext?.Response.Headers.Add("Roles", roles);

            return _mapper.Map<UserProfileDto>(user);
        }

        public async Task FollowChannel(string channelId, bool status)
        {
            var userId = _contextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (await _userManager.FindByIdAsync(userId) == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.UserNotFound, Message = _localizer["UserNotFound"]});
            if (await _context.Channels.FindAsync(channelId) == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.ChannelNotFound, Message = _localizer["ChannelNotFound"]});

            var follower =
                _context.ChannelFollowers.FirstOrDefault(chf => chf.ChannelId == channelId && chf.UserId == userId);
            if (follower == null)
                if (status)
                    await _context.ChannelFollowers.AddAsync(new ChannelFollower
                        {ChannelId = channelId, UserId = userId});
                else
                    throw new ExceptionService(new Error
                        {Code = ErrorCode.AlreadyFollowed, Message = _localizer["AlreadyFollowed"]});
            else if (status)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.AlreadyNotFollowed, Message = _localizer["AlreadyNotFollowed"]});
            else
                _context.ChannelFollowers.Remove(follower);

            await _context.SaveChangesAsync();
        }

        public async Task EditProfile(string email, string userName, string firstName, string lastName,
            string avatarId)
        {
            var userId = _contextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.UserNotFound, Message = _localizer["UserNotFound"]});

            // Check & edit email
            // Authorization for email ? 
            if (!string.IsNullOrWhiteSpace(email))
            {
                var duplicateUser = await _userManager.FindByEmailAsync(email);
                if (duplicateUser != null && duplicateUser.Id != userId)
                    throw new ExceptionService(new Error
                        {Code = ErrorCode.DuplicatedInput, Message = _localizer["DuplicateSingupEmail"]});
                user.Email = email;
            }

            // Check & edit userName
            if (!string.IsNullOrWhiteSpace(userName))
            {
                var duplicateUser = await _userManager.FindByNameAsync(userName);
                if (duplicateUser != null && duplicateUser.Id != userId)
                    throw new ExceptionService(new Error
                        {Code = ErrorCode.DuplicatedInput, Message = _localizer["DuplicateSingupUsername"]});
                user.UserName = userName;
            }

            // Check & edit firstName field
            if (firstName != null)
                user.FirstName = firstName;

            // Check & edit lastName field
            if (lastName != null)
                user.LastName = lastName;

            // Check & edit avatar field
            if (!string.IsNullOrWhiteSpace(avatarId))
            {
                var avatar = await _context.Files.FindAsync(avatarId);
                if (avatar == null)
                    throw new ExceptionService(new Error
                        {Code = ErrorCode.FileNotFound, Message = _localizer["AvatarNotFound"]});
                user.AvatarId = avatarId;
            }

            _context.Users.Update(user);
            await _context.SaveChangesAsync();
        }

        public IChannelEventDto[] SearchChannelsEvents(string channelId, string key, int size)
        {
            key = key.Trim();
            var channelsEvents = new List<IChannelEventDto>();
            if (channelId == null)
            {
                var candidateChannels = _mapper.Map<ChannelShortDto[]>(_context.Channels
                    .Where(ch => ch.Name.Contains(key) || ch.Description.Contains(key))
                    .OrderBy(ch => ch.Name + ch.Description).Take(size)
                    .Include(ch => ch.Events)
                    .Include(ch => ch.Followers).ToArray());
                var candidateEvents = _mapper.Map<EventShortDto[]>(_context.Events
                    .Where(evt =>
                        (evt.Name.Contains(key) || evt.Description.Contains(key)) && !evt.IsDeleted && evt.IsActive)
                    .OrderBy(ch => ch.Name + ch.Description).Take(size)
                    .Include(evt => evt.Items).ToArray());

                channelsEvents.AddRange(candidateChannels);
                channelsEvents.AddRange(candidateEvents);
            }
            else
            {
                var candidateEvents = _mapper.Map<EventShortDto[]>(_context.Events
                    .Where(evt => evt.ChannelId == channelId && evt.Name.Contains(key) || evt.Description.Contains(key))
                    .OrderBy(ch => ch.Name + ch.Description).Take(size).ToArray());

                channelsEvents.AddRange(candidateEvents);
            }

            return channelsEvents.OrderBy(chev => chev.Name + chev.Description).Take(size).ToArray();
        }

        public async Task<long> IncreaseCredit(long credit)
        {
            var userId = _contextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var user = await _userManager.FindByIdAsync(userId);

            if (user == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.UserNotFound, Message = _localizer["UserNotFound"]});

            if (credit is <= 0 or > 500000)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidCredit, Message = _localizer["InvalidCredit"]});

            user.Credit += credit;
            _context.Users.Update(user);
            await _context.SaveChangesAsync();

            return user.Credit;
        }

        public ChannelShortDto[] GetFollowedChannels()
        {
            var userId = _contextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var user = _context.Users.Include(usr => usr.FollowingChannels).ThenInclude(chf => chf.Channel)
                .FirstOrDefault(usr => usr.Id == userId);
            if (user == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.UserNotFound, Message = _localizer["UserNotFound"]});

            var followings = user.FollowingChannels.Select(fc => fc.Channel);

            return _mapper.Map<ChannelShortDto[]>(followings);
        }

        public ChannelProfileDto[] GetSuggestedChannels(int count)
        {
            var channels = _context.Channels.Take(count).Include(ch => ch.Followers);
            return _mapper.Map<ChannelProfileDto[]>(channels);
        }

        public EventDto[] GetSuggestedEvents(int count)
        {
            var events = _context.Events.Where(evt => evt.IsActive && !evt.IsDeleted).Take(count);
            return _mapper.Map<EventDto[]>(events);
        }

        public UserShortProfileDto[] SearchUsers(string searchKey, int size)
        {
            var users = _context.Users
                .Where(usr =>
                    searchKey == null || (usr.FirstName + usr.LastName + usr.UserName + usr.Email).Contains(searchKey))
                .Take(size).ToArray();
            return _mapper.Map<UserShortProfileDto[]>(users);
        }
    }
}