﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using Application.Common;
using Application.Resources;
using Domain.Entities;
using Domain.Enums;
using Infrastructure.Persistence;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;

namespace Infrastructure.Services
{
    public interface IAccountService
    {
        public Task SignUp(string userName, string email, string password);
        public Task SignIn(SignInType type, string login, string password);
        public Task SignOut();
        public Task ForgetPassword(string login, SignInType type);
        public Task ResetPassword(string email, string token, string newPassword);
        public Task ConfirmEmail(string email, string token);
        public AuthenticationProperties GoogleSignIn(string provider, string redirectUrl);
        public Task ExternalSignIn();
    }

    public class AccountService : IAccountService
    {
        private readonly DatabaseContext _context;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IEmailService _emailService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IConfiguration _configuration;
        private readonly IStringLocalizer<SharedResource> _localizer;
        private readonly Regex _emailRegex;

        public AccountService(DatabaseContext context, UserManager<User> userManager, SignInManager<User> signInManager,
            IEmailService emailService, IHttpContextAccessor httpContextAccessor, IConfiguration configuration,
            IStringLocalizer<SharedResource> localizer)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
            _emailService = emailService;
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _localizer = localizer;
            _emailRegex = new Regex(
                @"^[A-Za-z0-9!'#$%&*+\/=?^_`{|}~-]+(?:\.[A-Za-z0-9!'#$%&*+\/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+[A-Za-z]{2,}$",
                RegexOptions.IgnoreCase);
        }

        public async Task SignUp(string userName, string email, string password)
        {
            if (string.IsNullOrWhiteSpace(userName))
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidInput, Message = _localizer["EmptyUsername"]});
            if (string.IsNullOrWhiteSpace(email))
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidInput, Message = _localizer["EmptyUsername"]});
            if (!_emailRegex.IsMatch(email))
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidInput, Message = _localizer["InvalidEmail"]});
            if (string.IsNullOrWhiteSpace(password))
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidInput, Message = _localizer["EmptyPassword"]});
            if (password.Length < 6)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidInput, Message = _localizer["EmptyPassword"]});
            var duplicateUser = await _userManager.FindByEmailAsync(email);
            duplicateUser ??= await _userManager.FindByNameAsync(userName);
            if (duplicateUser != null)
                throw new ExceptionService(new Error
                {
                    Code = ErrorCode.DuplicatedInput,
                    Message = _localizer[
                        email == duplicateUser.Email ? "DuplicateSingUpEmail" : "DuplicateSingUpUsername"]
                });

            var user = new User
            {
                Email = email,
                UserName = userName
            };
            var result = await _userManager.CreateAsync(user, password);
            await _userManager.AddToRoleAsync(user, "User");
            if (!result.Succeeded)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.SingUpFailed, Message = _localizer["SingUpFailed"]});

            var emailToken = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            var linkAddress = _configuration["RedirectLinks:ConfirmEmail"];
            linkAddress += "?email=" + HttpUtility.UrlEncode(email);
            linkAddress += "&token=" + HttpUtility.UrlEncode(emailToken);


            _emailService.SendEmail(email, email,
                "تایید حساب کاربری",
                "اعتبار سنجی", "برای تایید ایمیل خود بر روی دکمه زیر کلیک کنید:",
                linkAddress, "تایید ایمیل");
        }

        public async Task SignIn(SignInType type, string login, string password)
        {
            if (string.IsNullOrWhiteSpace(login))
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidInput, Message = _localizer["EmptyUsername"]});
            if (string.IsNullOrWhiteSpace(password))
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidInput, Message = _localizer["EmptyPassword"]});
            var user = type switch
            {
                SignInType.Email => await _userManager.FindByEmailAsync(login),
                SignInType.UserName => await _userManager.FindByNameAsync(login),
                _ => throw new ExceptionService(new Error
                {
                    Code = ErrorCode.InvalidInput, Message = _localizer["InvalidSignIn"]
                })
            };
            if (user == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.SignInFailed, Message = _localizer["SignInFailed"]});
            if (!user.EmailConfirmed)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.UnConfirmedEmail, Message = _localizer["UnConfirmedEmail"]});
            var result = await _signInManager.PasswordSignInAsync(user, password, true, false);
            if (!result.Succeeded)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.SignInFailed, Message = _localizer["SignInFailed"]});
            await SetRolesCookie(user);
        }

        private async Task SetRolesCookie(User user)
        {
            var userRoles = await _userManager.GetRolesAsync(user);
            var roles = "";
            for (var i = 0; i < userRoles.Count; i++)
                roles += i == userRoles.Count - 1 ? userRoles[i] : userRoles[i] + ",";

            _httpContextAccessor.HttpContext.Response.Headers.Remove("Roles");
            _httpContextAccessor.HttpContext.Response.Headers.Add("Roles", roles);
        }

        public async Task ConfirmEmail(string email, string token)
        {
            email = EmailNormalize(email);

            if (string.IsNullOrWhiteSpace(email))
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidInput, Message = _localizer["EmptyEmail"]});
            if (!_emailRegex.IsMatch(email))
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidInput, Message = _localizer["InvalidEmail"]});
            if (string.IsNullOrWhiteSpace(token))
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidInput, Message = _localizer["EmptyToken"]});

            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.UserNotFound, Message = _localizer["UserNotFound"]});

            if (user.EmailConfirmed)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.EmailAlreadyConfirmed, Message = _localizer["EmailAlreadyConfirmed"]});

            var result = await _userManager.ConfirmEmailAsync(user, token);

            if (!result.Succeeded)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidToken, Message = _localizer["InvalidToken"]});

            await _signInManager.SignInAsync(user, true);
            await SetRolesCookie(user);
        }

        public AuthenticationProperties GoogleSignIn(string provider, string redirectUrl)
        {
            return _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
        }

        public async Task ExternalSignIn()
        {
            var info = await _signInManager.GetExternalLoginInfoAsync();

            var email = info.Principal.FindFirstValue(ClaimTypes.Email);
            if (email == null)
                throw new ExceptionService(new Error {Code = ErrorCode.Email, Message = _localizer["InvalidEmail"]});
            var user = await _userManager.FindByEmailAsync(email);

            var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey,
                true, true);

            if (result.Succeeded)
            {
                await SetRolesCookie(user);
                return;
            }

            if (user == null)
            {
                var userName = info.Principal.FindFirstValue(ClaimTypes.Email).Split("@")[0];

                if (_context.Users.Any(sampleUser => sampleUser.UserName == userName))
                {
                    var counter = 1;
                    while (_context.Users.Any(sampleUser => sampleUser.UserName == userName + counter))
                        counter++;
                    userName += counter;
                }

                user = new User
                {
                    FirstName = info.Principal.FindFirstValue(ClaimTypes.Name),
                    UserName = userName,
                    Email = info.Principal.FindFirstValue(ClaimTypes.Email)
                };
                var pass = new Guid().ToString();

                await _userManager.CreateAsync(user, pass);

                var userToken = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                await _userManager.ConfirmEmailAsync(user, userToken);

                await _userManager.AddToRoleAsync(user, "User");
            }

            await _userManager.AddLoginAsync(user, info);
            await _signInManager.SignInAsync(user, true);

            await SetRolesCookie(user);
        }


        public async Task SignOut()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task ForgetPassword(string login, SignInType type)
        {
            if (string.IsNullOrWhiteSpace(login))
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidInput, Message = _localizer["InvalidInput"]});

            if (type == SignInType.Email)
                login = EmailNormalize(login);

            var user = type == SignInType.Email
                ? await _userManager.FindByEmailAsync(login)
                : await _userManager.FindByNameAsync(login);
            if (user == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.UserNotFound, Message = _localizer["UserNotFound"]});

            if (!user.EmailConfirmed)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.UnConfirmedEmail, Message = _localizer["UnConfirmedEmail"]});
            var email = user.Email;
            var userToken =
                await _userManager.GenerateUserTokenAsync(user, TokenOptions.DefaultEmailProvider, "ChangePassword");
            var linkAddress = _configuration["RedirectLinks:ResetPassword"];
            linkAddress += "?email=" + HttpUtility.UrlEncode(email);
            linkAddress += "&token=" + HttpUtility.UrlEncode(userToken);
            _emailService.SendEmail(email, email,
                "درخواست تغییر کلمه عبور",
                "تغییر کلمه عبور", "برای تغییر رمز عبور خود بر روی دکمه زیر کلیک کنید:",
                linkAddress, "تغییر رمز عبور");
        }

        public async Task ResetPassword(string email, string token, string newPassword)
        {
            email = EmailNormalize(email);

            if (string.IsNullOrWhiteSpace(email))
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidInput, Message = _localizer["EmptyEmail"]});
            if (string.IsNullOrWhiteSpace(token))
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidInput, Message = _localizer["EmptyToken"]});
            if (string.IsNullOrWhiteSpace(newPassword))
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidInput, Message = _localizer["EmptyPassword"]});

            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.UserNotFound, Message = _localizer["UserNotFound"]});

            var isSucceeded =
                await _userManager.VerifyUserTokenAsync(user, TokenOptions.DefaultEmailProvider, "ChangePassword",
                    token);

            if (!isSucceeded)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidToken, Message = _localizer["InvalidToken"]});

            var changePasswordToken = await _userManager.GeneratePasswordResetTokenAsync(user);
            await _userManager.ResetPasswordAsync(user, changePasswordToken, newPassword);
        }

        private static string EmailNormalize(string email)
        {
            if (email == null)
                return null;

            if (!new Regex(@"@gmail.com$", RegexOptions.IgnoreCase).IsMatch(email)) return email;
            var emailFirstPart = email.Split("@")[0].Replace(".", "");
            var emailSecondPart = email.Split("@")[1];
            return emailFirstPart + "@" + emailSecondPart;
        }
    }
}