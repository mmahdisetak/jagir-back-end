﻿using System;
using Application.Common;
using Application.Resources;
using Domain.Enums;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using MimeKit;
using MimeKit.Text;

namespace Infrastructure.Services
{
    public interface IEmailService
    {
        public void SendEmail(string destinationMail, string name, string subject, string emailTitle,
            string emailContent, string linkInEmail, string emailButtonText);
    }

    public class EmailService : IEmailService
    {
        private readonly IStringLocalizer<SharedResource> _localizer;
        private readonly string _emailUserName;
        private readonly string _emailPassword;
        private const string EmailImageLink = "https://jagir.markop.ir/logo512.png";
        private const string EmailSiteLink = "https://jagir.markop.ir";
        private const string SiteName = "jagir";

        public EmailService(IConfiguration configuration, IStringLocalizer<SharedResource> localizer)
        {
            _localizer = localizer;
            _emailUserName = configuration["EmailService:Address"];
            _emailPassword = configuration["EmailService:Password"];
        }

        public void SendEmail(string destinationMail, string name, string subject, string emailTitle,
            string emailContent,
            string linkInEmail, string emailButtonText)
        {
            var content = new TextPart(TextFormat.Html)
            {
                Text = @"
                <!DOCTYPE html>
                <html xmlns='http://www.w3.org/1999/xhtml' xmlns:v='urn:schemas-microsoft-com:vml' xmlns:o='urn:schemas-microsoft-com:office:office'>
                    <head>
                        <style>
                        .body {
                            background: #e8e8e8;
                            padding: 20px;
                        }
                        .email {
                            background: #fff;
                            border-radius: 5px;
                        }
                        .title {
                            text-align: center;
                            padding: 20px 0;
                            width: 100%;
                        }
                        .title-img {
                            height: 50px;
                            width: 50px;
                        }
                        .title-site {
                            text-decoration: none;
                            color: #1d224c !important;
                            font-size: 20px;
                            display: block;
                        }
                        .subject {
                            background: #1d224c;
                            text-align: center;
                            padding: 5px 0;
                            font-size: 20px;
                            color: #fff;
                        }
                        .content {
                            padding: 20px;
                            text-align: " + "right" + @";
                        }
                        .content-subject {
                            margin-bottom: 5px;
                            font-size: 16px;
                            color: #000 !important;
                        }
                        .content-confirm-button {
                            text-align: center;
                            margin-top: 30px;
                        }
                        .content-confirm-button-link {
                            text-decoration: none;
                            background: #2d84c1;
                            border-radius: 5px;
                            padding: 10px 20px;
                            font-size: 16px;
                            color: #fff !important;
                        }
                        .seperator {
                            background: #c1c1c1;
                            margin-top: 20px;
                            width: 100%;
                            height: 1px;
                        }
                        .footer {
                            text-align: " + "right" + @";
                            font-size: 16px;
                            padding: 20px;
                            color: #000 !important;
                        }
                        </style>
                    </head>
                    <body>
                        <section class='body'>
                            <section class='email'>
                                <div class='title'>
                                    <img class='title-img' src='" + EmailImageLink + @"' />
                                    <a class='title-site' href='" + EmailSiteLink +
                       @"' target='_blank' >" + SiteName + @"</a>
                                </div>
                                <div class='subject'>" + emailTitle + @"</div>
                                <div class='content'>
                                    <div class='content-subject' dir='auto'>" + emailContent + @"
                                    </div>
                                    <div class='content-confirm-button'>
                                    <a
                                        class='content-confirm-button-link'
                                        href='" + linkInEmail + @"'
                                        target='_blank'
                                    >" + emailButtonText + @"
                                    </a>
                                    </div>
                                </div>
                                <div class='seperator'></div>
                                <div class='footer' dir='auto'>" +
                       "اگر این درخواست از جانب شما نیست، آن را نادیده بگیرید." + @"</div>
                            </section>
                        </section>
                    </body>
                </html>
                "
            };

            var message = new MimeMessage();
            message.To.Add(new MailboxAddress(name, destinationMail));
            message.From.Add(new MailboxAddress("جاگیر", _emailUserName));
            message.Subject = subject;
            message.Body = content;

            try
            {
                using var client = new SmtpClient();
                client.Connect("mail.markop.ir", 587, SecureSocketOptions.StartTls);
                client.Authenticate(_emailUserName, _emailPassword);
                client.Send(message);
                client.Disconnect(true);
            }
            catch (Exception)
            {
                throw new ExceptionService(new Error
                    {Code = ErrorCode.Email, Message = _localizer["ErrorSendingEmail"]});
            }
        }
    }
}