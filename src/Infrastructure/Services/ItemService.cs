﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Application.Common;
using Application.Dtos.Item;
using Application.Resources;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using Infrastructure.Persistence;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;

namespace Infrastructure.Services
{
    public interface IItemService
    {
        void CreateItem(CreateItemDto item, string eventId);
        void EditItem(ItemDto item);
        Task RemoveItem(string id);
        public Task<ItemDto[]> GetEventItems(string eventId);
    }

    public class ItemService : IItemService
    {
        private readonly DatabaseContext _context;
        private readonly IStringLocalizer<SharedResource> _localizer;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly IHttpContextAccessor _contextAccessor;

        public ItemService(DatabaseContext context, IStringLocalizer<SharedResource> localizer, IMapper mapper,
            UserManager<User> userManager, IHttpContextAccessor contextAccessor)
        {
            _context = context;
            _localizer = localizer;
            _mapper = mapper;
            _userManager = userManager;
            _contextAccessor = contextAccessor;
        }

        public void CreateItem(CreateItemDto item, string eventId)
        {
            if (item.Schedules == null || item.Schedules.Length == 0)
            {
                _context.Items.Add(_mapper.Map<Item>(item));
            }
            else
            {
                var items = new Item[item.Schedules.Length];
                for (var i = 0; i < item.Schedules.Length; i++)
                    items[i] = new Item
                    {
                        Name = item.Name,
                        Description = item.Description,
                        Capacity = item.Capacity,
                        Schedule = item.Schedules[i],
                        EventId = eventId
                    };

                _context.Items.AddRange(items);
            }

            _context.SaveChanges();
        }

        public void EditItem(ItemDto item)
        {
            foreach (var session in item.Sessions)
            {
                var dbItem = _context.Items.Find(session.Id);
                if (dbItem == null)
                    throw new ExceptionService(new Error
                    {
                        Code = ErrorCode.ItemNotFound,
                        Message = _localizer["ItemNotFound"]
                    });
                _context.Update(dbItem);
                if (!string.IsNullOrWhiteSpace(item.Name))
                    dbItem.Name = item.Name.Trim();
                if (!string.IsNullOrWhiteSpace(item.Description))
                    dbItem.Description = item.Description.Trim();
                dbItem.Capacity = item.Capacity;
                dbItem.Price = item.Price;
                dbItem.LimitPerUser = item.LimitPerUser;
                dbItem.Schedule = session.Schedule;
            }

            _context.SaveChanges();
        }

        public async Task RemoveItem(string id)
        {
            var item = _context.Items.Include(itm => itm.Event).FirstOrDefault(itm => itm.Id == id);
            if (item == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.ItemNotFound, Message = _localizer["ItemNotFound"]});
            await CheckAccess(item.Event.ChannelId);
            _context.Items.Remove(item);
            await _context.SaveChangesAsync();
        }

        public async Task<ItemDto[]> GetEventItems(string eventId)
        {
            var @event = _context.Events.Where(evt => evt.Id == eventId).Include(evt => evt.Items).FirstOrDefault();
            if (@event == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.EventNotFound, Message = _localizer["EventNotFound"]});

            var userId = _contextAccessor.HttpContext?.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var user = await _userManager.FindByIdAsync(userId);
            if (!string.IsNullOrEmpty(userId) && user == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.UserNotFound, Message = _localizer["UserNotFound"]});
            // IQueryable<Ticket> userItems = null;
            // if (user != null)
            //     userItems = _context.Tickets.Where(tkt => tkt.UserId == userId && tkt.Item.EventId == eventId);

            var itemsDto = new List<ItemDto>();
            if (@event.Items == null || @event.Items.Count == 0)
                return itemsDto.ToArray();
            var items = @event.Items.OrderBy(itm => itm.Name).ToArray();
            var i = 0;
            var sessionsDto = new List<SessionDto>();
            foreach (var item in items)
            {
                if (i == 0 || itemsDto[i - 1].Name != item.Name)
                {
                    if (i != 0)
                        itemsDto[i - 1].Sessions = sessionsDto.ToArray();

                    var userBooked = 0;
                    if (user != null)
                        userBooked = _context.Tickets.Count(tkt => tkt.UserId == userId && tkt.ItemId == item.Id);
                    sessionsDto = new List<SessionDto>
                    {
                        new()
                        {
                            Id = item.Id, Booked = item.Booked,
                            Schedule = item.Schedule?.ToUniversalTime(),
                            UserBooked = userBooked
                        }
                    };
                    itemsDto.Add(new ItemDto
                    {
                        Name = item.Name, Capacity = item.Capacity, Price = item.Price, LimitPerUser = item.LimitPerUser
                    });
                    i++;
                }
                else
                {
                    var userBooked = 0;
                    if (user != null)
                        userBooked = _context.Tickets.Count(tkt => tkt.UserId == userId && tkt.ItemId == item.Id);
                    sessionsDto.Add(new SessionDto
                    {
                        Id = item.Id, Booked = item.Booked,
                        Schedule = item.Schedule?.ToUniversalTime(),
                        UserBooked = userBooked
                    });
                }
            }

            itemsDto[i - 1].Sessions = sessionsDto.ToArray();

            return itemsDto.ToArray();
        }

        private async Task CheckAccess(string channelId)
        {
            var userId = _contextAccessor.HttpContext?.User.FindFirstValue(ClaimTypes.NameIdentifier);

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                throw new ExceptionService(new Error
                {
                    Code = ErrorCode.UserNotFound, Message = _localizer["UserNotFound"]
                });
            var roles = await _userManager.GetRolesAsync(user);

            var channelSupervisor =
                _context.ChannelSupervisors.FirstOrDefault(chs => chs.UserId == userId && chs.ChannelId == channelId);
            if (channelSupervisor == null && !roles.Contains("Admin"))
                throw new ExceptionService(new Error
                {
                    Code = ErrorCode.AccessDenied, Message = _localizer["AccessDenied"]
                });
        }
    }
}