﻿using System.IO;
using System.Threading.Tasks;
using Application.Common;
using Application.Dtos.File;
using Application.Resources;
using Domain.Entities;
using Domain.Enums;
using Infrastructure.Persistence;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;

namespace Infrastructure.Services
{
    public interface IFileService
    {
        public Task<string> Upload(IFormFile file, byte? type);
        public DownloadFileDto Download(string id);
    }

    public class FileService : IFileService
    {
        private readonly DatabaseContext _context;
        private readonly IConfiguration _configuration;
        private readonly IStringLocalizer<SharedResource> _localizer;

        public FileService(DatabaseContext context, IConfiguration configuration,
            IStringLocalizer<SharedResource> localizer)
        {
            _context = context;
            _configuration = configuration;
            _localizer = localizer;
        }

        public async Task<string> Upload(IFormFile file, byte? type)
        {
            if (file == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidInput, Message = _localizer["InvalidFile"]});

            var fileEntity = new FileEntity
            {
                Name = file.FileName,
                Size = file.Length,
                ContentType = file.ContentType,
            };

            await _context.Files.AddAsync(fileEntity);

            var fileId = fileEntity.Id;
            var path = _configuration["StorePath"] + fileId;

            await using var stream = File.Create(path);
            await file.CopyToAsync(stream);

            await _context.SaveChangesAsync();
            return fileId;
        }

        public DownloadFileDto Download(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidInput, Message = _localizer["InvalidFileID"]});

            var file = _context.Files.Find(id);
            if (file == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.FileNotFound, Message = _localizer["FileNotFound"]});

            var path = _configuration["StorePath"] + id;
            return new DownloadFileDto {Name = file.Name, Path = path, ContentType = file.ContentType};
        }
    }
}