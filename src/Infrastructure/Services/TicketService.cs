﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Application.Common;
using Application.Dtos.Ticket;
using Application.Resources;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using Infrastructure.Persistence;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;

namespace Infrastructure.Services
{
    public interface ITicketService
    {
        public Task Reserve(string[] itemIds);
        public Task<TicketDto[]> GetUserTickets();
        public void Cancel(string id);
    }

    public class TicketService : ITicketService
    {
        private readonly DatabaseContext _context;
        private readonly UserManager<User> _userManager;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IMapper _mapper;
        private readonly IStringLocalizer<SharedResource> _localizer;

        public TicketService(DatabaseContext context, UserManager<User> userManager,
            IHttpContextAccessor contextAccessor, IMapper mapper, IStringLocalizer<SharedResource> localizer)
        {
            _context = context;
            _userManager = userManager;
            _contextAccessor = contextAccessor;
            _mapper = mapper;
            _localizer = localizer;
        }

        public async Task Reserve(string[] itemIds)
        {
            var userId = _contextAccessor.HttpContext?.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.UserNotFound, Message = _localizer["UserNotFound"]});

            var totalCost = 0;
            var items = new List<Item>();
            foreach (var itemId in itemIds)
            {
                var item = await _context.Items.FindAsync(itemId);
                if (item == null)
                    throw new ExceptionService(new Error
                        {Code = ErrorCode.ItemNotFound, Message = _localizer["ItemNotFound"]});
                if (item.Capacity - item.Booked <= 0)
                    throw new ExceptionService(new Error
                        {Code = ErrorCode.OutOfCapacity, Message = _localizer["OutOfCapacity"]});
                totalCost += item.Price;
                items.Add(item);
            }

            if (user.Credit < totalCost)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.NotEnoughCredit, Message = _localizer["NotEnoughCredit"]});

            foreach (var item in items)
            {
                item.Booked += 1;
                _context.Items.Update(item);
                var ticket = new Ticket
                {
                    Item = item, Name = item.Name, Schedule = item.Schedule, User = user,
                    QueueNumber = item.Booked, Price = item.Price
                };
                await _context.Tickets.AddAsync(ticket);
                user.Credit -= item.Price;
            }

            await _context.SaveChangesAsync();
        }

        public async Task<TicketDto[]> GetUserTickets()
        {
            var userId = _contextAccessor.HttpContext?.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.UserNotFound, Message = _localizer["UserNotFound"]});
            var tickets = _context.Tickets.Where(tck => tck.UserId == userId).Include(tkt => tkt.Item)
                .ThenInclude(itm => itm.Event).ThenInclude(evt => evt.Channel).ToArray();
            return _mapper.Map<TicketDto[]>(tickets);
        }

        public void Cancel(string id)
        {
            var ticket = _context.Tickets.Where(tkt => tkt.Id == id)
                .Include(tkt => tkt.User).Include(tkt => tkt.Item).FirstOrDefault();
            if (ticket == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.TicketNotFound, Message = _localizer["TicketNotFound"]});
            ticket.User.Credit += ticket.Price;
            ticket.Item.Booked -= 1;
            _context.Tickets.Remove(ticket);
            _context.SaveChanges();
        }
    }
}