﻿using Application.Dtos.Event;
using Application.Common;
using Domain.Entities;
using Domain.Enums;
using Infrastructure.Persistence;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Application.Dtos.Ticket;
using Application.Resources;
using AutoMapper;
using Hangfire;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;

namespace Infrastructure.Services
{
    public interface IEventService
    {
        public Task<string> CreateEvent(EventDto @event, string channelId);
        public void EditEvent(string id, EventDto @event);
        public Task RemoveEvent(string id);
        (string ChannelId, EventDto) GetEvent(string id);

        public (int, EventDto[]) SearchEvents(string channelId, string key, EventTime filter, EventType type,
            int pageNumber, int pageSize, EventFilter? sort = null);

        public EventDto[] GetBestEvents(int size);
        Task<UserTicketDto[]> GetEventTickets(string eventId);
    }

    public class EventService : IEventService
    {
        private readonly DatabaseContext _context;
        private readonly IMapper _mapper;
        private readonly IStringLocalizer<SharedResource> _localizer;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly UserManager<User> _userManager;
        private readonly IRecurringJobManager _jobManager;

        public EventService(DatabaseContext context, IMapper mapper, IStringLocalizer<SharedResource> localizer,
            IHttpContextAccessor contextAccessor, UserManager<User> userManager, IRecurringJobManager jobManager)
        {
            _context = context;
            _mapper = mapper;
            _localizer = localizer;
            _contextAccessor = contextAccessor;
            _userManager = userManager;
            _jobManager = jobManager;
        }


        public async Task<string> CreateEvent(EventDto @event, string channelId)
        {
            await CheckAccess(channelId);
            var channel = await _context.Channels.FindAsync(channelId);
            if (channel == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.ChannelNotFound, Message = _localizer["ChannelNotFound"]});
            if (@event.AvatarId != null && await _context.Files.FindAsync(@event.AvatarId) == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.FileNotFound, Message = _localizer["FileNotFound"]});
            if (string.IsNullOrEmpty(@event.Name))
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidInput, Message = _localizer["InvalidEventName"]});
            if (@event.StartingDate == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidInput, Message = _localizer["InvalidStartEvent"]});
            if (@event.EndingDate == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidInput, Message = _localizer["InvalidFinishEvent"]});

            var newEvent = _mapper.Map<Event>(@event);
            newEvent.ChannelId = channelId;
            newEvent.Id = null;

            _context.Add(newEvent);
            await _context.SaveChangesAsync();
            if (newEvent.WeeklyReset)
                _jobManager.AddOrUpdate<IJobService>(newEvent.Id, service => service.UpdateEvent(newEvent.Id),
                    Cron.Weekly());
            return newEvent.Id;
        }

        public void EditEvent(string id, EventDto @event)
        {
            var eventEntity = _context.Events.Find(id);
            if (eventEntity == null || eventEntity.IsDeleted)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.EventNotFound, Message = _localizer["EventNotFound"]});
            if (@event == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.InvalidInput, Message = _localizer["InvalidEvent"]});
            if (!string.IsNullOrWhiteSpace(@event.AvatarId))
            {
                if (_context.Files.Find(@event.AvatarId) == null)
                    throw new ExceptionService(new Error
                        {Code = ErrorCode.FileNotFound, Message = _localizer["FileNotFound"]});
                eventEntity.AvatarId = @event.AvatarId;
            }

            if (!string.IsNullOrWhiteSpace(@event.Name))
                eventEntity.Name = @event.Name;
            if (!string.IsNullOrWhiteSpace(@event.Description))
                eventEntity.Description = @event.Description;

            if (@event.StartingDate != null)
                eventEntity.StartingDate = (DateTime) @event.StartingDate;
            if (@event.EndingDate != null)
                eventEntity.EndingDate = (DateTime) @event.EndingDate;

            if (@event.WeeklyReset != null)
                eventEntity.WeeklyReset = (bool) @event.WeeklyReset;
            if (@event.IsActive != null)
                eventEntity.IsActive = (bool) @event.IsActive;

            _context.Events.Update(eventEntity);
            _context.SaveChanges();
            if (eventEntity.WeeklyReset)
                _jobManager.AddOrUpdate<IJobService>(eventEntity.Id, service => service.UpdateEvent(eventEntity.Id),
                    Cron.Weekly());
            else
                _jobManager.RemoveIfExists(eventEntity.Id);
        }

        public async Task RemoveEvent(string id)
        {
            var @event = await _context.Events.FindAsync(id);
            if (@event == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.EventNotFound, Message = _localizer["EventNotFound"]});
            await CheckAccess(@event.ChannelId);
            @event.IsDeleted = true;
            await _context.SaveChangesAsync();
        }

        public (string ChannelId, EventDto) GetEvent(string id)
        {
            var @event = _context.Events.Find(id);
            if (@event == null || @event.IsDeleted)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.EventNotFound, Message = _localizer["EventNotFound"]});

            return (@event.ChannelId, _mapper.Map<EventDto>(@event));
        }

        public (int, EventDto[]) SearchEvents(string channelId, string key, EventTime filter, EventType type,
            int pageNumber, int pageSize, EventFilter? sort = null)
        {
            if (!string.IsNullOrWhiteSpace(channelId))
            {
                var channel = _context.Channels.Find(channelId);
                if (channel == null)
                    throw new ExceptionService(new Error
                        {Code = ErrorCode.ChannelNotFound, Message = _localizer["ChannelNotFound"]});
            }


            key = key.Trim();
            var now = DateTime.UtcNow;

            var queryableEvents = _context.Events.Where(evt =>
                    (string.IsNullOrWhiteSpace(channelId) || evt.ChannelId == channelId) &&
                    (evt.Name.Contains(key) || evt.Description.Contains(key)) &&
                    (((filter & EventTime.Passed) > 0 && evt.EndingDate < now) ||
                     ((filter & EventTime.Running) > 0 && evt.StartingDate < now && evt.EndingDate > now) ||
                     ((filter & EventTime.Future) > 0 && evt.StartingDate > now)) &&
                    (((type & EventType.Casual) > 0 && !evt.WeeklyReset) ||
                     ((type & EventType.Weekly) > 0 && evt.WeeklyReset)) && evt.IsActive && !evt.IsDeleted)
                .OrderBy(evt =>
                    sort == null || sort == EventFilter.Sell ? evt.CreateDate :
                    sort == EventFilter.StartDate ? evt.StartingDate : evt.EndingDate);

            var total = queryableEvents.Count();

            var events = queryableEvents.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToArray();
            return (total, _mapper.Map<EventDto[]>(events));
        }

        public EventDto[] GetBestEvents(int size)
        {
            var entityEven = _context.Events.OrderByDescending(evt => evt.Items.Count).Take(size).ToArray();
            return _mapper.Map<EventDto[]>(entityEven);
        }

        public async Task<UserTicketDto[]> GetEventTickets(string eventId)
        {
            var @event = await _context.Events.FindAsync(eventId);
            if (@event == null)
                throw new ExceptionService(new Error
                    {Code = ErrorCode.EventNotFound, Message = _localizer["EventNotFound"]});
            await CheckAccess(@event.ChannelId);
            var tickets = _context.Tickets.Where(tck => tck.Item.EventId == eventId).Include(tck => tck.Item)
                .Include(tck => tck.User);
            return _mapper.Map<UserTicketDto[]>(tickets);
        }

        private async Task CheckAccess(string channelId)
        {
            var userId = _contextAccessor.HttpContext?.User.FindFirstValue(ClaimTypes.NameIdentifier);

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                throw new ExceptionService(new Error
                {
                    Code = ErrorCode.UserNotFound, Message = _localizer["UserNotFound"]
                });
            var roles = await _userManager.GetRolesAsync(user);

            var channelSupervisor =
                _context.ChannelSupervisors.FirstOrDefault(chs => chs.UserId == userId && chs.ChannelId == channelId);
            if (channelSupervisor == null && !roles.Contains("Admin"))
                throw new ExceptionService(new Error
                {
                    Code = ErrorCode.AccessDenied, Message = _localizer["AccessDenied"]
                });
        }
    }
}