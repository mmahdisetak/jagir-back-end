﻿using System;
using Domain.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Infrastructure.Persistence
{
    public class DatabaseContext : IdentityDbContext<User>
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> option) : base(option)
        {
        }

        public DbSet<Channel> Channels { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Like> Likes { get; set; }
        public DbSet<Poll> Polls { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<FileEntity> Files { get; set; }
        public DbSet<ChannelFollower> ChannelFollowers { get; set; }
        public DbSet<ChannelSupervisor> ChannelSupervisors { get; set; }
        public DbSet<InitializeHistory> InitializeHistories { set; get; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var dateTimeConverter = new ValueConverter<DateTime, DateTime>(
                v => v.ToUniversalTime(),
                v => DateTime.SpecifyKind(v, DateTimeKind.Utc));

            var nullableDateTimeConverter = new ValueConverter<DateTime?, DateTime?>(
                v => v.HasValue ? v.Value.ToUniversalTime() : v,
                v => v.HasValue ? DateTime.SpecifyKind(v.Value, DateTimeKind.Utc) : v);

            foreach (var entityType in modelBuilder.Model.GetEntityTypes())
            {
                if (entityType.IsKeyless)
                    continue;

                foreach (var property in entityType.GetProperties())
                {
                    if (property.ClrType == typeof(DateTime))
                        property.SetValueConverter(dateTimeConverter);
                    else if (property.ClrType == typeof(DateTime?))
                        property.SetValueConverter(nullableDateTimeConverter);
                }
            }

            modelBuilder.Entity<User>(user =>
            {
                user.HasKey(u => u.Id);
                user.Property(u => u.Id).ValueGeneratedOnAdd();
                user.Property(u => u.Email).IsRequired();
                user.HasIndex(u => u.NormalizedEmail).IsUnique();
                user.HasIndex(u => u.UserName).IsUnique();
                user.Property(u => u.RegisterDate).HasDefaultValueSql("now() at time zone 'utc'").ValueGeneratedOnAdd();
                user.HasOne(u => u.Avatar).WithMany().HasForeignKey(u => u.AvatarId);
            });

            modelBuilder.Entity<FileEntity>(file =>
            {
                file.HasKey(f => f.Id);
                file.Property(f => f.Id).ValueGeneratedOnAdd();
                file.Property(f => f.IsDeleted).HasDefaultValue(false);
                file.Property(f => f.CreateDate).HasDefaultValueSql("now() at time zone 'utc'").ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Channel>(channel =>
            {
                channel.HasKey(c => c.Id);
                channel.Property(c => c.Id).ValueGeneratedOnAdd();
                channel.HasOne(c => c.Creator).WithMany().HasForeignKey(c => c.CreatorId);
                channel.HasOne(c => c.Owner).WithMany().HasForeignKey(c => c.OwnerId);
                channel.HasOne(c => c.Avatar).WithMany().HasForeignKey(c => c.AvatarId);
                channel.HasOne(c => c.Banner).WithMany().HasForeignKey(c => c.BannerId);
                channel.Property(c => c.CreateDate).HasDefaultValueSql("now() at time zone 'utc'")
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Comment>(comment =>
            {
                comment.HasKey(c => c.Id);
                comment.Property(c => c.Id).ValueGeneratedOnAdd();
                comment.HasOne(c => c.User).WithMany(usr => usr.Comments).HasForeignKey(c => c.UserId);
                comment.HasOne(c => c.Event).WithMany(evt => evt.Comments).HasForeignKey(c => c.EventId);
                comment.Property(c => c.CreateDate).HasDefaultValueSql("now() at time zone 'utc'")
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Ticket>(ticket =>
            {
                ticket.HasKey(t => t.Id);
                ticket.Property(t => t.Id).ValueGeneratedOnAdd();
                ticket.HasOne(t => t.User).WithMany(usr => usr.Tickets).HasForeignKey(t => t.UserId);
                ticket.HasOne(t => t.Item).WithMany().HasForeignKey(t => t.ItemId);
                ticket.Property(t => t.CreateDate).HasDefaultValueSql("now() at time zone 'utc'").ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Poll>(poll =>
            {
                poll.HasKey(p => p.Id);
                poll.Property(p => p.Id).ValueGeneratedOnAdd();
                poll.HasOne(p => p.User).WithMany().HasForeignKey(p => p.UserId);
                poll.HasOne(p => p.Item).WithMany().HasForeignKey(p => p.ItemId);
            });

            modelBuilder.Entity<Like>(like =>
            {
                like.HasKey(l => l.Id);
                like.Property(l => l.Id).ValueGeneratedOnAdd();
                like.HasOne(l => l.User).WithMany().HasForeignKey(l => l.UserId);
                like.HasOne(l => l.Comment).WithMany().HasForeignKey(l => l.CommentId);
            });

            modelBuilder.Entity<Event>(@event =>
            {
                @event.HasKey(e => e.Id);
                @event.Property(e => e.Id).ValueGeneratedOnAdd();
                @event.HasOne(e => e.Channel).WithMany(ch => ch.Events).HasForeignKey(e => e.ChannelId);
                @event.HasOne(c => c.Avatar).WithMany().HasForeignKey(c => c.AvatarId);
                @event.Property(e => e.CreateDate).HasDefaultValueSql("now() at time zone 'utc'").ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Item>(item =>
            {
                item.HasKey(i => i.Id);
                item.Property(i => i.Id).ValueGeneratedOnAdd();
                item.HasMany(i => i.Polls).WithOne();
                item.HasOne(i => i.Event).WithMany(evt => evt.Items).HasForeignKey(i => i.EventId);
            });

            modelBuilder.Entity<ChannelFollower>(channelFollower =>
            {
                channelFollower.HasKey(cf => cf.Id);
                channelFollower.Property(cf => cf.Id).ValueGeneratedOnAdd();
                channelFollower.HasOne(cf => cf.Channel).WithMany(c => c.Followers).HasForeignKey(cf => cf.ChannelId)
                    .OnDelete(DeleteBehavior.Cascade);
                channelFollower.HasOne(cf => cf.User).WithMany(u => u.FollowingChannels).HasForeignKey(cf => cf.UserId);
            });

            modelBuilder.Entity<ChannelSupervisor>(channelSupervisor =>
            {
                channelSupervisor.HasKey(cf => cf.Id);
                channelSupervisor.Property(cf => cf.Id).ValueGeneratedOnAdd();
                channelSupervisor.HasOne(cf => cf.Channel).WithMany(c => c.Supervisors)
                    .HasForeignKey(cf => cf.ChannelId);
                channelSupervisor.HasOne(cf => cf.User).WithMany(u => u.SupervisingChannels)
                    .HasForeignKey(cf => cf.UserId);
            });

            modelBuilder.Entity<TicketItem>(ticketItem =>
            {
                ticketItem.HasKey(ti => ti.Id);
                ticketItem.Property(ti => ti.Id).ValueGeneratedOnAdd();
                ticketItem.HasOne(ti => ti.Ticket).WithMany().HasForeignKey(ti => ti.TicketId);
                ticketItem.HasOne(ti => ti.Item).WithMany().HasForeignKey(ti => ti.ItemId);
            });

            modelBuilder.Entity<InitializeHistory>(history =>
            {
                history.HasKey(s => s.Version);
                history.Property(n => n.DateTime).HasDefaultValueSql("now() at time zone 'utc'");
            });
        }
    }
}