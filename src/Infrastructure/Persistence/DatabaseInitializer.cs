﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Application.Resources;
using Domain.Entities;
using Hangfire;
using Infrastructure.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;

namespace Infrastructure.Persistence
{
    public class DatabaseInitializer
    {
        private DatabaseContext Context { get; }
        private UserManager<User> UserManager { get; }
        private RoleManager<IdentityRole> RoleManager { get; }
        private IConfiguration Configuration { get; }
        private IRecurringJobManager JobManager { get; }
        private IStringLocalizer<SharedResource> Localizer { get; }
        private string Directory { get; }
        private const int UsersNumber = 100;
        private readonly List<FileEntity> _profiles = new();
        private readonly List<User> _users = new();

        public DatabaseInitializer(IServiceProvider scopeServiceProvider)
        {
            Context = scopeServiceProvider.GetService<DatabaseContext>();
            UserManager = scopeServiceProvider.GetService<UserManager<User>>();
            RoleManager = scopeServiceProvider.GetService<RoleManager<IdentityRole>>();
            Configuration = scopeServiceProvider.GetService<IConfiguration>();
            Localizer = scopeServiceProvider.GetService<IStringLocalizer<SharedResource>>();
            JobManager = scopeServiceProvider.GetService<IRecurringJobManager>();
            Directory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        }

        public async Task Initialize()
        {
            try
            {
                // await Context.Database.MigrateAsync();
                await Context.Database.EnsureDeletedAsync();
                await Context.Database.EnsureCreatedAsync();
                await Initializer();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private async Task Initializer()
        {
            const string version = "V1";

            if (Context.InitializeHistories.Any(history => history.Version == version))
                return;

            await RoleInitializer();
            await ProfileInitializer();
            await AdminInitializer();
            await UserInitializer();
            await ChannelInitializer();
            await EventInitializer();
            await FollowersInitializer();
            await SupervisorInitializer();
            await Context.InitializeHistories.AddAsync(new InitializeHistory
            {
                Version = version
            });
            await Context.SaveChangesAsync();
        }

        private async Task SupervisorInitializer()
        {
            var supervisors = new List<User>();
            var mod = _profiles.Count + 1;
            for (var i = 0; i < 5; i++)
            {
                var user = new User
                {
                    UserName = $"MarkopSupervisor{i}", FirstName = $"Supervisor{i}", LastName = "Markop",
                    Email = $"Supervisor{i}@Markop.ir", Avatar = i % mod != mod - 1 ? _profiles[i % mod] : null
                };
                await AddUser(user, "SupervisorPassword", "User", true);
                supervisors.Add(user);
            }

            await Context.Channels.ForEachAsync(ch =>
                supervisors.ForEach(usr =>
                {
                    ch.Supervisors ??= new List<ChannelSupervisor>();
                    ch.Supervisors.Add(new ChannelSupervisor {Channel = ch, User = usr});
                }));
            await Context.SaveChangesAsync();
        }

        private async Task FollowersInitializer()
        {
            await Context.Channels.ForEachAsync(ch =>
                _users.ForEach(usr =>
                {
                    ch.Followers ??= new List<ChannelFollower>();
                    ch.Followers.Add(new ChannelFollower {Channel = ch, User = usr});
                }));
            await Context.SaveChangesAsync();
        }

        private async Task ProfileInitializer()
        {
            _profiles.Add(await UploadFile(File.OpenRead(Directory + "/Persistence/Files/avatars/001-man.png")));
            _profiles.Add(await UploadFile(File.OpenRead(Directory + "/Persistence/Files/avatars/002-old man.png")));
            _profiles.Add(await UploadFile(File.OpenRead(Directory + "/Persistence/Files/avatars/003-woman.png")));
            _profiles.Add(await UploadFile(File.OpenRead(Directory + "/Persistence/Files/avatars/004-woman.png")));
            _profiles.Add(await UploadFile(File.OpenRead(Directory + "/Persistence/Files/avatars/005-woman.png")));
            _profiles.Add(await UploadFile(File.OpenRead(Directory + "/Persistence/Files/avatars/006-man.png")));
            _profiles.Add(await UploadFile(File.OpenRead(Directory + "/Persistence/Files/avatars/007-man.png")));
            _profiles.Add(await UploadFile(File.OpenRead(Directory + "/Persistence/Files/avatars/008-woman.png")));
            _profiles.Add(await UploadFile(File.OpenRead(Directory + "/Persistence/Files/avatars/009-woman.png")));
            _profiles.Add(await UploadFile(File.OpenRead(Directory + "/Persistence/Files/avatars/010-woman.png")));
            _profiles.Add(await UploadFile(File.OpenRead(Directory + "/Persistence/Files/avatars/011-man.png")));
            _profiles.Add(await UploadFile(File.OpenRead(Directory + "/Persistence/Files/avatars/012-man.png")));
            _profiles.Add(await UploadFile(File.OpenRead(Directory + "/Persistence/Files/avatars/013-man.png")));
            _profiles.Add(await UploadFile(File.OpenRead(Directory + "/Persistence/Files/avatars/014-woman.png")));
            _profiles.Add(await UploadFile(File.OpenRead(Directory + "/Persistence/Files/avatars/015-woman.png")));
            _profiles.Add(await UploadFile(File.OpenRead(Directory + "/Persistence/Files/avatars/016-man.png")));
            _profiles.Add(await UploadFile(File.OpenRead(Directory + "/Persistence/Files/avatars/017-old man.png")));
            _profiles.Add(await UploadFile(File.OpenRead(Directory + "/Persistence/Files/avatars/018-woman.png")));
            _profiles.Add(await UploadFile(File.OpenRead(Directory + "/Persistence/Files/avatars/019-woman.png")));
            _profiles.Add(await UploadFile(File.OpenRead(Directory + "/Persistence/Files/avatars/020-man.png")));
        }

        private async Task AddUser(User user, string password, string role, bool confirmed)
        {
            await UserManager.CreateAsync(user, password);
            await UserManager.AddToRoleAsync(user, role);
            if (!confirmed) return;
            var token = await UserManager.GenerateEmailConfirmationTokenAsync(user);
            await UserManager.ConfirmEmailAsync(user, token);
        }

        private async Task UserInitializer()
        {
            var mod = _profiles.Count + 1;
            for (var i = 0; i < UsersNumber; i++)
            {
                var user = new User
                {
                    UserName = $"MarkopUser{i}", FirstName = $"User{i}", LastName = "Markop",
                    Email = $"User{i}@Markop.ir", Avatar = i % mod != mod - 1 ? _profiles[i % mod] : null
                };
                await AddUser(user, "UserPassword", "User", true);
                _users.Add(user);
            }

            await Context.SaveChangesAsync();
        }

        private async Task AdminInitializer()
        {
            var owner = new User
            {
                UserName = "owner", FirstName = "Owner", LastName = "Jagir",
                Email = "owner@jagir.ir", Avatar = _profiles[16], Credit = 100000
            };
            await AddUser(owner, "OwnerPassword", "Admin", true);

            var admin1 = new User
            {
                UserName = "MMahdiSetak", FirstName = "M.Mahdi", LastName = "Setak", Email = "M.Mahdi.khu@gmail.com",
                Avatar = _profiles[15], Credit = 100000
            };
            await AddUser(admin1, "AdminPassword", "Admin", true);

            var admin2 = new User
            {
                UserName = "MehrabToghani", FirstName = "Mehrab", LastName = "Toghani",
                Email = "mehrabtoghani@gmail.com", Avatar = _profiles[19], Credit = 100000
            };
            await AddUser(admin2, "AdminPassword", "Admin", true);

            var admin3 = new User
            {
                UserName = "MohadeseSafari", FirstName = "Mohadese", LastName = "Safari",
                Email = "mohadese.safari@ymail.com", Avatar = _profiles[14], Credit = 100000
            };
            await AddUser(admin3, "AdminPassword", "Admin", true);

            var admin4 = new User
            {
                UserName = "BijanEisapour", FirstName = "Bijan", LastName = "Eisapour",
                Email = "bijaneisapour@gmail.com", Avatar = _profiles[10], Credit = 100000
            };
            await AddUser(admin4, "AdminPassword", "Admin", true);

            await Context.SaveChangesAsync();
        }

        private async Task RoleInitializer()
        {
            await RoleManager.CreateAsync(new IdentityRole {Name = "Admin"});
            await RoleManager.CreateAsync(new IdentityRole {Name = "User"});
        }

        private async Task ChannelInitializer()
        {
            var owner = await UserManager.FindByNameAsync("owner");
            var channel1 = new Channel
            {
                Name = "تربیت بدنی",
                Avatar = await UploadFile(File.OpenRead(Directory + "/Persistence/Files/sport_avatar.png")),
                Banner = await UploadFile(File.OpenRead(Directory + "/Persistence/Files/sport_banner.jpg")),
                Creator = owner,
                Description = @"اطلاع‌رسانی سانس‌های مجتمع‌های ورزشی شامل استخر، سالن فوتسال و سالن بدنسازی
                برگزاری کلاس‌های آموزشی از مبتدی تا پیشرفته با مباحث فوتبال، والیبال، بسکتبال و بدنسازی
                میزبانی مسابقات ورزشی در سطح شهر، استان و کشور
                حمایت مالی از دانشجویان ورزشکاری که در مسابقات دانشگاهی مدال کسب کنند"
            };
            await Context.Channels.AddAsync(channel1);

            var channel2 = new Channel
            {
                Name = "فرهنگ و هنر",
                Banner = await UploadFile(File.OpenRead(Directory + "/Persistence/Files/art_banner.jpg")),
                Creator = owner,
                Description = @"برگزاری رویدادها و همایش‌های فرهنگی-هنری
                تمرکز بر روی بالابردن اطلاعات عمومی دانشجویان با برگزاری رویدادهای مربوطه
                برگزاری همایش‌های فرهنگی با هدفِ بومی‌سازی صنایع و تکنولوژی‌های محبوب"
            };
            await Context.Channels.AddAsync(channel2);

            var channel3 = new Channel
            {
                Name = "مهندسی کامپیوتر",
                Avatar = await UploadFile(File.OpenRead(Directory + "/Persistence/Files/computer_avatar.png")),
                Banner = await UploadFile(File.OpenRead(Directory + "/Persistence/Files/computer_banner.jpg")),
                Creator = owner,
                Description = @"⚜️ انجمن علمی مهندسی کامپیوتر دانشگاه خوارزمی ⚜️
                آخرین اخبار دنیای مهندسی کامپیوتر و تکنولوژی‌های نوظهور
                حداقل یک همایش در هر ماه با دعوت از اساتید برتر حوزۀ فناوری اطلاعات
                کارگاه‌های آموزشی با حضور اساتید مجرب و پیشرو در صنعت"
            };
            await Context.Channels.AddAsync(channel3);

            var channel4 = new Channel
            {
                Name = "مرکز مشاوره",
                Avatar = await UploadFile(File.OpenRead(Directory + "/Persistence/Files/psycho_avatar.png")),
                Banner = await UploadFile(File.OpenRead(Directory + "/Persistence/Files/psycho_banner.jpg")),
                Creator = owner,
                Description = @"رزرو نوبت مشاوره و روانکاوی نزد مجرب‌ترین متخصصان
                اطلاع‌رسانی تایم‌های خالی به صورت سریع و منظم
                برگزاری کارگاه‌های رایگان با حضور اساتید برتر سراسر کشور"
            };
            await Context.Channels.AddAsync(channel4);
            await Context.SaveChangesAsync();
        }

        private async Task EventInitializer()
        {
            var compChannel = Context.Channels.FirstOrDefault(ch => ch.Name == "مهندسی کامپیوتر");
            var now = DateTime.UtcNow;
            var fullDay = TimeSpan.FromHours(24);
            var twoMonth = TimeSpan.FromDays(60);
            var halfMonth = TimeSpan.FromDays(15);
            var halfDay = TimeSpan.FromHours(12);
            var quarterDay = TimeSpan.FromHours(6);
            var hour = TimeSpan.FromHours(1);
            var event1 = new Event
            {
                Name = "همایش هوش مصنوعی", Description = "مرور اخبار و فناوری های روز حوزه هوش مصنوعی", IsActive = true,
                Channel = compChannel, StartingDate = now - fullDay, EndingDate = now + twoMonth, WeeklyReset = false,
                Avatar = await UploadFile(File.OpenRead(Directory + "/Persistence/Files/AI_avatar.jpg"))
            };
            var item1 = new Item
            {
                Name = "صندلی معمولی", Event = event1, Capacity = 30, Price = 10000, IsActive = true, LimitPerUser = 1
            };
            var item2 = new Item
            {
                Name = "صندلی ویژه", Event = event1, Capacity = 10, Price = 20000, IsActive = true, LimitPerUser = 1
            };
            await Context.Events.AddAsync(event1);
            await Context.Items.AddAsync(item1);
            await Context.Items.AddAsync(item2);
            var event2 = new Event
            {
                Name = "همایش کلان داده", Description = "مرور اخبار و فناوری های روز حوزه کلان داده", IsActive = true,
                Channel = compChannel, StartingDate = now - halfMonth, EndingDate = now - quarterDay,
                WeeklyReset = false,
                Avatar = await UploadFile(File.OpenRead(Directory + "/Persistence/Files/bigdata_avatar.jpg"))
            };
            var item3 = new Item
            {
                Name = "صندلی معمولی", Event = event2, Capacity = 30, Price = 10000, IsActive = true, LimitPerUser = 1
            };
            var item4 = new Item
            {
                Name = "صندلی ویژه", Event = event2, Capacity = 10, Price = 20000, IsActive = true, LimitPerUser = 1
            };
            await Context.Events.AddAsync(event2);
            await Context.Items.AddAsync(item3);
            await Context.Items.AddAsync(item4);
            var event3 = new Event
            {
                Name = "همایش امنیت", Description = "مرور اخبار و فناوری های روز حوزه امنیت", IsActive = true,
                Channel = compChannel, StartingDate = now + halfMonth, EndingDate = now + twoMonth, WeeklyReset = false,
                Avatar = await UploadFile(File.OpenRead(Directory + "/Persistence/Files/security_avatar.jpg"))
            };
            var item5 = new Item
            {
                Name = "صندلی معمولی", Event = event2, Capacity = 30, Price = 10000, IsActive = true, LimitPerUser = 1
            };
            var item6 = new Item
            {
                Name = "صندلی ویژه", Event = event2, Capacity = 10, Price = 20000, IsActive = true, LimitPerUser = 1
            };
            await Context.Events.AddAsync(event3);
            await Context.Items.AddAsync(item5);
            await Context.Items.AddAsync(item6);


            var sportChannel = Context.Channels.FirstOrDefault(ch => ch.Name == "تربیت بدنی");
            var event4 = new Event
            {
                Name = "سالن های فوتسال", Description = "رویداد رزرو هفتگی سالن های فوتسال دانشگاه خوارزمی",
                IsActive = true,
                Channel = sportChannel, StartingDate = now - halfMonth, EndingDate = now + twoMonth,
                WeeklyReset = true,
                Avatar = await UploadFile(File.OpenRead(Directory + "/Persistence/Files/soccer_avatar.jpg"))
            };
            var item7 = new Item
            {
                Name = "سالن نشاط", Event = event4, Capacity = 20, Price = 5000, IsActive = true, LimitPerUser = 1,
                Schedule = new DateTime(2021, 8, 2, 11, 30, 0)
            };
            var item8 = new Item
            {
                Name = "سالن نشاط", Event = event4, Capacity = 20, Price = 5000, IsActive = true, LimitPerUser = 1,
                Schedule = new DateTime(2021, 8, 3, 13, 30, 0)
            };
            var item9 = new Item
            {
                Name = "سالن تندرستی", Event = event4, Capacity = 20, Price = 5000, IsActive = true, LimitPerUser = 1,
                Schedule = new DateTime(2021, 8, 4, 11, 30, 0)
            };
            var item10 = new Item
            {
                Name = "سالن تندرستی", Event = event4, Capacity = 20, Price = 5000, IsActive = true, LimitPerUser = 1,
                Schedule = new DateTime(2021, 8, 5, 13, 30, 0)
            };
            var item11 = new Item
            {
                Name = "سالن عیدی زاده", Event = event4, Capacity = 20, Price = 5000, IsActive = true, LimitPerUser = 1,
                Schedule = new DateTime(2021, 8, 4, 11, 30, 0)
            };
            var item12 = new Item
            {
                Name = "سالن عیدی زاده", Event = event4, Capacity = 20, Price = 5000, IsActive = true, LimitPerUser = 1,
                Schedule = new DateTime(2021, 8, 15, 13, 30, 0)
            };
            var item13 = new Item
            {
                Name = "سالن عیدی زاده", Event = event4, Capacity = 20, Price = 5000, IsActive = true, LimitPerUser = 1,
                Schedule = new DateTime(2021, 8, 10, 13, 30, 0)
            };
            var item14 = new Item
            {
                Name = "سالن پهلوانان", Event = event4, Capacity = 20, Price = 5000, IsActive = true, LimitPerUser = 1,
                Schedule = new DateTime(2021, 8, 8, 14, 30, 0)
            };
            await Context.Events.AddAsync(event4);
            await Context.Items.AddAsync(item7);
            await Context.Items.AddAsync(item8);
            await Context.Items.AddAsync(item9);
            await Context.Items.AddAsync(item10);
            await Context.Items.AddAsync(item11);
            await Context.Items.AddAsync(item12);
            await Context.Items.AddAsync(item13);
            await Context.Items.AddAsync(item14);
            var event5 = new Event
            {
                Name = "استخر", Description = "رویداد هفتگی رزرو سانس های استخر دانشگاه خوارزمی", IsActive = true,
                Channel = sportChannel, StartingDate = now - halfMonth, EndingDate = now - fullDay,
                WeeklyReset = false,
                Avatar = await UploadFile(File.OpenRead(Directory + "/Persistence/Files/swim_avatar.jpg"))
            };
            var item15 = new Item
            {
                Name = "سانس های آقایان", Event = event5, Capacity = 40, Price = 10000, IsActive = true,
                LimitPerUser = 1,
                Schedule = new DateTime(2021, 8, 13, 10, 30, 0)
            };
            var item16 = new Item
            {
                Name = "سانس های آقایان", Event = event5, Capacity = 40, Price = 10000, IsActive = true,
                LimitPerUser = 1,
                Schedule = new DateTime(2021, 8, 13, 13, 0, 0)
            };
            var item17 = new Item
            {
                Name = "سانس های آقایان", Event = event5, Capacity = 40, Price = 10000, IsActive = true,
                LimitPerUser = 1,
                Schedule = new DateTime(2021, 8, 15, 10, 30, 0)
            };
            var item18 = new Item
            {
                Name = "سانس های آقایان", Event = event5, Capacity = 40, Price = 10000, IsActive = true,
                LimitPerUser = 1,
                Schedule = new DateTime(2021, 8, 15, 13, 0, 0)
            };
            var item19 = new Item
            {
                Name = "سانس های آقایان", Event = event5, Capacity = 40, Price = 10000, IsActive = true,
                LimitPerUser = 1,
                Schedule = new DateTime(2021, 8, 17, 10, 30, 0)
            };
            var item20 = new Item
            {
                Name = "سانس های آقایان", Event = event5, Capacity = 40, Price = 10000, IsActive = true,
                LimitPerUser = 1,
                Schedule = new DateTime(2021, 8, 17, 13, 0, 0)
            };
            var item21 = new Item
            {
                Name = "سانس های بانوان", Event = event5, Capacity = 40, Price = 10000, IsActive = true,
                LimitPerUser = 1,
                Schedule = new DateTime(2021, 8, 14, 10, 30, 0)
            };
            var item22 = new Item
            {
                Name = "سانس های بانوان", Event = event5, Capacity = 40, Price = 10000, IsActive = true,
                LimitPerUser = 1,
                Schedule = new DateTime(2021, 8, 14, 13, 0, 0)
            };
            var item23 = new Item
            {
                Name = "سانس های بانوان", Event = event5, Capacity = 40, Price = 10000, IsActive = true,
                LimitPerUser = 1,
                Schedule = new DateTime(2021, 8, 16, 10, 30, 0)
            };
            var item24 = new Item
            {
                Name = "سانس های بانوان", Event = event5, Capacity = 40, Price = 10000, IsActive = true,
                LimitPerUser = 1,
                Schedule = new DateTime(2021, 8, 16, 13, 0, 0)
            };
            var item25 = new Item
            {
                Name = "سانس های بانوان", Event = event5, Capacity = 40, Price = 10000, IsActive = true,
                LimitPerUser = 1,
                Schedule = new DateTime(2021, 8, 18, 10, 30, 0)
            };
            var item26 = new Item
            {
                Name = "سانس های بانوان", Event = event5, Capacity = 40, Price = 10000, IsActive = true,
                LimitPerUser = 1,
                Schedule = new DateTime(2021, 8, 18, 13, 0, 0)
            };
            await Context.Events.AddAsync(event5);
            await Context.Items.AddAsync(item15);
            await Context.Items.AddAsync(item16);
            await Context.Items.AddAsync(item17);
            await Context.Items.AddAsync(item18);
            await Context.Items.AddAsync(item19);
            await Context.Items.AddAsync(item20);
            await Context.Items.AddAsync(item21);
            await Context.Items.AddAsync(item22);
            await Context.Items.AddAsync(item23);
            await Context.Items.AddAsync(item24);
            await Context.Items.AddAsync(item25);
            await Context.Items.AddAsync(item26);
            var event6 = new Event
            {
                Name = "دوشنبه های شاد", Description = "رویداد مسابقات هفتگی ورزشی دانشگاه خوارزمی", IsActive = true,
                Channel = sportChannel, StartingDate = now - fullDay, EndingDate = now + halfMonth, WeeklyReset = true,
                Avatar = await UploadFile(File.OpenRead(Directory + "/Persistence/Files/happy_avatar.jpg"))
            };
            var item27 = new Item
            {
                Name = "مسابقه طناب کشی", Event = event6, Capacity = 10, Price = 0, IsActive = true, LimitPerUser = 1
            };
            var item28 = new Item
            {
                Name = "مسابقه فوتبال دستی", Event = event6, Capacity = 16, Price = 0, IsActive = true, LimitPerUser = 1
            };
            var item29 = new Item
            {
                Name = "مسابقه روپایی", Event = event6, Capacity = 20, Price = 0, IsActive = true, LimitPerUser = 1
            };
            await Context.Events.AddAsync(event6);
            await Context.Items.AddAsync(item27);
            await Context.Items.AddAsync(item28);
            await Context.Items.AddAsync(item29);
            var event7 = new Event
            {
                Name = "همایش هوش مصنوعی", Description = "مرور اخبار و فناوری های روز حوزه هوش مصنوعی", IsActive = true,
                Channel = compChannel, StartingDate = now + fullDay, EndingDate = now + twoMonth, WeeklyReset = false,
                Avatar = await UploadFile(File.OpenRead(Directory + "/Persistence/Files/AI1_avatar.jpg"))
            };
            var event8 = new Event
            {
                Name = "همایش کلان داده", Description = "مرور اخبار و فناوری های روز حوزه کلان داده", IsActive = true,
                Channel = compChannel, StartingDate = now + quarterDay, EndingDate = now + fullDay, WeeklyReset = false,
                Avatar = await UploadFile(File.OpenRead(Directory + "/Persistence/Files/bigdata_avatar.jpg"))
            };
            var event9 = new Event
            {
                Name = "همایش امنیت", Description = "مرور اخبار و فناوری های روز حوزه امنیت", IsActive = true,
                Channel = compChannel, StartingDate = now - fullDay, EndingDate = now - halfDay, WeeklyReset = false,
                Avatar = await UploadFile(File.OpenRead(Directory + "/Persistence/Files/security_avatar.jpg"))
            };
            var event10 = new Event
            {
                Name = "همایش هوش مصنوعی", Description = "مرور اخبار و فناوری های روز حوزه هوش مصنوعی", IsActive = true,
                Channel = compChannel, StartingDate = now - fullDay, EndingDate = now - quarterDay, WeeklyReset = false,
                Avatar = await UploadFile(File.OpenRead(Directory + "/Persistence/Files/AI2_avatar.jpg"))
            };
            var event11 = new Event
            {
                Name = "همایش کلان داده", Description = "مرور اخبار و فناوری های روز حوزه کلان داده", IsActive = true,
                Channel = compChannel, StartingDate = now - twoMonth, EndingDate = now + twoMonth, WeeklyReset = false,
                Avatar = await UploadFile(File.OpenRead(Directory + "/Persistence/Files/bigdata1_avatar.jpg"))
            };
            var event12 = new Event
            {
                Name = "همایش امنیت", Description = "مرور اخبار و فناوری های روز حوزه امنیت", IsActive = true,
                Channel = compChannel, StartingDate = now - hour, EndingDate = now + fullDay, WeeklyReset = false,
                Avatar = await UploadFile(File.OpenRead(Directory + "/Persistence/Files/security1_avatar.jpg"))
            };
            var event13 = new Event
            {
                Name = "همایش هوش مصنوعی", Description = "مرور اخبار و فناوری های روز حوزه هوش مصنوعی", IsActive = true,
                Channel = compChannel, StartingDate = now - halfMonth, EndingDate = now + fullDay, WeeklyReset = false,
                Avatar = await UploadFile(File.OpenRead(Directory + "/Persistence/Files/AI3_avatar.jpg"))
            };
            var event14 = new Event
            {
                Name = "همایش کلان داده", Description = "مرور اخبار و فناوری های روز حوزه کلان داده", IsActive = true,
                Channel = compChannel, StartingDate = now - twoMonth, EndingDate = now - quarterDay,
                WeeklyReset = false,
                Avatar = await UploadFile(File.OpenRead(Directory + "/Persistence/Files/bigdata2_avatar.jpg"))
            };
            var event15 = new Event
            {
                Name = "همایش امنیت", Description = "مرور اخبار و فناوری های روز حوزه امنیت", IsActive = true,
                Channel = compChannel, StartingDate = now + halfDay, EndingDate = now + halfMonth, WeeklyReset = false,
                Avatar = await UploadFile(File.OpenRead(Directory + "/Persistence/Files/security2_avatar.jpg"))
            };
            await Context.Events.AddAsync(event7);
            await Context.Events.AddAsync(event8);
            await Context.Events.AddAsync(event9);
            await Context.Events.AddAsync(event10);
            await Context.Events.AddAsync(event11);
            await Context.Events.AddAsync(event12);
            await Context.Events.AddAsync(event13);
            await Context.Events.AddAsync(event14);
            await Context.Events.AddAsync(event15);
            JobManager.AddOrUpdate<IJobService>(event4.Id, service => service.UpdateEvent(event4.Id), Cron.Weekly());
            JobManager.AddOrUpdate<IJobService>(event6.Id, service => service.UpdateEvent(event6.Id), Cron.Weekly());
        }

        private async Task<FileEntity> UploadFile(FileStream fileStream)
        {
            var file = new FileEntity
            {
                Name = Path.GetFileName(fileStream.Name),
                Size = fileStream.Length,
                ContentType = "image/png"
            };
            await Context.Files.AddAsync(file);
            var stream = File.Create(Configuration["StorePath"] + file.Id);
            await fileStream.CopyToAsync(stream);
            return file;
        }
    }
}