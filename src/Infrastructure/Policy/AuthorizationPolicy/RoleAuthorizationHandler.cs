﻿using System.Linq;
using System.Threading.Tasks;
using Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace Infrastructure.Policy.AuthorizationPolicy
{
    public class RoleAuthorizationHandler : AuthorizationHandler<AuthorizationRequirements>
    {
        private readonly UserManager<User> _userManager;

        public RoleAuthorizationHandler(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context,
            AuthorizationRequirements requirement)
        {
            if (context.User.Identity is {IsAuthenticated: false})
                return;
            var userAsync = await _userManager.GetUserAsync(context.User);
            var userRoles = await _userManager.GetRolesAsync(userAsync);
            userRoles ??= System.Array.Empty<string>();

            if (requirement.RoleNames.Intersect(userRoles).ToList().Count != 0)
                context.Succeed(requirement);
        }
    }
}