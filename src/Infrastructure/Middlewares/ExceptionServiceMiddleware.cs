﻿using System;
using System.Threading.Tasks;
using Application.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Infrastructure.Middlewares
{
    public class ExceptionServiceMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionServiceMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, ILogger<ExceptionServiceMiddleware> logger)
        {
            try
            {
                await _next.Invoke(context);
            }
            catch (ExceptionService e)
            {
                context.Response.StatusCode = 200;
                context.Response.ContentType = "application/json; charset=utf-8";
                var newResponse = JsonConvert.SerializeObject(new BaseResponse
                {
                    Error = e.Error
                }, new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });
                await context.Response.WriteAsync(newResponse);
            }
            catch (Exception e)
            {
                // var logEvent = new LogEventInfo(LogLevel.Error, logger.Name, "Exception Occured")
                // {
                //     Exception = e
                // };
                // logger.Log(logEvent);
                logger.LogError(e, "Exception Occured");
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                context.Response.StatusCode = 200;
                context.Response.ContentType = "application/json; charset=utf-8";
                var newResponse = JsonConvert.SerializeObject(new BaseResponse {Error = Error.Unexpected()},
                    new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    });
                await context.Response.WriteAsync(newResponse);
            }
        }
    }
}