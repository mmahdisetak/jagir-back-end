﻿using Hangfire.Dashboard;

namespace Infrastructure.Identity
{
    public class HangFireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            return true;
        }
    }
}