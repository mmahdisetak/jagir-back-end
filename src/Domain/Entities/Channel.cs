﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class Channel
    {
        public string Id { get; set; }
        public User Creator { get; set; }
        public string CreatorId { get; set; }
        public User Owner { get; set; }
        public string OwnerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public FileEntity Avatar { get; set; }
        public string AvatarId { get; set; }
        public FileEntity Banner { get; set; }
        public string BannerId { get; set; }
        public ICollection<ChannelSupervisor> Supervisors { get; set; }
        public ICollection<Event> Events { get; set; }
        public ICollection<ChannelFollower> Followers { get; set; }
        public DateTime CreateDate { get; set; }
    }
}