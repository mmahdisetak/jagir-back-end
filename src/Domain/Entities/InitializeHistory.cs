﻿using System;

namespace Domain.Entities
{
    public class InitializeHistory
    {
        public string Version { set; get; }
        public DateTime DateTime { set; get; }
    }
}