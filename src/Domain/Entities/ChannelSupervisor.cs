﻿namespace Domain.Entities
{
    public class ChannelSupervisor
    {
        public string Id { get; set; }
        public Channel Channel { get; set; }
        public string ChannelId { get; set; }
        public User User { get; set; }
        public string UserId { get; set; }
    }
}