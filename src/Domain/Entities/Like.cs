﻿using Domain.Enums;

namespace Domain.Entities
{
    public class Like
    {
        public string Id { get; set; }
        public User User { get; set; }
        public string UserId { get; set; }
        public Comment Comment { get; set; }
        public string CommentId { get; set; }
        public LikeStatus Status { get; set; }
    }
}