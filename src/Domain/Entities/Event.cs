﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class Event
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public FileEntity Avatar { get; set; }
        public string AvatarId { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime StartingDate { get; set; }
        public DateTime EndingDate { get; set; }
        public bool WeeklyReset { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public ICollection<Item> Items { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public Channel Channel { get; set; }
        public string ChannelId { get; set; }
    }
}