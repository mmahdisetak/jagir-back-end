﻿namespace Domain.Entities
{
    public class Poll
    {
        public string Id { get; set; }
        public User User { get; set; }
        public string UserId { get; set; }
        public int Rate { get; set; }
        public Item Item { get; set; }
        public string ItemId { get; set; }
    }
}