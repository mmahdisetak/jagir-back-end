﻿using System;

namespace Domain.Entities
{
    public class FileEntity
    {
        public String Id { get; set; }
        public string Name { get; set; }
        public string ContentType { set; get; }
        public long Size { set; get; }
        public bool? IsDeleted { get; set; }
        public DateTime CreateDate { get; set; }
    }
}