﻿using System;

namespace Domain.Entities
{
    public class Ticket
    {
        public string Id { get; set; }
        public User User { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public long Price { get; set; }
        public Item Item { get; set; }
        public string ItemId { get; set; }
        public int QueueNumber { get; set; }
        public bool IsConfirmed { get; set; }
        public bool IsUsed { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? Schedule { get; set; }
    }
}