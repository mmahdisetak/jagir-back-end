﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class Item
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public int Capacity { get; set; }
        public int Booked { get; set; }
        public int LimitPerUser { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public ICollection<Poll> Polls { get; set; }
        public DateTime TicketExpireDate { get; set; }
        public DateTime? Schedule { get; set; }
        public Event Event { get; set; }
        public string EventId { get; set; }
    }
}