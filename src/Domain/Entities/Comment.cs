﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class Comment
    {
        public string Id { get; set; }
        public User User { get; set; }
        public string UserId { get; set; }
        public string Content { get; set; }
        public Event Event { get; set; }
        public string EventId { get; set; }
        public ICollection<Like> Likes { get; set; }
        public bool IsConfirmed { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ConfirmDate { get; set; }
    }
}