﻿namespace Domain.Entities
{
    public class TicketItem
    {
        public string Id { get; set; }
        public Ticket Ticket { get; set; }
        public string TicketId { get; set; }
        public Item Item { get; set; }
        public string ItemId { get; set; }
    }
}