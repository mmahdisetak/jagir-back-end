﻿using System;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class User : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public FileEntity Avatar { get; set; }
        public string AvatarId { get; set; }
        public long Credit { get; set; }
        public ICollection<Ticket> Tickets { get; set; }
        public ICollection<ChannelFollower> FollowingChannels { get; set; }
        public ICollection<ChannelSupervisor> SupervisingChannels { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public DateTime RegisterDate { get; set; }
    }
}