﻿using System;

namespace Domain.Enums
{
    [Flags]
    public enum EventTime
    {
        Passed = 1,
        Running = 2,
        Future = 4
    }
}