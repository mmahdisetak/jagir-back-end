﻿namespace Domain.Enums
{
    public enum SignInType
    {
        Email,
        UserName
    }
}