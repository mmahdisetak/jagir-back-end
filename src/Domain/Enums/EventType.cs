﻿using System;

namespace Domain.Enums
{
    [Flags]
    public enum EventType
    {
        Weekly = 1,
        Casual = 2
    }
}