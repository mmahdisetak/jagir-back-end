﻿namespace Domain.Enums
{
    public enum EventFilter
    {
        StartDate = 0,
        EndDate = 1,
        Sell = 2
    }
}