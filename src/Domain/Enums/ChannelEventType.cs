﻿namespace Domain.Enums
{
    public enum ChannelEventType
    {
        Channel,
        Event
    }
}