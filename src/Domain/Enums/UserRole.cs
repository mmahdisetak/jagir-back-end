﻿namespace Domain.Enums
{
    public enum UserRole
    {
        Follower,
        Supervisor,
        Owner
    }
}