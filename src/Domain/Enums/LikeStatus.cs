﻿namespace Domain.Enums
{
    public enum LikeStatus : short
    {
        Liked,
        Disliked
    }
}