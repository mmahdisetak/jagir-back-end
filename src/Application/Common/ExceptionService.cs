﻿using System;

namespace Application.Common
{
    public class ExceptionService : Exception
    {
        public Error Error { get; }

        public ExceptionService(Error error)
        {
            Error = error;
        }
    }
}