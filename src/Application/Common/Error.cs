using Domain.Enums;

namespace Application.Common
{
    public struct Error
    {
        public string Message { set; get; }
        public ErrorCode Code { set; get; }

        public static Error Unexpected()
        {
            return new Error
            {
                Code = ErrorCode.Unexpected,
                Message = "!خطای غیر منتظره"
            };
        }
    }
}