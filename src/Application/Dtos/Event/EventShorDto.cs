﻿using Application.Dtos.User;
using Domain.Enums;

namespace Application.Dtos.Event
{
    public class EventShortDto : IChannelEventDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AvatarId { get; set; }
        public ChannelEventType Type { get; set; }
        public int ItemsCount { get; set; }
        public bool IsWeekly { get; set; }
        public EventTime Status { get; set; }
    }
}