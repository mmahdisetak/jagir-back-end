﻿using System;

namespace Application.Dtos.Event
{
    public class EventDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string AvatarId { get; set; }
        public string Description { get; set; }
        public DateTime? StartingDate { get; set; }
        public DateTime? EndingDate { get; set; }
        public bool? WeeklyReset { get; set; }
        public bool? IsActive { get; set; }
    }
}