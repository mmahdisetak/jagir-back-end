﻿using System;
using Application.Dtos.User;

namespace Application.Dtos.Ticket
{
    public class UserTicketDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Event { get; set; }
        public string Channel { get; set; }
        public UserShortProfileDto User { get; set; }
        public long Price { get; set; }
        public int QueueNumber { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime Schedule { get; set; }
    }
}