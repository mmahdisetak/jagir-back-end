﻿using System;

namespace Application.Dtos.Item
{
    public class SessionDto
    {
        public string Id { get; set; }
        public int Booked { get; set; }
        public int? UserBooked { get; set; }
        public DateTime? Schedule { get; set; }
    }
}