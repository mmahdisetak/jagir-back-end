﻿using System;

namespace Application.Dtos.Item
{
    public class CreateItemDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public int Capacity { get; set; }
        public int LimitPerUser { get; set; }
        public DateTime[] Schedules { get; set; }
    }
}