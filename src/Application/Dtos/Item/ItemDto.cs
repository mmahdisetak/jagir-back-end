﻿namespace Application.Dtos.Item
{
    public class ItemDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public int Capacity { get; set; }
        public int LimitPerUser { get; set; }
        public SessionDto[] Sessions { get; set; }
    }
}