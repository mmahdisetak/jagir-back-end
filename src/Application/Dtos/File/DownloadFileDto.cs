﻿namespace Application.Dtos.File
{
    public class DownloadFileDto
    {
        public string Path { set; get; }
        public string Name { set; get; }
        public string ContentType { set; get; }
    }
}