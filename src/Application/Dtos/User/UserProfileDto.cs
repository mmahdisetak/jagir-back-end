﻿namespace Application.Dtos.User
{
    public class UserProfileDto
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AvatarId { get; set; }
        public long Credit { get; set; }
    }
}