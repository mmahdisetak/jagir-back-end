﻿using Domain.Enums;

namespace Application.Dtos.User
{
    public interface IChannelEventDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AvatarId { get; set; }
        public ChannelEventType Type { get; set; }
    }
}