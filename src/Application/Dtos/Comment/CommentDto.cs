﻿using System;
using Application.Dtos.User;

namespace Application.Dtos.Comment
{
    public class CommentDto
    {
        public string Id { get; set; }
        public UserShortProfileDto User { get; set; }
        public string Content { get; set; }
        public DateTime CreateDate { get; set; }
    }
}