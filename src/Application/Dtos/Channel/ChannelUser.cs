﻿using Domain.Enums;

namespace Application.Dtos.Channel
{
    public class ChannelUser
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AvatarId { get; set; }
        public UserRole Role { get; set; }
    }
}