﻿using System;

namespace Application.Dtos.Channel
{
    public class ChannelProfileDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AvatarId { get; set; }
        public string BannerId { get; set; }
        public DateTime CreateDate { get; set; }
        public int FollowersCount { get; set; }
        public bool? FollowStatus { get; set; }
    }
}