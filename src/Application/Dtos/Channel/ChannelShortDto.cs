﻿using Application.Dtos.User;
using Domain.Enums;

namespace Application.Dtos.Channel
{
    public class ChannelShortDto : IChannelEventDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AvatarId { get; set; }
        public ChannelEventType Type { get; set; }
        public int FollowersCount { get; set; }
        public int EventsCount { get; set; }
    }
}