﻿using System;
using Application.Dtos.User;
using Application.Dtos.Channel;
using Application.Dtos.Comment;
using Application.Dtos.Event;
using Application.Dtos.Item;
using Application.Dtos.Ticket;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;

namespace Application.Mapping
{
    public class MapperConfig : Profile
    {
        public MapperConfig()
        {
            CreateMap<User, UserProfileDto>();
            CreateMap<User, UserShortProfileDto>();
            CreateMap<User, ChannelUser>();

            CreateMap<Event, EventDto>().ReverseMap();

            CreateMap<Channel, ChannelProfileDto>();
            CreateMap<Channel, ChannelShortDto>()
                .ForMember(ch => ch.AvatarId, opt => opt.MapFrom(src => src.BannerId))
                .ForMember(ch => ch.FollowersCount, opt => opt.MapFrom(src => src.Followers.Count))
                .ForMember(ch => ch.EventsCount, opt => opt.MapFrom(src => src.Events.Count))
                .ForMember(ch => ch.Type, opt => opt.MapFrom(src => ChannelEventType.Channel));
            CreateMap<Event, EventShortDto>()
                .ForMember(evt => evt.ItemsCount, opt => opt.MapFrom(src => src.Items.Count))
                .ForMember(evt => evt.Status,
                    opt => opt.MapFrom(src =>
                        src.EndingDate < DateTime.UtcNow ? EventTime.Passed :
                        src.StartingDate < DateTime.UtcNow ? EventTime.Running : EventTime.Future))
                .ForMember(evt => evt.Type, opt => opt.MapFrom(src => ChannelEventType.Event));

            CreateMap<Comment, CommentDto>();

            CreateMap<Ticket, TicketDto>()
                .ForMember(tkt => tkt.Event, opt => opt.MapFrom(src => src.Item.Event.Name))
                .ForMember(tkt => tkt.Channel, opt => opt.MapFrom(src => src.Item.Event.Channel.Name));
            CreateMap<Ticket, UserTicketDto>()
                .ForMember(tkt => tkt.Event, opt => opt.MapFrom(src => src.Item.Event.Name))
                .ForMember(tkt => tkt.Channel, opt => opt.MapFrom(src => src.Item.Event.Channel.Name));


            CreateMap<Item, CreateItemDto>();
        }
    }
}