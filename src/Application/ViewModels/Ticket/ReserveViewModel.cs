﻿using Application.Common;

namespace Application.ViewModels.Ticket
{
    public class ReserveViewModel
    {
        public class Request
        {
            public string[] ItemIds { get; set; }
        }

        public class Response : BaseResponse
        {
        }
    }
}