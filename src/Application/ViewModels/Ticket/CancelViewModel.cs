﻿using Application.Common;

namespace Application.ViewModels.Ticket
{
    public class CancelViewModel
    {
        public class Request
        {
            public string Id { get; set; }
        }

        public class Response : BaseResponse
        {
        }
    }
}