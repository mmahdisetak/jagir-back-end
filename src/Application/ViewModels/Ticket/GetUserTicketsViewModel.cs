﻿using Application.Common;
using Application.Dtos.Ticket;

namespace Application.ViewModels.Ticket
{
    public class GetUserTicketsViewModel
    {
        public class Response:BaseResponse
        {
            public TicketDto[] Tickets { get; set; }
        }
    }
}