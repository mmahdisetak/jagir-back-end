﻿using Application.Common;

namespace Application.ViewModels.File
{
    public class UploadViewModel
    {
        public class Response : BaseResponse
        {
            public string Id { get; set; }
        }
    }
}