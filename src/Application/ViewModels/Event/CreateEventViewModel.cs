﻿using Application.Common;
using Application.Dtos.Event;

namespace Application.ViewModels.Event
{
    public class CreateEventViewModel
    {
        public class Request
        {
            public string ChannelId { get; set; }
            public EventDto Event { get; set; }
        }

        public class Response : BaseResponse
        {
            public string Id { get; set; }
        }
    }
}