﻿using Application.Common;
using Application.Dtos.Event;

namespace Application.ViewModels.Event
{
    public class GetEventViewModel
    {
        public class Request
        {
            public string Id { get; set; }
        }

        public class Response : BaseResponse
        {
            public string ChannelId { get; set; }
            public EventDto Event { get; set; }
        }
    }
}