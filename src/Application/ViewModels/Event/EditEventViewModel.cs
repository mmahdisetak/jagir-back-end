﻿using Application.Common;
using Application.Dtos.Event;

namespace Application.ViewModels.Event
{
    public class EditEventViewModel
    {
        public class Request
        {
            public string Id { get; set; }
            public EventDto Event { get; set; }
        }

        public class Response : BaseResponse
        {
        }
    }
}