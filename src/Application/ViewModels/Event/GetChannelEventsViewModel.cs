﻿using Application.Common;
using Application.Dtos.Event;
using Domain.Enums;

namespace Application.ViewModels.Event
{
    public class GetChannelEventsViewModel
    {
        public class Request
        {
            public string Id { get; set; }
            public EventFilter Filter { get; set; }
        }

        public class Response : BaseResponse
        {
            public EventDto[] Events { get; set; }
        }
    }
}