﻿using Application.Common;
using Application.Dtos.Ticket;

namespace Application.ViewModels.Event
{
    public class GetEventTicketsViewModel
    {
        public class Request
        {
            public string EventId { get; set; }
        }

        public class Response : BaseResponse
        {
            public UserTicketDto[] Tickets { get; set; }
        }
    }
}