﻿using Application.Common;

namespace Application.ViewModels.Event
{
    public class RemoveEventViewModel
    {
        public class Request
        {
            public string Id { get; set; }
        }

        public class Response : BaseResponse
        {
        }
    }
}