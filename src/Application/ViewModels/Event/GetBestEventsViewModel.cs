﻿using Application.Common;
using Application.Dtos.Event;

namespace Application.ViewModels.Event
{
    public class GetBestEventsViewModel
    {
        public class Request
        {
            public int Size { get; set; }
        }

        public class Response : BaseResponse
        {
            public EventDto[] Events { get; set; }
        }
    }
}