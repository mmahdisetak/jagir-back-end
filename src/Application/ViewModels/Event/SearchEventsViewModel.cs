﻿using Application.Common;
using Application.Dtos.Event;
using Domain.Enums;

namespace Application.ViewModels.Event
{
    public class SearchEventsViewModel
    {
        public class Request
        {
            public string ChannelId { get; set; }
            public string Key { get; set; }
            public EventTime Filter { get; set; }
            public EventType Type { get; set; }
            public EventFilter? Sort { get; set; }
            public int PageSize { get; set; }
            public int PageNumber { get; set; }
        }

        public class Response : BaseResponse
        {
            public EventDto[] Events { get; set; }
            public int Total { get; set; }
        }
    }
}