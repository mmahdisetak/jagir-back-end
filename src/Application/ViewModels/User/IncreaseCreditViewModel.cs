﻿using Application.Common;

namespace Application.ViewModels.User
{
    public class IncreaseCreditViewModel
    {
        public class Request
        {
            public long Credit { get; set; }
        }

        public class Response : BaseResponse
        {
            public long Credit { get; set; }
        }
    }
}