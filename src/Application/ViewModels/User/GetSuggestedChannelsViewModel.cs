﻿using Application.Common;
using Application.Dtos.Channel;

namespace Application.ViewModels.User
{
    public class GetSuggestedChannelsViewModel
    {
        public class Request
        {
            public int Count { get; set; }
        }

        public class Response : BaseResponse
        {
            public ChannelProfileDto[] Channels { get; set; }
        }
    }
}