﻿using Application.Common;
using Application.Dtos.Channel;

namespace Application.ViewModels.User
{
    public class GetFollowedChannelsViewModel
    {
        public class Response : BaseResponse
        {
            public ChannelShortDto[] Channels { get; set; }
        }
    }
}