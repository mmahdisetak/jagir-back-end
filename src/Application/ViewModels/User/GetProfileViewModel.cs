﻿using Application.Common;
using Application.Dtos.User;

namespace Application.ViewModels.User
{
    public class GetProfileViewModel
    {
        public class Response : BaseResponse
        {
            public UserProfileDto Profile { get; set; }
        }
    }
}