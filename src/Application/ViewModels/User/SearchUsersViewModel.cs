﻿using Application.Common;
using Application.Dtos.User;

namespace Application.ViewModels.User
{
    public class SearchUsersViewModel
    {
        public class Request
        {
            public string SearchKey { get; set; }
            public int Size { get; set; }
        }

        public class Response : BaseResponse
        {
            public UserShortProfileDto[] Users { get; set; }
        }
    }
}