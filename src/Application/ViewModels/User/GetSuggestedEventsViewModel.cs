﻿using Application.Common;
using Application.Dtos.Event;

namespace Application.ViewModels.User
{
    public class GetSuggestedEventsViewModel
    {
        public class Request
        {
            public int Count { get; set; }
        }

        public class Response : BaseResponse
        {
            public EventDto[] Events { get; set; }
        }
    }
}