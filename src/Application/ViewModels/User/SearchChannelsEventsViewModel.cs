﻿using Application.Common;
using Application.Dtos.User;

namespace Application.ViewModels.User
{
    public class SearchChannelsEventsViewModel
    {
        public class Request
        {
            public string ChannelId { get; set; }
            public string Key { get; set; }
            public int Size { get; set; }
        }

        public class Response : BaseResponse
        {
            public IChannelEventDto[] ChannelsEvents { get; set; }
        }
    }
}