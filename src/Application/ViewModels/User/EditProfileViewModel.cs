﻿using Application.Common;

namespace Application.ViewModels.User
{
    public class EditProfileViewModel
    {
        public class Request
        {
            public string Email { get; set; }
            public string UserName { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string AvatarId { get; set; }
        }

        public class Response : BaseResponse
        {
        }
    }
}