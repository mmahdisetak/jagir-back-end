﻿using Application.Common;

namespace Application.ViewModels.User
{
    public class FollowChannelViewModel
    {
        public class Request
        {
            public string ChannelId { get; set; }
            public bool Status { get; set; }
        }

        public class Response : BaseResponse
        {
        }
    }
}