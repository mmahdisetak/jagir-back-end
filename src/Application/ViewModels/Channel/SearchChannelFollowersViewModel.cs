﻿using Application.Common;
using Application.Dtos.User;

namespace Application.ViewModels.Channel
{
    public class SearchChannelFollowersViewModel
    {
        public class Request
        {
            public string ChannelId { get; set; }
            public string SearchKey { get; set; }
            public int PageSize { get; set; }
            public int PageNumber { get; set; }
        }

        public class Response : BaseResponse
        {
            public int Total { get; set; }
            public UserShortProfileDto[] Followers { get; set; }
        }
    }
}