﻿using Application.Common;

namespace Application.ViewModels.Channel
{
    public class CreateChannelViewModel
    {
        public class Request
        {
            public string Name { get; set; }
            public string Description { get; set; }
            public string OwnerId { get; set; }
            public string[] SupervisorsId { get; set; }
        }

        public class Response : BaseResponse
        {
            public string Id { get; set; }
        }
    }
}