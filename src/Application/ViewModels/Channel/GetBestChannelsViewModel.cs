﻿using Application.Common;
using Application.Dtos.Channel;

namespace Application.ViewModels.Channel
{
    public class GetBestChannelsViewModel
    {
        public class Request
        {
            public int Size { get; set; }
        }

        public class Response : BaseResponse
        {
            public ChannelProfileDto[] Channels { get; set; }
        }
    }
}