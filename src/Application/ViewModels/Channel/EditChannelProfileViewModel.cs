﻿using Application.Common;

namespace Application.ViewModels.Channel
{
    public class EditChannelProfileViewModel
    {
        public class Request
        {
            public string ChannelId { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public string AvatarId { get; set; }
            public string BannerId { get; set; }
        }

        public class Response : BaseResponse
        {
        }
    }
}