﻿using Application.Common;
using Application.Dtos.Channel;

namespace Application.ViewModels.Channel
{
    public class SearchChannelUsersViewModel
    {
        public class Request
        {
            public string ChannelId { get; set; }
            public string SearchKey { get; set; }
        }

        public class Response : BaseResponse
        {
            public ChannelUser[] Users { get; set; }
        }
    }
}