﻿using Application.Common;
using Application.Dtos.Channel;

namespace Application.ViewModels.Channel
{
    public class GetChannelProfileViewModel
    {
        public class Request
        {
            public string Id { get; set; }
        }

        public class Response : BaseResponse
        {
            public ChannelProfileDto Channel { get; set; }
            public bool IsSupervisor { get; set; }
        }
    }
}