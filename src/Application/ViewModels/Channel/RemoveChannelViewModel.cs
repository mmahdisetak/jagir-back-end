﻿using Application.Common;

namespace Application.ViewModels.Channel
{
    public class RemoveChannelViewModel
    {
        public class Request
        {
            public string Id { get; set; }
        }

        public class Response : BaseResponse
        {
        }
    }
}