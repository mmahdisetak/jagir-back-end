﻿using Application.Common;

namespace Application.ViewModels.Channel
{
    public class ChangeSupervisorViewModel
    {
        public class Request
        {
            public string ChannelId { get; set; }
            public string UserId { get; set; }
        }

        public class Response : BaseResponse
        {
        }
    }
}