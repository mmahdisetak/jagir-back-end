﻿using Application.Common;
using Application.Dtos.Channel;

namespace Application.ViewModels.Channel
{
    public class GetOwnedChannelsViewModel
    {
        public class Response : BaseResponse
        {
            public ChannelShortDto[] Channels { get; set; }
        }
    }
}