﻿using Application.Common;
using Application.Dtos.Item;

namespace Application.ViewModels.Item
{
    public class GetEventItemsViewModel
    {
        public class Request
        {
            public string EventId { get; set; }
        }

        public class Response : BaseResponse
        {
            public ItemDto[] Items { get; set; }
        }
    }
}