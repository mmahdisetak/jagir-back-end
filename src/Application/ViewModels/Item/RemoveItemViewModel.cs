﻿using Application.Common;

namespace Application.ViewModels.Item
{
    public class RemoveItemViewModel
    {
        public class Request
        {
            public string Id { get; set; }
        }

        public class Response : BaseResponse
        {
        }
    }
}