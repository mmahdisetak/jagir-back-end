﻿using Application.Common;
using Application.Dtos.Item;

namespace Application.ViewModels.Item
{
    public class CreateItemViewModel
    {
        public class Request
        {
            public string EventId { get; set; }
            public CreateItemDto Item { get; set; }
        }

        public class Response : BaseResponse
        {
        }
    }
}