﻿using Application.Common;
using Application.Dtos.Item;

namespace Application.ViewModels.Item
{
    public class EditItemViewModel
    {
        public class Request
        {
            public ItemDto Item { get; set; }
        }

        public class Response : BaseResponse
        {
        }
    }
}