﻿using Application.Common;

namespace Application.ViewModels.Comment
{
    public class SubmitCommentViewModel
    {
        public class Request
        {
            public string EventId { get; set; }
            public string Content { get; set; }
        }

        public class Response : BaseResponse
        {
        }
    }
}