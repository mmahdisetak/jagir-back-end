﻿using Application.Common;
using Application.Dtos.Comment;

namespace Application.ViewModels.Comment
{
    public class GetEventCommentsViewModel
    {
        public class Request
        {
            public string EventId { get; set; }
        }

        public class Response : BaseResponse
        {
            public CommentDto[] Comments { get; set; }
        }
    }
}