using Application.Common;
using Domain.Enums;

namespace Application.ViewModels.Account
{
    public class ForgetPasswordViewModel
    {
        public class Request
        {
            public string Login { get; set; }
            public SignInType Type { get; set; }
        }

        public class Response : BaseResponse
        {
        }
    }
}