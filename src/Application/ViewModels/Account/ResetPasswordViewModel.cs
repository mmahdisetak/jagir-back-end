using Application.Common;

namespace Application.ViewModels.Account
{
    public class ResetPasswordViewModel
    {
        public class Request
        {
            public string Email { get; set; }
            public string Token { get; set; }
            public string NewPassword { get; set; }
        }

        public class Response : BaseResponse
        {
        }
    }
}