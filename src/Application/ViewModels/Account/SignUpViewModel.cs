﻿using Application.Common;

namespace Application.ViewModels.Account
{
    public class SignUpViewModel
    {
        public class Request
        {
            public string UserName { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
        }

        public class Response : BaseResponse
        {
        }
    }
}