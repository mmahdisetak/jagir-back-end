﻿using Application.Common;
using Domain.Enums;

namespace Application.ViewModels.Account
{
    public class SignInViewModel
    {
        public class Request
        {
            public SignInType Type { get; set; }
            public string Login { get; set; }
            public string Password { set; get; }
        }

        public class Response : BaseResponse
        {
        }
    }
}