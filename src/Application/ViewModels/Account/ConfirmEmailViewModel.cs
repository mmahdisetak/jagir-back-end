﻿using Application.Common;

namespace Application.ViewModels.Account
{
    public class ConfirmEmailViewModel
    {
        public class Request
        {
            public string Email { set; get; }
            public string Token { set; get; }
        }

        public class Response : BaseResponse
        {
        }
    }
}