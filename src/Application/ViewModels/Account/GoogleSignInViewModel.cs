﻿using Application.Common;

namespace Application.ViewModels.Account
{
    public class GoogleSignInViewModel
    {
        public class Request
        {
            public string IdToken { set; get; }
        }

        public class Response : BaseResponse
        {
        }
    }
}