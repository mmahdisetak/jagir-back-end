﻿using System.Threading.Tasks;
using Application.ViewModels.Ticket;
using Infrastructure.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class TicketController : Controller
    {
        private readonly ITicketService _ticketService;

        public TicketController(ITicketService ticketService)
        {
            _ticketService = ticketService;
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(ReserveViewModel.Response), 200)]
        public async Task<IActionResult> Reserve(ReserveViewModel.Request request)
        {
            await _ticketService.Reserve(request.ItemIds);
            return Json(new ReserveViewModel.Response());
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(GetUserTicketsViewModel.Response), 200)]
        public async Task<IActionResult> GetUserTickets()
        {
            return Json(new GetUserTicketsViewModel.Response
                {Tickets = await _ticketService.GetUserTickets()});
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(CancelViewModel.Response), 200)]
        public IActionResult Cancel(CancelViewModel.Request request)
        {
            _ticketService.Cancel(request.Id);
            return Json(new CancelViewModel.Response());
        }
    }
}