﻿using System.Threading.Tasks;
using Application.ViewModels.User;
using Infrastructure.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(GetProfileViewModel.Response), 200)]
        public async Task<IActionResult> GetProfile()
        {
            return Json(new GetProfileViewModel.Response
                {Profile = await _userService.GetProfile()}); //TODO test
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(FollowChannelViewModel.Response), 200)]
        public async Task<IActionResult> FollowChannel(FollowChannelViewModel.Request request)
        {
            await _userService.FollowChannel(request.ChannelId, request.Status);
            return Json(new FollowChannelViewModel.Response());
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(EditProfileViewModel.Response), 200)]
        public async Task<IActionResult> EditProfile(EditProfileViewModel.Request request)
        {
            await _userService.EditProfile(request.Email, request.UserName, request.FirstName, request.LastName,
                request.AvatarId);
            return Json(new EditProfileViewModel.Response());
        }

        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(SearchChannelsEventsViewModel.Response), 200)]
        public IActionResult SearchChannelsEvents(SearchChannelsEventsViewModel.Request request)
        {
            var output = JsonConvert.SerializeObject(new SearchChannelsEventsViewModel.Response
                    {ChannelsEvents = _userService.SearchChannelsEvents(request.ChannelId, request.Key, request.Size)},
                new JsonSerializerSettings {ContractResolver = new CamelCasePropertyNamesContractResolver()});
            return Content(output, new MediaTypeHeaderValue("application/json")); //TODO test
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(IncreaseCreditViewModel.Response), 200)]
        public async Task<IActionResult> IncreaseCredit(IncreaseCreditViewModel.Request request)
        {
            return Json(new IncreaseCreditViewModel.Response
                {Credit = await _userService.IncreaseCredit(request.Credit)}); //TODO test
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(GetFollowedChannelsViewModel.Response), 200)]
        public IActionResult GetFollowedChannels()
        {
            return Json(new GetFollowedChannelsViewModel.Response
                {Channels = _userService.GetFollowedChannels()}); //TODO test
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(GetSuggestedChannelsViewModel.Response), 200)]
        public IActionResult GetSuggestedChannels(GetSuggestedChannelsViewModel.Request request)
        {
            return Json(new GetSuggestedChannelsViewModel.Response
                {Channels = _userService.GetSuggestedChannels(request.Count)}); //TODO test
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(GetSuggestedEventsViewModel.Response), 200)]
        public IActionResult GetSuggestedEvents(GetSuggestedEventsViewModel.Request request)
        {
            return Json(new GetSuggestedEventsViewModel.Response
                {Events = _userService.GetSuggestedEvents(request.Count)}); //TODO test
        }

        [HttpPost]
        [Authorize(Policy = "AdminPolicy")]
        [ProducesResponseType(typeof(SearchUsersViewModel.Response), 200)]
        public IActionResult SearchUsers(SearchUsersViewModel.Request request)
        {
            return Json(new SearchUsersViewModel.Response
                {Users = _userService.SearchUsers(request.SearchKey, request.Size)});
        }
    }
}