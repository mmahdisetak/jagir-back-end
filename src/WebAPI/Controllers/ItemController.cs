﻿using System.Threading.Tasks;
using Application.ViewModels.Item;
using Infrastructure.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class ItemController : Controller
    {
        private readonly IItemService _itemService;

        public ItemController(IItemService itemService)
        {
            _itemService = itemService;
        }

        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(GetEventItemsViewModel.Response), 200)]
        public async Task<IActionResult> GetEventItems(GetEventItemsViewModel.Request request)
        {
            return Json(new GetEventItemsViewModel.Response
                {Items = await _itemService.GetEventItems(request.EventId)});
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(CreateItemViewModel.Response), 200)]
        public IActionResult CreateItem(CreateItemViewModel.Request request)
        {
            _itemService.CreateItem(request.Item, request.EventId);
            return Json(new CreateItemViewModel.Response());
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(EditItemViewModel.Response), 200)]
        public IActionResult EditItem(EditItemViewModel.Request request)
        {
            _itemService.EditItem(request.Item);
            return Json(new EditItemViewModel.Response());
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(RemoveItemViewModel.Response), 200)]
        public async Task<IActionResult> RemoveItem(RemoveItemViewModel.Request request)
        {
            await _itemService.RemoveItem(request.Id);
            return Json(new RemoveItemViewModel.Response());
        }
    }
}