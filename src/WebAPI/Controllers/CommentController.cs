﻿using System.Threading.Tasks;
using Application.ViewModels.Comment;
using Infrastructure.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class CommentController : Controller
    {
        private readonly ICommentService _commentService;

        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }
        
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(GetEventCommentsViewModel.Response), 200)]
        public IActionResult GetEventComments(GetEventCommentsViewModel.Request request)
        {
            return Json(new GetEventCommentsViewModel.Response
                {Comments = _commentService.GetEventComments(request.EventId)});
        }
        
        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(SubmitCommentViewModel.Response), 200)]
        public async Task<IActionResult> SubmitComment(SubmitCommentViewModel.Request request)
        {
            await _commentService.SubmitComment(request.EventId, request.Content);
            return Json(new SubmitCommentViewModel.Response());
        }
    }
}