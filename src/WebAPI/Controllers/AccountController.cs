﻿using System.Threading.Tasks;
using Application.ViewModels.Account;
using Infrastructure.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(SignUpViewModel.Response), 200)]
        public async Task<IActionResult> SignUp(SignUpViewModel.Request request)
        {
            await _accountService.SignUp(request.UserName, request.Email, request.Password);
            return Json(new SignUpViewModel.Response());
        }

        [AllowAnonymous]
        [HttpPost]
        [ProducesResponseType(typeof(ConfirmEmailViewModel.Response), 200)]
        public async Task<IActionResult> ConfirmEmail(ConfirmEmailViewModel.Request request)
        {
            await _accountService.ConfirmEmail(request.Email, request.Token);
            return Json(new ConfirmEmailViewModel.Response()); //TODO test
        }

        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(SignInViewModel.Response), 200)]
        public async Task<IActionResult> SignIn(SignInViewModel.Request request)
        {
            await _accountService.SignIn(request.Type, request.Login, request.Password);
            return Json(new SignInViewModel.Response());
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(SignOutViewModel.Response), 200)]
        public new async Task<IActionResult> SignOut()
        {
            await _accountService.SignOut();
            return Json(new SignOutViewModel.Response());
        }

        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(ForgetPasswordViewModel.Response), 200)]
        public async Task<IActionResult> ForgetPassword(ForgetPasswordViewModel.Request request)
        {
            await _accountService.ForgetPassword(request.Login, request.Type);
            return Json(new ForgetPasswordViewModel.Response());
        }


        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(ResetPasswordViewModel.Response), 200)]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel.Request request)
        {
            await _accountService.ResetPassword(request.Email, request.Token, request.NewPassword);
            return Json(new ResetPasswordViewModel.Response()); // TODO test
        }

        [AllowAnonymous]
        [HttpGet("{provider}")]
        public IActionResult GoogleSignIn(string provider)
        {
            var properties = _accountService.GoogleSignIn(provider, Url.Action(nameof(ExternalSignIn)));
            return new ChallengeResult(provider, properties);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> ExternalSignIn()
        {
            try
            {
                await _accountService.ExternalSignIn();
                return Redirect("https://jagir.markop.ir/google");
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}