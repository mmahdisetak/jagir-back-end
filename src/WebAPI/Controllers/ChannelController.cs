﻿using System.Threading.Tasks;
using Application.ViewModels.Channel;
using Infrastructure.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class ChannelController : Controller
    {
        private readonly IChannelService _channelService;

        public ChannelController(IChannelService channelService)
        {
            _channelService = channelService;
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(EditChannelProfileViewModel.Response), 200)]
        public async Task<IActionResult> EditChannelProfile(EditChannelProfileViewModel.Request request)
        {
            await _channelService.EditChannelProfile(request.ChannelId, request.Name, request.Description,
                request.BannerId, request.AvatarId);
            return Json(new EditChannelProfileViewModel.Response());
        }

        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(GetChannelProfileViewModel.Response), 200)]
        public async Task<IActionResult> GetChannelProfile(GetChannelProfileViewModel.Request request)
        {
            var (isSupervisor, channel) = await _channelService.GetChannelProfile(request.Id);
            return Json(new GetChannelProfileViewModel.Response
                {IsSupervisor = isSupervisor, Channel = channel});
        }

        [HttpPost]
        [Authorize(Policy = "AdminPolicy")]
        [ProducesResponseType(typeof(CreateChannelViewModel.Response), 200)]
        public async Task<IActionResult> CreateChannel(CreateChannelViewModel.Request request)
        {
            return Json(new CreateChannelViewModel.Response
            {
                Id = await _channelService.CreateChannel(request.Name, request.Description, request.OwnerId,
                    request.SupervisorsId)
            });
        }

        [HttpPost]
        [Authorize(Policy = "AdminPolicy")]
        [ProducesResponseType(typeof(RemoveChannelViewModel.Response), 200)]
        public IActionResult RemoveChannel(RemoveChannelViewModel.Request request)
        {
            _channelService.RemoveChannel(request.Id);
            return Json(new RemoveChannelViewModel.Response());
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(SearchChannelFollowersViewModel.Response), 200)]
        public IActionResult SearchChannelFollowers(SearchChannelFollowersViewModel.Request request)
        {
            var (total, followers) =
                _channelService.SearchChannelFollowers(request.ChannelId, request.SearchKey, request.PageSize,
                    request.PageNumber);
            return Json(new SearchChannelFollowersViewModel.Response {Total = total, Followers = followers});
        }

        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(ChangeSupervisorViewModel.Response), 200)]
        public async Task<IActionResult> AddSupervisor(ChangeSupervisorViewModel.Request request)
        {
            await _channelService.AddSupervisor(request.ChannelId, request.UserId);
            return Json(new ChangeSupervisorViewModel.Response());
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(ChangeSupervisorViewModel.Response), 200)]
        public async Task<IActionResult> RemoveSupervisor(ChangeSupervisorViewModel.Request request)
        {
            await _channelService.RemoveSupervisor(request.ChannelId, request.UserId);
            return Json(new ChangeSupervisorViewModel.Response());
        }

        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(GetBestChannelsViewModel.Response), 200)]
        public IActionResult GetBestChannels(GetBestChannelsViewModel.Request request)
        {
            return Json(new GetBestChannelsViewModel.Response
                {Channels = _channelService.GetBestChannels(request.Size)});
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(SearchChannelUsersViewModel.Response), 200)]
        public async Task<IActionResult> SearchChannelUsers(SearchChannelUsersViewModel.Request request)
        {
            return Json(new SearchChannelUsersViewModel.Response
                {Users = await _channelService.SearchChannelUsers(request.ChannelId, request.SearchKey)});
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(GetOwnedChannelsViewModel.Response), 200)]
        public async Task<IActionResult> GetOwnedChannels()
        {
            return Json(new GetOwnedChannelsViewModel.Response
                {Channels = await _channelService.GetOwnedChannels()});
        }
    }
}