﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Application.ViewModels.File;
using Infrastructure.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class FileController : Controller
    {
        private readonly IFileService _fileService;

        public FileController(IFileService fileService)
        {
            _fileService = fileService;
        }

        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(UploadViewModel.Response), 200)]
        public async Task<IActionResult> Upload(IFormCollection collection)
        {
            return Json(new UploadViewModel.Response
            {
                Id = await _fileService.Upload(collection.Files.FirstOrDefault(),
                    byte.TryParse(collection["type"], out var type) ? type : (byte?) null)
            });
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Download(string id)
        {
            try
            {
                var fileDto = _fileService.Download(id);

                if (!string.IsNullOrWhiteSpace(fileDto.ContentType) && fileDto.ContentType.StartsWith("image"))
                    HttpContext.Response.Headers.Append("Cache-Control",
                        "max-age=" + new TimeSpan(30, 0, 0, 0)
                            .TotalSeconds.ToString("0"));

                var stream = System.IO.File.OpenRead(fileDto.Path);
                return File(stream, fileDto.ContentType ?? "application/octet-stream", fileDto.Name);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return NotFound();
            }
        }
    }
}