﻿using System.Threading.Tasks;
using Application.ViewModels.Event;
using Domain.Enums;
using Infrastructure.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class EventController : Controller
    {
        private readonly IEventService _eventService;

        public EventController(IEventService eventService)
        {
            _eventService = eventService;
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(CreateEventViewModel.Response), 200)]
        public async Task<IActionResult> CreateEvent(CreateEventViewModel.Request request)
        {
            return Json(new CreateEventViewModel.Response
                {Id = await _eventService.CreateEvent(request.Event, request.ChannelId)});
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(EditEventViewModel.Response), 200)]
        public IActionResult EditEvent(EditEventViewModel.Request request)
        {
            _eventService.EditEvent(request.Id, request.Event);
            return Json(new EditEventViewModel.Response()); //TODO test
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(RemoveEventViewModel.Response), 200)]
        public async Task<IActionResult> RemoveEvent(RemoveEventViewModel.Request request)
        {
            await _eventService.RemoveEvent(request.Id);
            return Json(new RemoveEventViewModel.Response());
        }

        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(GetEventViewModel.Response), 200)]
        public IActionResult GetEvent(GetEventViewModel.Request request)
        {
            var (channelId, @event) = _eventService.GetEvent(request.Id);
            return Json(new GetEventViewModel.Response {ChannelId = channelId, Event = @event});
        }

        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(GetChannelEventsViewModel.Response), 200)]
        public IActionResult GetCurrentEvents(GetChannelEventsViewModel.Request request)
        {
            var (_, events) = _eventService.SearchEvents(request.Id, "", EventTime.Running,
                EventType.Casual | EventType.Weekly, 1, 20, request.Filter);
            return Json(new GetChannelEventsViewModel.Response {Events = events});
        }

        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(GetChannelEventsViewModel.Response), 200)]
        public IActionResult GetUpcomingEvents(GetChannelEventsViewModel.Request request)
        {
            var (_, events) = _eventService.SearchEvents(request.Id, "", EventTime.Future,
                EventType.Casual | EventType.Weekly, 1, 20, request.Filter);
            return Json(new GetChannelEventsViewModel.Response {Events = events});
        }

        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(GetChannelEventsViewModel.Response), 200)]
        public IActionResult GetPassedWeeklyEvents(GetChannelEventsViewModel.Request request)
        {
            var (_, events) = _eventService.SearchEvents(request.Id, "", EventTime.Passed, EventType.Weekly, 1, 20,
                request.Filter);
            return Json(new GetChannelEventsViewModel.Response {Events = events});
        }

        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(GetChannelEventsViewModel.Response), 200)]
        public IActionResult GetPassedCasualEvents(GetChannelEventsViewModel.Request request)
        {
            var (_, events) = _eventService.SearchEvents(request.Id, "", EventTime.Passed, EventType.Casual, 1, 20,
                request.Filter);
            return Json(new GetChannelEventsViewModel.Response {Events = events});
        }

        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(SearchEventsViewModel.Response), 200)]
        public IActionResult SearchEvents(SearchEventsViewModel.Request request)
        {
            var (total, events) = _eventService.SearchEvents(request.ChannelId, request.Key, request.Filter,
                request.Type, request.PageNumber, request.PageSize, request.Sort);
            return Json(new SearchEventsViewModel.Response {Events = events, Total = total});
        }

        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(GetBestEventsViewModel.Response), 200)]
        public IActionResult GetBestEvents(GetBestEventsViewModel.Request request)
        {
            return Json(new GetBestEventsViewModel.Response
                {Events = _eventService.GetBestEvents(request.Size)});
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(GetEventTicketsViewModel.Response), 200)]
        public async Task<IActionResult> GetEventTickets(GetEventTicketsViewModel.Request request)
        {
            return Json(
                new GetEventTicketsViewModel.Response {Tickets = await _eventService.GetEventTickets(request.EventId)});
        }
    }
}