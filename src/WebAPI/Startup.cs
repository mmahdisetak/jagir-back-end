using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Application.Mapping;
using Domain.Entities;
using Infrastructure.Middlewares;
using Infrastructure.Persistence;
using Infrastructure.Policy.AuthorizationPolicy;
using Infrastructure.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Authentication.Google;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Options;
using Hangfire;
using Hangfire.PostgreSql;
using Infrastructure.Identity;

namespace WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            if (!Directory.Exists(Configuration["StorePath"]))
                Directory.CreateDirectory(Configuration["StorePath"]);

            services.AddDbContextPool<DatabaseContext>(option =>
            {
                var stringConnection = Environment.GetEnvironmentVariable("JagirDBConnection");
                option.UseNpgsql(stringConnection ?? Configuration.GetConnectionString("DBConnectionPostgreSQL"));
            });

            services.AddCors(options =>
            {
                options.AddPolicy("EnableCors", builder =>
                {
                    builder.WithOrigins("http://localhost:3000", "https://localhost:3000", "http://jagir.markop.ir",
                            "https://jagir.markop.ir")
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials()
                        .WithExposedHeaders("Roles", "Content-Type")
                        .Build();
                });
            });

            services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<DatabaseContext>()
                .AddUserManager<UserManager<User>>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 6;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
            });

            services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.Name = "token";
                options.Cookie.HttpOnly = false;
                options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
                options.Cookie.SameSite = SameSiteMode.None;
                options.ExpireTimeSpan = TimeSpan.FromDays(365);
                options.Events.OnRedirectToLogin = context =>
                {
                    context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                    return Task.CompletedTask;
                };
                options.Events.OnRedirectToAccessDenied = context =>
                {
                    context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                    return Task.CompletedTask;
                };
            });

            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IChannelService, ChannelService>();
            services.AddScoped<IEventService, EventService>();
            services.AddScoped<IFileService, FileService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IItemService, ItemService>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<ICommentService, CommentService>();
            services.AddScoped<ITicketService, TicketService>();
            services.AddScoped<IJobService, JobService>();


            services.AddControllers();
            services.AddSwaggerGen(option =>
            {
                option.CustomSchemaIds(type =>
                {
                    var name = string.Empty;
                    if (type.DeclaringType != null)
                        name += type.DeclaringType.Name + ".";
                    return name + type.Name;
                });
            });
            services.AddAutoMapper(typeof(MapperConfig));

            services.AddAuthentication()
                .AddGoogle(GoogleDefaults.AuthenticationScheme, options =>
                {
                    options.ClientId = "549353084111-uljpn463vsp0qlka1fprs5803li2eqmv.apps.googleusercontent.com";
                    options.ClientSecret = "D8GDXmFm4R20Fv8e0yX9diAc";
                });

            services.AddAuthorization(options => options.AddPolicy("AdminPolicy",
                policy => policy.AddRequirements(new AuthorizationRequirements(new List<string> {"Admin"}))));
            services.AddScoped<IAuthorizationHandler, RoleAuthorizationHandler>();

            services.AddLocalization(options => options.ResourcesPath = "Resources");
            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[] {new CultureInfo("fa-IR")};
                options.DefaultRequestCulture = new RequestCulture("fa-IR");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });

            var hangFireDbConnectionPostgreSql =
                Environment.GetEnvironmentVariable("HangFireDBConnectionPostgreSQL");
            if (Utilities.IsUnitTestRunning) return;
            services.AddHangfireServer();
            services.AddHangfire(config =>
                config.UsePostgreSqlStorage(hangFireDbConnectionPostgreSql ??
                                            Configuration.GetConnectionString("HangFireDBConnectionPostgreSQL")));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.Use(async (context, next) =>
            {
                if (!string.IsNullOrEmpty(context.Request.Headers["X-Forwarded-Proto"]))
                    context.Request.Scheme = context.Request.Headers["X-Forwarded-Proto"];
                await next.Invoke();
            });
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "Jagir API"); });
            if (!Utilities.IsUnitTestRunning)
                app.UseHangfireDashboard("/hangfire", new DashboardOptions
                {
                    Authorization = new[] {new HangFireAuthorizationFilter()}
                });

            app.UseRouting();

            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(options.Value);

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseMiddleware<ExceptionServiceMiddleware>();

            app.UseCors("EnableCors");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
                if (!Utilities.IsUnitTestRunning)
                    endpoints.MapHangfireDashboard();
            });
        }
    }
}