﻿using System;
using System.Linq;

namespace WebAPI
{
    public static class Utilities
    {
        public static bool IsUnitTestRunning => AppDomain.CurrentDomain.GetAssemblies().Any(a =>
            a.FullName != null && a.FullName.ToLowerInvariant().StartsWith("xunit.execution.dotnet"));
    }
}